

SELECT  objetos.grupo,  concat(e.ff,'-',e.of) as ffof, 
  e.clase,   e.programa,   e.actividad,   e.obj,   e.ff,   e.of,   e.dpto,   objetos.obj_decripcion, i.inicial, i.modifi, i.vigente,  
  pla1, eje1, pla2, eje2, pla3, eje3, pla4, eje4, pla5, eje5, pla6, eje6,   pla7, eje7,  
  pla8, eje8, pla9, eje9,  pla10, eje10, pla11, eje11, pla12, eje12, platotal, ejetotal, 

  planfinanciero,  obligado, saldo,  saldo_plan, pagado, pendiente,   prevision,  compromiso  
  
FROM  
  migracion.qry_estructura e left join basicas.objetos on (objetos.obj = e.obj)  
  left join migracion.inicial_migracion i on ( i.clase = e.clase AND  i.programa = e.programa AND  i.actividad = e.actividad AND  
	i.obj = e.obj AND  i.ff = e.ff AND  i.of = e.of AND  i.dpto = e.dpto )  

  left join migracion.planfinanciero_migracion p on ( p.clase = e.clase AND  p.programa = e.programa AND  p.actividad = e.actividad AND 
	p.obj = e.obj AND  p.ff = e.ff AND  p.of = e.of AND  p.dpto = e.dpto )   


  left join migracion.qry_ejecucion_migracion j on ( j.clase = e.clase AND  j.programa = e.programa AND  j.actividad = e.actividad AND 
	j.obj = e.obj AND  j.ff = e.ff AND  j.of = e.of AND  j.dpto = e.dpto )  	 


  left join migracion.prevcom_migracion c on ( c.clase = e.clase AND  c.programa = e.programa AND  c.actividad = e.actividad AND 
	c.obj = e.obj AND  c.ff = e.ff AND  c.of = e.of AND  c.dpto = e.dpto )   

    where 
        not ( e.ff = 30 and e.of = 76 ) 
        and not ( e.ff = 20 and e.of = 401 ) 
        and e.obj <> 811 
        and e.obj <> 861  


    order by objetos.grupo, e.ff,   e.of, 
    e.clase,   e.programa,   e.actividad,   e.obj,   e.dpto  
	