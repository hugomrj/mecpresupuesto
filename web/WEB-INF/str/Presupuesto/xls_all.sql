
SELECT  
  e.clase,   e.programa,   e.actividad,   e.obj,   e.ff,   e.of,   e.dpto,   objetos.obj_decripcion, 
  i.inicial, i.modifi, i.vigente,  
  pla1, eje1, pla2, eje2, pla3, eje3, pla4, eje4, pla5, eje5, pla6, eje6,   pla7, eje7,   
  pla8, eje8, pla9, eje9,  pla10, eje10, pla11, eje11, pla12, eje12, ejetotal,  
  
     CASE WHEN platotal is null THEN 0 ELSE platotal end as platotal, 
     CASE WHEN prevision is null THEN 0 ELSE prevision end as prevision,   
     CASE WHEN pendiente is null THEN 0 ELSE pendiente end as pendiente, 
     CASE WHEN planfinanciero is null THEN 0 ELSE planfinanciero end as planificado, 
     CASE WHEN planfinanciero is null and platotal is null  
     	THEN 0  
     	ELSE (platotal - planfinanciero )  end  
     	as reserva, 
     CASE WHEN obligado is null THEN 0 ELSE obligado end as obligado, 
     CASE WHEN saldo_plan is null THEN 0 ELSE saldo_plan end as saldo_plan, 
     CASE WHEN compromiso is null THEN 0 ELSE compromiso end as compromiso, 
    pagado 

FROM  
  migracion.qry_estructura e left join basicas.objetos on (objetos.obj = e.obj)  
  left join migracion.inicial_migracion i on ( i.clase = e.clase AND  i.programa = e.programa AND  i.actividad = e.actividad AND 
	i.obj = e.obj AND  i.ff = e.ff AND  i.of = e.of AND  i.dpto = e.dpto )  

  left join migracion.planfinanciero_migracion p on ( p.clase = e.clase AND  p.programa = e.programa AND  p.actividad = e.actividad AND 
	p.obj = e.obj AND  p.ff = e.ff AND  p.of = e.of AND  p.dpto = e.dpto )   

  left join migracion.qry_ejecucion_migracion j on ( j.clase = e.clase AND  j.programa = e.programa AND  j.actividad = e.actividad AND  
	j.obj = e.obj AND  j.ff = e.ff AND  j.of = e.of AND  j.dpto = e.dpto )  	 

  left join migracion.prevcom_migracion c on ( c.clase = e.clase AND  c.programa = e.programa AND  c.actividad = e.actividad AND  
	c.obj = e.obj AND  c.ff = e.ff AND  c.of = e.of AND  c.dpto = e.dpto )   

    order by e.clase,   e.programa,   e.actividad,   e.obj,   e.ff,   e.of,   e.dpto  




