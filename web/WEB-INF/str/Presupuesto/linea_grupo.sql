


SELECT  
  e.clase,   e.programa,   e.actividad,   objetos.grupo,   e.ff,   e.of,   e.dpto,   
  
sum(i.inicial) inicial,  sum(i.modifi)  modifi ,  sum(i.vigente) vigente, 
  sum(pla1) pla1,  sum(eje1) eje1, sum(pla2) pla2, sum(eje2) eje2,  sum(pla3) pla3, sum(eje3) eje3 , 
  sum(pla4) pla4, sum(eje4) eje4,  sum(pla5) pla5,  sum(eje5) eje5, sum(pla6) pla6,  sum(eje6) eje6,   
  sum(pla7) pla7,  sum(eje7) eje7,  sum(pla8)  pla8,  sum(eje8) eje8,  sum(pla9) pla9,  sum(eje9) eje9,  
  sum(pla10) pla10,  sum(eje10) eje10, sum(pla11) pla11,  sum(eje11) eje11, sum(pla12) pla12,  sum(eje12 ) eje12, 
  sum(platotal) platotal, sum(ejetotal) ejetotal, 
sum(planfinanciero) planfinanciero, 
   sum(obligado)  obligado, sum(saldo) saldo,
 sum(saldo_plan) saldo_plan,  sum(pagado) pagado, 
  sum(pendiente) pendiente,   sum(prevision) prevision,  sum(compromiso ) compromiso  
  
FROM  
  migracion.qry_estructura e left join basicas.objetos on (objetos.obj = e.obj)  
  left join migracion.inicial_migracion i on ( i.clase = e.clase AND  i.programa = e.programa AND  i.actividad = e.actividad AND 
	i.obj = e.obj AND  i.ff = e.ff AND  i.of = e.of AND  i.dpto = e.dpto )  

  left join migracion.planfinanciero_migracion p on ( p.clase = e.clase AND  p.programa = e.programa AND  p.actividad = e.actividad AND 
	p.obj = e.obj AND  p.ff = e.ff AND  p.of = e.of AND  p.dpto = e.dpto )   


  left join migracion.qry_ejecucion_migracion j on ( j.clase = e.clase AND  j.programa = e.programa AND  j.actividad = e.actividad AND 
	j.obj = e.obj AND  j.ff = e.ff AND  j.of = e.of AND  j.dpto = e.dpto )  	 


  left join migracion.prevcom_migracion c on ( c.clase = e.clase AND  c.programa = e.programa AND  c.actividad = e.actividad AND 
	c.obj = e.obj AND  c.ff = e.ff AND  c.of = e.of AND  c.dpto = e.dpto )   

WHERE e.clase = v0 
    AND e.programa = v1  
    AND e.actividad = v2 
AND objetos.grupo = v3  
AND e.ff = v4  
AND e.of = v5  
AND e.dpto = v6  


group by   e.clase,   e.programa,   e.actividad,   objetos.grupo,   e.ff,   e.of,   e.dpto  
order by  objetos.grupo 



