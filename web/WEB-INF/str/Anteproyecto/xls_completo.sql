select m.*,  ' ' as separador, x.*,  ' ' as separador, a.* from 
(
SELECT tip_codigo, pro_codigo, sub_codigo, obj_codigo, fue_codigo, fin_codigo, dpt_codigo
FROM aplicacion.migradorxls
group by tip_codigo, pro_codigo, sub_codigo, obj_codigo, fue_codigo, fin_codigo, dpt_codigo
union 
SELECT tip_codigo, pro_codigo, sub_codigo, obj_codigo, fue_codigo, fin_codigo, dpt_codigo
FROM aplicacion.anteproyecto a 
group by tip_codigo, pro_codigo, sub_codigo, obj_codigo, fue_codigo, fin_codigo, dpt_codigo
) as m left join anteproyecto a 
on (a.tip_codigo = m.tip_codigo and a.pro_codigo = m.pro_codigo
and a.sub_codigo =  m.sub_codigo and a.obj_codigo = m.obj_codigo 
and a.fue_codigo = m.fue_codigo  and a.fin_codigo = m.fin_codigo 
and a.dpt_codigo = m.dpt_codigo) 
left join migradorxls x 
on (x.tip_codigo = m.tip_codigo and x.pro_codigo = m.pro_codigo
and x.sub_codigo =  m.sub_codigo and x.obj_codigo = m.obj_codigo 
and x.fue_codigo = m.fue_codigo  and x.fin_codigo = m.fin_codigo 
and x.dpt_codigo = m.dpt_codigo) 
order by m.tip_codigo, m.pro_codigo, m.sub_codigo, m.obj_codigo, m.fue_codigo, m.fin_codigo, m.dpt_codigo
