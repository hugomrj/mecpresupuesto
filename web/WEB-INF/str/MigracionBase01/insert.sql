
INSERT INTO migracion.basefox_actual 
    (migracion,  
    tip_codigo, pro_codigo, sub_codigo, obj_codigo, fue_codigo, fin_codigo, dpt_codigo,     
    ley, modi, vig, 
    pl1, ej1, pl2, ej2, pl3, ej3, pl4, ej4, pl5, ej5, pl6, ej6, 
    pl7, ej7, pl8, ej8, pl9, ej9, pl10, ej10, pl11, ej11, pl12, ej12, 
    total_e, total_pl, planf, saldoprev, saldocom, 
    obligado, saldoplan,  pagado, oblipenpa, pac,  
    reserva, clasific, fecha_id ) 
VALUES( co0, 
    es1, es2, es3, es4, es5, es6, es7,     
    ii01, ii02, ii03,  
    pp01,ee01, pp02, ee02, pp03, ee03, pp04, ee04, pp05, ee05, pp06, ee06,      
    pp07,ee07, pp08, ee08, pp09, ee09, pp10, ee10, pp11, ee11, pp12, ee12,      
    zz01, zz02, zz03, zz04, zz05,   
    zz06, zz07, zz08, zz09, zz10,   
    zz11,  
    'cc02', fc) 
RETURNING id   

