

function BaseInicialSaldo(){
    
   this.tipo = "baseinicialsaldo";   
   this.recurso = "";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/contrataciones";   
   
   
   this.campoid=  'id';
   
   
   this.tablacampos =  [ 'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];
   
   
   
   this.etiquetas =  [ 'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];            
            
   this.tablaformat =  [ 'U', 'N', 'U', 'Z',
                        'D', 'C', 'C', 'N', 'N'];
   

      
   this.botones_lista = [ this.lista_new] ;
   //this.botones_form = "consulta-acciones";   
  
   
}





BaseInicialSaldo.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};




// this
BaseInicialSaldo.prototype.form_ini = function(obj) {    


    var clase = document.getElementById('clase');          
    clase.onblur  = function() {                
        clase.value  = fmtNum(clase.value);
    };     
    clase.onblur();       
            
    var programa = document.getElementById('programa');          
    programa.onblur  = function() {                
        programa.value  = fmtNum(programa.value);
    };     
    programa.onblur();       
            
    var actividad = document.getElementById('actividad');          
    actividad.onblur  = function() {                
        actividad.value  = fmtNum(actividad.value);
    };     
    actividad.onblur();       
            
    var objeto = document.getElementById('objeto');          
    objeto.onblur  = function() {                
        objeto.value  = fmtNum(objeto.value);
    };     
    objeto.onblur();       
            
    var ff = document.getElementById('ff');          
    ff.onblur  = function() {                
        ff.value  = fmtNum(ff.value);
    };     
    ff.onblur();       
            
    var of = document.getElementById('of');          
    of.onblur  = function() {                
        of.value  = fmtNum(of.value);
    };     
    of.onblur();       
            

    var dpto = document.getElementById('dpto');          
    dpto.onblur  = function() {                
        dpto.value  = fmtNum(dpto.value);
    };     
    dpto.onblur();       
            
    
    
    
    
    // boton enviar
    var btn_cab_buscar = document.getElementById('btn_cab_buscar');
    btn_cab_buscar.onclick = function(event) {     
    
        var ins = new BaseInicialSaldo();
        ins.consulta1_detalle_promesa(obj);    
        
        
        //var ins = new BaseInicialSaldo();
        ins.certificacion_presupuestaria_detalle_promesa(obj);        
        
        
    }
        
    
};






BaseInicialSaldo.prototype.form_validar = function() {    
    
    return true;
};







BaseInicialSaldo.prototype.main_form = function(obj) {    


    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html' )
        .then(response => {
            return response.text();
        })
        .then(data => {        
            document.getElementById('cab_form').innerHTML = data ;        
        })
        .then(data => {
            
            this.ultimafecha(obj);
            obj.form_ini(obj);                
            
        })
            





    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/lin.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('lin_form').innerHTML = data ;        
        //reflex.getTituloplu(obj);                                 
            
      })



};





BaseInicialSaldo.prototype.ultimafecha = function( obj  ) {    

    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var url = html.url.absolute() + '/api/basefoxfecha/ultimafecha'
        var xhr =  new XMLHttpRequest();      

        var metodo = "GET";                                     
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                var json = xhr.responseText;

                var ojson = JSON.parse(json) ; 
                
                var migracion_fecha = document.getElementById('migracion_fecha');                
                migracion_fecha.innerHTML = ojson["fecha"];
                
                var migracion_codigo = document.getElementById('migracion_codigo');                
                migracion_codigo.value = ojson["fecha_id"];
                
                loader.fin();
            }
        };

        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   
        xhr.setRequestHeader("token", localStorage.getItem('token'));           
        xhr.send( null );                       

    })

    return promise;

};




BaseInicialSaldo.prototype.consulta1_detalle_promesa = function( obj ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();
            
   
            var url = html.url.absolute() + '/api/migracionbasefox/linea/'
                +';clase='+document.getElementById("clase").value 
                +';prog='+document.getElementById("programa").value 
                +';acti='+document.getElementById("actividad").value 
                +';obj='+document.getElementById("objeto").value 
                +';ff='+document.getElementById("ff").value 
                +';of='+document.getElementById("of").value                 
                +';dpto='+document.getElementById("dpto").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    var vjson = xhr.responseText;

                    var ojson = JSON.parse( vjson ) ; 
                    vjson = JSON.stringify(ojson['datos']) ;  
                    ojson = JSON.parse( vjson ) ; 

  
                    // cargar primera linea
                    if (vjson == "[]"){
                        
                        document.getElementById("l1_clase").innerHTML = "0";
                        document.getElementById("l1_programa").innerHTML = "0" ;
                        document.getElementById("l1_actividad").innerHTML = "0" ;
                        
                        document.getElementById("l1_obj").innerHTML = "0" ;
                        document.getElementById("l1_ff").innerHTML = "0" ;
                        document.getElementById("l1_of").innerHTML = "0" ;
                        document.getElementById("l1_dpto").innerHTML = "0" ;
                        document.getElementById("l1_dpto").innerHTML = "0" ;
                        
                        document.getElementById("l1_inicial").innerHTML = "0" ;
                        document.getElementById("l1_modifi").innerHTML = "0" ;                        
                        document.getElementById("l1_vigente").innerHTML = "0" ;
                        document.getElementById("l1_saldoplan").innerHTML = "0" ;
                    }
                    else {
                        

                        document.getElementById("l1_clase").innerHTML = ojson[0]["tip_codigo"]; 
                        document.getElementById("l1_programa").innerHTML = ojson[0]["pro_codigo"] ;
                        document.getElementById("l1_actividad").innerHTML = ojson[0]["sub_codigo"];
                        
                        document.getElementById("l1_obj").innerHTML = ojson[0]["obj_codigo"];
                        document.getElementById("l1_ff").innerHTML = ojson[0]["fue_codigo"];
                        document.getElementById("l1_of").innerHTML = ojson[0]["fin_codigo"];                        
                        document.getElementById("l1_dpto").innerHTML = ojson[0]["dpt_codigo"];
                        
                        document.getElementById("l1_inicial").innerHTML =  ojson[0]["ley"];
                        document.getElementById("l1_modifi").innerHTML = ojson[0]["modi"];
                        document.getElementById("l1_vigente").innerHTML = ojson[0]["vig"];
                        document.getElementById("l1_saldoplan").innerHTML = ojson[0]["saldoplan"];
                        

                    }
                     


                    var obj = new Object();
                    obj.tablaformat = ['N','N','N','N','N','N','N','N','N','N','N'  ];    
                    tabla.id = "lin1-tabla";
                    tabla.formato(obj);  
  
                         resolve( xhr );





                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};








BaseInicialSaldo.prototype.certificacion_presupuestaria_detalle_promesa = function( obj ) {    

    const promise = new Promise((resolve, reject) => {

            loader.inicio();            
               
            var url = html.url.absolute() + '/api/certificacion_query/baseinicialsaldo/'
                +';clase='+document.getElementById("clase").value 
                +';prog='+document.getElementById("programa").value 
                +';acti='+document.getElementById("actividad").value 
                +';obj='+document.getElementById("objeto").value 
                +';ff='+document.getElementById("ff").value 
                +';of='+document.getElementById("of").value                 
                +';dpto='+document.getElementById("dpto").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    var vjson = xhr.responseText;

                    var ojson = JSON.parse( vjson ) ; 
                    vjson = JSON.stringify(ojson['datos']) ;  
                    //ojson = JSON.parse( vjson ) ; 
  
                                        
                    var jdet = JSON.stringify(ojson['datos']);

                    tabla.json = jdet;    

                    tabla.id = "lin2-tabla";
                    tabla.linea = "id";
                    tabla.tbody_id = "lin2-tb";
                    tabla.campos = ['agno', 'fecha_emision', 'nombre',
                        'monto_total' , 'descripcion' , 'cc', 'monto'
                        ];                

                    tabla.gene();   

                    var obj = new Object();
                    obj.tablaformat = ['N','N','N','N','N','N','N','N','N' ];    
                    tabla.id = "lin1-tabla";
                    tabla.formato(obj);
  
  
                         resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};


