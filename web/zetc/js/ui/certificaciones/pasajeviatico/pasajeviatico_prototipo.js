
function PasajeViatico(){
    
   this.tipo = "pasajeviatico";   
   this.recurso = "pasajesviaticos";   
   this.value = 0;
   
   this.json_descrip = "nombre";
   
   this.dom="";
   
   this.carpeta=  "/certificaciones";   
   
   
   this.titulosin = ""
   this.tituloplu = ""   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['fecha', 'dependencia.nombre', 'expediente',
                'saldo_disponible', 'monto_requerido', 'monto_restante'];
   
   this.etiquetas =  [''];                                  
    
   this.tablaformat = ['D', 'C', 'R', 'N', 'N','N' ];                                  
   
   this.tbody_id = "objeto-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "objeto-acciones";   
         
   this.parent = null;
  
  
}



PasajeViatico.prototype.new = function( obj , mes, 
                            clase, programa, actividad, objeto, ff, of, dpto,
                            saldo_disponible ) {                
    

    var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html' 

    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {
      
        document.getElementById('arti_form').innerHTML = data ;
              
              
        var pasajeviatico_mes = document.getElementById('pasajeviatico_mes');
        pasajeviatico_mes.value = mes;
        pasajeviatico_mes.disabled = true;
        
        
        document.getElementById('pasajeviatico_clase').value = clase;
        document.getElementById('pasajeviatico_programa').value = programa;
        document.getElementById('pasajeviatico_actividad').value = actividad;
        document.getElementById('pasajeviatico_obj').value = objeto;
        document.getElementById('pasajeviatico_ff').value = ff;
        document.getElementById('pasajeviatico_of').value = of;
        document.getElementById('pasajeviatico_dpto').value = dpto;
        
        document.getElementById('pasajeviatico_saldo_disponible').value = saldo_disponible;
        
        
        obj.form_boolean();                
        obj.form_ini_abm(obj);   
        obj.form_ini_abm_accion(obj);   
                
        document.getElementById('linea_obligado').style.display = 'none';
        
        
        //  botones de alta        
        reflex.acciones.button_add_promise(obj);       

        boton.objeto = "" + obj.tipo;        
        boton.ini(obj);
        document.getElementById( obj.tipo +'-acciones' ).innerHTML 
                    =  boton.basicform.get_botton_add();    





        var btn_objeto_guardar = document.getElementById('btn_pasajeviatico_guardar');
        btn_objeto_guardar.addEventListener('click',
            function(event) {               


                if ( obj.form_validar()) {
                    
                    loader.inicio();

                    form.name = "form_"+obj.tipo;

                    var xhr =  new XMLHttpRequest();     
                    var url = arasa.html.url.absolute()  +"/api/"+ obj.recurso;   

                    var metodo = "POST";        
                    ajax.json = form.datos.getjson() ;
                    
                 

                    xhr.open( metodo.toUpperCase(),   url,  true );      

                    xhr.onreadystatechange = function () {
                        if (this.readyState == 4 ){

                            //ajax.headers.get();
                            ajax.local.token =  xhr.getResponseHeader("token") ;            
                            localStorage.setItem('token', xhr.getResponseHeader("token") );     
                            //ajax.state = xhr.status;


                            switch (xhr.status) {

                              case 200:

                                    msg.ok.mostrar("registro agregado");          
                                    reflex.data  = xhr.responseText;
                                                                                        
                                    //reflex.form_id_promise( obj, reflex.getJSONdataID( obj.campoid ) ) ;  
                                    
                                    //alert(reflex.getJSONdataID( obj.campoid ) );
                                    
                                    obj.form_id_promise( obj, reflex.getJSONdataID( obj.campoid ) );   
                                    
                                    break; 


                              case 401:

                                    window.location =  html.url.absolute();         
                                    break;                             


                              case 500:

                                    msg.error.mostrar(  reflex.data  );          
                                    break; 


                              case 502:                              
                                    msg.error.mostrar(  reflex.data  );          
                                    break;                                 


                              default: 
                                msg.error.mostrar("error de acceso");           
                            }                        

                            //arasa.html.url.redirect( xhr.status );                            
                            loader.fin();


                        }
                    };
                    xhr.onerror = function (e) {                    
                        reject( xhr );                 
                    };  


                    var type = "application/json";
                    xhr.setRequestHeader('Content-Type', type);   
                    //ajax.headers.set();
                    xhr.setRequestHeader("token", localStorage.getItem('token'));           

                    xhr.send( ajax.json );      


                }

            },
            false
        )





        var btn_objeto_cancelar = document.getElementById('btn_pasajeviatico_cancelar');
        btn_objeto_cancelar.addEventListener('click',
            function(event) {                     
                alert("cancelar")
            },
            false
        );    

              

        
            
      })







    // reflex.form_new( obj );         
      
    /*
    reflex.acciones.button_add_promise(obj);          
    
    var certificacionplanfin_certificacion_tipo 
            = document.getElementById('certificacionplanfin_certificacion_tipo');          
    */
   
   
//    certificacionplanfin_certificacion_tipo.value = tipo_certi;



    
};








PasajeViatico.prototype.form_ini = function(obj) {    
    
    
    var mes = new Mes(); 
    mes.combobox("mes");    
    
    
    var btn_buscare = document.getElementById('btn_buscare') ;
    btn_buscare.onclick = function(){                                  

        var obj = new CertificacionPlanfin();                 

        obj.acctionresul = function(id) {    
            
            gestru = id;
            
            // pasara a una funcion
            var obj2 = new PasajeViatico();      
            obj2.cabecera_detalle(obj2, id);                
            
        };            
        
        modal.ancho = 700;
        
        obj.url = html.url.absolute() + obj.carpeta +'/'+ 
                    obj.tipo + '/htmf/lista_busqueda.html'; 
        
        var va = 1;
        obj.get_api = html.url.absolute()+'/api/' + obj.recurso + '/lista/' + va;   
        
        busqueda.modal.custom(obj);        
        document.getElementById( obj.tipo + '_paginacion' ).innerHTML = "";   
        

        var saldo_disponible = document.getElementById('saldo_disponible');            
         saldo_disponible.onblur  = function() {                  
             saldo_disponible.value = fmtNum(saldo_disponible.value);      
        };      
        saldo_disponible.onblur();           
        
    };       
    
    
    
    var combomes = document.getElementById('mes');    
    combomes.onchange = function(){
            // pasara a una funcion
            var obj2 = new PasajeViatico();      
            obj2.cabecera_detalle(obj2, document.getElementById('id_lineae').value);
        
    };
    
    
    
    // buscar
    var busquedat = document.getElementById('busquedat') ;  
    busquedat.onkeypress = function(event) {                
        
        if (event.keyCode == 13) {                
            
            // funcion filtrar  certificaciones
            var instance = new PasajeViatico(); 
            var buscar = document.getElementById("busquedat" ).value;
            instance.lista_estructura(instance, 1, buscar);

        };                
        
    };      
    
        
    
    
    
    obj.detalles_html(obj);
    
    
    
};



PasajeViatico.prototype.cabecera_detalle = function(obj, id) {

            
    var url = html.url.absolute()+'/api/' + 'certificacionplanfin' + '/'+id;   
    ajax.async.json( "get", url, null )
        .then(( xhr ) => {

            if (xhr.status == 200)
            {     

                var ojson = JSON.parse( xhr.responseText ) ;      


                document.getElementById('id_lineae').value =  id;

                document.getElementById('eclase').innerHTML =  ojson["clase"] ;
                document.getElementById('eprograma').innerHTML =  ojson["programa"] ;
                document.getElementById('eactividad').innerHTML =  ojson["actividad"] ;
                document.getElementById('eobj').innerHTML =  ojson["obj"] ;
                document.getElementById('eff').innerHTML =  ojson["ff"] ;
                document.getElementById('eof').innerHTML =  ojson["of"] ;
                document.getElementById('edpto').innerHTML =  ojson["dpto"] ;


                var combomes = document.getElementById('mes');  
                var nombremes = combomes.options[combomes.selectedIndex].text;
                nombremes = nombremes.toLowerCase() + "_saldo";
                document.getElementById('saldo_disponible').value =  ojson[nombremes] ;

                document.getElementById('saldo_disponible').onblur();            



                // funcion filtrar  certificaciones
                var instance = new PasajeViatico(); 
                var buscar = document.getElementById("busquedat" ).value;
                instance.lista_estructura(instance, 1, buscar);


            }
            else{
            }   
        })      






}




PasajeViatico.prototype.form_ini_abm = function(obj) {    
    
        
    var pasajeviatico_saldo_disponible = document.getElementById('pasajeviatico_saldo_disponible');            
     pasajeviatico_saldo_disponible.onblur  = function() {                  
         pasajeviatico_saldo_disponible.value = fmtNum(pasajeviatico_saldo_disponible.value);      
    };      
    pasajeviatico_saldo_disponible.disabled = true;
    pasajeviatico_saldo_disponible.onblur();          
    
    
    var pasajeviatico_monto_restante = document.getElementById('pasajeviatico_monto_restante');            
     pasajeviatico_monto_restante.onblur  = function() {                  
         pasajeviatico_monto_restante.value = fmtNum(pasajeviatico_monto_restante.value);      
    };      
    pasajeviatico_monto_restante.disabled = true;
    pasajeviatico_monto_restante.onblur();          
        
    
    
    var pasajeviatico_monto_requerido = document.getElementById('pasajeviatico_monto_requerido');            
     pasajeviatico_monto_requerido.onblur  = function() {                  
         pasajeviatico_monto_requerido.value = fmtNum(pasajeviatico_monto_requerido.value);    
         
         pasajeviatico_monto_restante.value = 
                parseInt(NumQP(pasajeviatico_saldo_disponible.value))
                - parseInt(NumQP(pasajeviatico_monto_requerido.value)) ;
        pasajeviatico_monto_restante.onblur();  
         
    };          
    pasajeviatico_monto_requerido.onblur();          
    


    var pasajeviatico_dependencia = document.getElementById('pasajeviatico_dependencia');            
     pasajeviatico_dependencia.onblur  = function() {          
         pasajeviatico_dependencia.value = fmtNum(pasajeviatico_dependencia.value);      
         pasajeviatico_dependencia.value = NumQP(pasajeviatico_dependencia.value);               
    };      
    pasajeviatico_dependencia.onblur();          
    
    
    
    
    var pasajeviatico_memo_numero = document.getElementById('pasajeviatico_memo_numero');            
     pasajeviatico_memo_numero.onblur  = function() {          
         pasajeviatico_memo_numero.value = fmtNum(pasajeviatico_memo_numero.value);      
         pasajeviatico_memo_numero.value = NumQP(pasajeviatico_memo_numero.value);               
    };      
    pasajeviatico_memo_numero.onblur();          
    
    
    
    var pasajeviatico_monto_obligado = document.getElementById('pasajeviatico_monto_obligado');            
     pasajeviatico_monto_obligado.onblur  = function() {          
         pasajeviatico_monto_obligado.value = fmtNum(pasajeviatico_monto_obligado.value);                         
    };      
    pasajeviatico_monto_obligado.onblur();          
    
    
   
        
    
    

};






PasajeViatico.prototype.form_ini_abm_accion = function(obj) {    
    
  
        var depar = new Departamento();         
        depar.combobox("pasajeviatico_departamento");                   
        
        // buscar dependencia
        var pasajeviatico_dependencia = document.getElementById('pasajeviatico_dependencia');   
        pasajeviatico_dependencia.onblur  = function() {                        

            pasajeviatico_dependencia.value = fmtNum(pasajeviatico_dependencia.value);      
            pasajeviatico_dependencia.value = NumQP(pasajeviatico_dependencia.value);      
            
            var depen = pasajeviatico_dependencia.value ;

            ajax.url =  html.url.absolute()+'/api/dependencias/'+depen;
                        
            ajax.async.get()
                .then(( xhr ) => {

                    if (xhr.status == 200)
                    {     
                        var ojson = JSON.parse(xhr.responseText) ;     

                        document.getElementById('pasajeviatico_dependencia').value = ojson['dependencia'];

                        document.getElementById('dependencia_descripcion_out').innerHTML = 
                                ojson['nombre'];
                    }
                    else{
                        document.getElementById('pasajeviatico_dependencia').value = 0;
                        document.getElementById('dependencia_descripcion_out').innerHTML = "";
                    }                  
                })            
         };    

        

        var ico_more_dependencia = document.getElementById('ico-more-dependencia');
        ico_more_dependencia.addEventListener('click',
            function(event) {     

                var obj = new Dependencia();      

                obj.acctionresul = function(id) {    
                    pasajeviatico_dependencia.value = id; 
                    pasajeviatico_dependencia.onblur(); 
                };       
                modal.ancho = 900;
                busqueda.modal.objeto(obj);
            },
            false
        );        
    
    
};







PasajeViatico.prototype.lista_estructura = function( obj, page, buscar ) { 
    
            

    var mes = document.getElementById('mes').value;
    var clase = document.getElementById('eclase').innerHTML;
    var programa = document.getElementById('eprograma').innerHTML;
    var actividad = document.getElementById('eactividad').innerHTML;
    var objeto = document.getElementById('eobj').innerHTML;
    var ff = document.getElementById('eff').innerHTML;
    var of = document.getElementById('eof').innerHTML;
    var dpto = document.getElementById('edpto').innerHTML;
            
                
    loader.inicio();


    var url = html.url.absolute()+'/api/' + "pasajesviaticos/lista/"
        +mes+"/"+clase+"/"+programa+"/"+actividad+"/"
        +objeto+"/"+ff+"/"+of+"/"+dpto+"?page="+page+"&q="+buscar;   



    ajax.async.json( "get", url, null )
        .then(( xhr ) => {

            loader.fin();

            if (xhr.status == 200)
            {     


                var ojson = JSON.parse( xhr.responseText ) ; 
                tabla.json = JSON.stringify(ojson['datos']) ;  


                tabla.ini(obj);                            
                tabla.gene();          
                tabla.formato(obj);


    // acciones de registros
    // tabla.set.tablaid(obj);     
    // tabla.lista_registro(obj, reflex.form_id_promise );                 
            
            
                tabla.set.tablaid(obj);     
                
                tabla.lista_registro(obj, 
                    function ( obj, id ){

                        /*
                        var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');
                        ordencompra_ordencompra.value =   id

                        ordencompra_ordencompra.value = fmtNum(ordencompra_ordencompra.value);      
                        ordencompra_ordencompra.value = NumQP(ordencompra_ordencompra.value);                      
                        cargar_datos ();        

                        modal.ventana_cerrar("vid");
                        */
                       
                       
                       obj.form_id_promise(obj, id);

                    }

                ); 

                var json_paginacion = JSON.stringify(ojson['paginacion']);
                
                obj.paginacion_html(obj, json_paginacion )
            
            }
            else {
            }                  

        })            

      


};



PasajeViatico.prototype.paginacion_html = function( obj , json ) {   


    arasa.html.paginacion.ini(json);

    document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
            = arasa.html.paginacion.gene();   

//    arasa.html.paginacion.move(obj, "", reflex.form_id);    
        

    var listaUL = document.getElementById( obj.tipo + "_paginacion" );
    var uelLI = listaUL.getElementsByTagName('li');


    var pagina = 0;


    for (var i=0 ; i < uelLI.length; i++)
    {
        var lipag = uelLI[i];   

        if (lipag.dataset.pagina == "act"){                                     
            pagina = lipag.firstChild.innerHTML;
        }                    
    }



    for (var i=0 ; i < uelLI.length; i++)
    {
        var datapag = uelLI[i].dataset.pagina;     

        if (!(datapag == "act"  || datapag == "det"  ))
        {
            uelLI[i].addEventListener ( 'click',
                function() {                                      

                    switch (this.dataset.pagina)
                    {
                       case "sig": 
                               pagina = parseInt(pagina) +1;
                               break;                                                                          

                       case "ant":                                     
                               pagina = parseInt(pagina) -1;
                               break;

                       default:  
                               pagina = this.childNodes[0].innerHTML.toString().trim();
                               break;
                    }
                    pagina = parseInt( pagina , 10);                                                                       

                    //arasa.html.paginacion.pagina = pagina;
                    
                    // funcion filtrar  certificaciones
                    var instance = new PasajeViatico();                     
                    var buscar = document.getElementById("busquedat" ).value;                    
                    instance.lista_estructura(instance, pagina, buscar);                    

                    //tabla.refresh_promise( obj, pagina, busca, fn  ) ;

                },
                false
            );                
        }            
    }    


    
    
    
}












PasajeViatico.prototype.form_validar = function() {   
    
    
    var pasajeviatico_fecha = document.getElementById('pasajeviatico_fecha');
    if (pasajeviatico_fecha.value == ""){
        msg.error.mostrar("Error en fecha ");                    
        pasajeviatico_fecha.focus();
        pasajeviatico_fecha.select();                                       
        return false;        
    }    
    
    
    
    var pasajeviatico_expediente = document.getElementById('pasajeviatico_expediente');
    if (pasajeviatico_expediente.value == ""){
        
        msg.error.mostrar("Falta numero de expediente ");                    
        pasajeviatico_expediente.focus();
        pasajeviatico_expediente.select();                                       
        return false;        
    }
    

    
    var pasajeviatico_dependencia = document.getElementById('pasajeviatico_dependencia');    
    if (parseInt(NumQP(pasajeviatico_dependencia.value)) == 0 ) {
        
        msg.error.mostrar("Falta seleccionar Dependencia ");                    
        pasajeviatico_dependencia.focus();
        pasajeviatico_dependencia.select();                                       
        return false;        
    }
    
            
    
    
    
    return true;
};






PasajeViatico.prototype.detalles_html = function(obj) {   

    var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/detalle.html' 
    
    
    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {
      
        document.getElementById('objeto_detalle').innerHTML = data ;


        var btn_insertar = document.getElementById('btn_insertar');              
        btn_insertar.onclick = function( obj )
        {  
            
            var obj = new PasajeViatico();    
                        
            var mes = document.getElementById('mes').value;     
            var clase = document.getElementById('eclase').innerHTML;     
            var programa = document.getElementById('eprograma').innerHTML;     
            var actividad = document.getElementById('eactividad').innerHTML;     
            var objeto = document.getElementById('eobj').innerHTML;     
            var ff = document.getElementById('eff').innerHTML;     
            var of = document.getElementById('eof').innerHTML;     
            var dpto = document.getElementById('edpto').innerHTML;     
            
            
            var saldo_disponible =  document.getElementById('saldo_disponible').value;
            saldo_disponible = parseInt(NumQP(saldo_disponible));
            
            obj.new(obj, mes, 
                clase, programa, actividad, objeto, ff, of, dpto,
                saldo_disponible
                );
            
        }; 
            
      })

}











PasajeViatico.prototype.main_form = function(obj ) {    

    var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html'     


    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {
      
        document.getElementById('arti_form').innerHTML = data ;

        obj.form_ini(obj );
            
      })


};







PasajeViatico.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['departamento'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}




PasajeViatico.prototype.preedit = function( obj  ) {                



        
};








PasajeViatico.prototype.combobox_tipo_certificacion = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/certificacionestipos/all' ;    
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['certificacion_tipo'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}




 
 
 


PasajeViatico.prototype.form_id = function( obj, id ) { 
    

    
console.log("prototype.form_id");


    
    var url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html';   
    
    
    fetch( url )
      .then( response => {
        return response.text();
      })
      .then(data => {
      
             document.getElementById( obj.dom ).innerHTML = data;
     

            var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   
            ajax.async.json( "get", url, null )
                .then(( xhr ) => {

                    if (xhr.status == 200)
                    {     
    //                    var ojson = JSON.parse(xhr.responseText) ;     

                        form.name = "form_" + obj.tipo;
                        form.json = xhr.responseText;   
                        
                        
                        form.disabled(false);
                        form.llenar();                
                        form.llenar_cbx(obj);   
  
                        obj.form_form_id_botones(obj, id);

                    }
                    else{

                    }                  

                })            
            
      })


};




PasajeViatico.prototype.form_form_id_botones = function( obj, id ) { 
       
        
        
        form.name = "form_certificacionplanfin";
        form.disabled(false);
        

        boton.ini(obj);
        boton.blabels = ["Modificar", "Lista"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;



        var btn_certificacionplanfin_modificar = document.getElementById('btn_certificacionplanfin_modificar');              
        btn_certificacionplanfin_modificar.onclick = function(  )
        {  
            /*
            var obj = new TransitoCosto();    
             */
            
            obj.form_editar(obj, id);
        };       


        var btn_certificacionplanfin_lista = document.getElementById('btn_certificacionplanfin_lista');              
        btn_certificacionplanfin_lista.onclick = function(  )
        {  
 
            var obj = new CertificacionPlanfin();    
            obj.dom = 'arti_form';

            var  tipo_certi =  document.getElementById('certificacionplanfin_certificacion_tipo').value;               


console.log(tipo_certi);

            serie_inicio(obj, tipo_certi);
            
            
            
        };       



}









PasajeViatico.prototype.post_form_id = function( obj  ) {                

    //certificacionplanfin-acciones
    var id = document.getElementById('certificacionplanfin_id').value;   
    
    obj.form_form_id_botones(obj, id);    
    
    
};






PasajeViatico.prototype.form_editar = function( obj, id ) { 

        form.name = "form_certificacionplanfin";
        
        obj.form_ini();
        
        form.disabled(true);
        
        boton.ini(obj);
        
        
        boton.blabels = ['Guardar', "Cancelar"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;        
        
        
       
        var btn_certificacionplanfin_guardar = document.getElementById('btn_certificacionplanfin_guardar');              
        btn_certificacionplanfin_guardar.onclick = function(  )
        {  
                        
            
            //var obj = new TransitoCosto();              

            if ( obj.form_validar())
            {
                
                var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   

                form.name = "form_"+obj.tipo;

                var data = form.datos.getjson() ;  
                
                var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   
                ajax.async.json( "put", url, data )
                    .then(( xhr ) => {


                        switch (xhr.status) {

                          case 200:

                                msg.ok.mostrar("registro agregado");          
                                reflex.data  = data;
                                //reflex.form_id( obj, reflex.getJSONdataID( obj.campoid ) ) ;       

                                obj.dom = 'arti_form';
                                obj.form_id( obj, id );
                                
                                break; 

                          case 401:
                                window.location = html.url.absolute();         
                                break;                             

                          case 500:
                                msg.error.mostrar( data );          
                                break; 

                          case 502:                              
                                msg.error.mostrar( data );          
                                break;                                 

                          default: 
                            msg.error.mostrar("error de acceso");           
                        }   


                    })            
                

            }

            
        };       
        
        
        

        var btn_certificacionplanfin_cancelar = document.getElementById('btn_certificacionplanfin_cancelar');              
        btn_certificacionplanfin_cancelar.onclick = function(  )
        {  
            //obj.form_form_id_botones(obj);
        };       

    
};







PasajeViatico.prototype.form_id_promise = function( obj, id ){    
    
    
 
    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var comp = window.location.pathname;                 
        var path =  comp.replace(arasa.html.url.absolute() , "");

        var xhr =  new XMLHttpRequest();            
        var url = arasa.html.url.absolute() +"/api/"+ obj.recurso + '/'+id;   


        var metodo = "GET";                         
        ajax.json  = null;
        xhr.open( metodo.toUpperCase(),   url,  true );      


        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){


                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     
                ajax.state = xhr.status;

                var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html' 
                
                fetch(url)
                    .then( response => {
                        return response.text();
                    })
                    .then(data => {
                        document.getElementById('arti_form').innerHTML = data ;

                        // cargar datos 
                        form.name = "form_" + obj.tipo;
                        form.json = xhr.responseText;   
                        

                        form.disabled(false);
                        obj.form_llenar(obj, xhr.responseText )
                

                        obj.form_ini_abm(obj);       
                        
                        reflex.acciones.button_reg_promise(obj);
                        
                        boton.ini(obj);
                        boton.blabels =  ['Modificar', 'Eliminar' ,'Lista'] ;
                        var strhtml =  boton.get_botton_base();    
                        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;

                    
                    
                        var btn_pasajeviatico_modificar = document.getElementById('btn_pasajeviatico_modificar');              
                        btn_pasajeviatico_modificar.onclick = function(  )
                        {  
                            obj.editar_acciones(obj, id);                            
                        };       
          
                              
                    

                        var btn_pasajeviatico_eliminar = document.getElementById('btn_pasajeviatico_eliminar');              
                        btn_pasajeviatico_eliminar.onclick = function(  )
                        {                              
                            obj.eliminar_acciones(obj, id);
                        };       
          
          


                    

                        var btn_pasajeviatico_lista = document.getElementById('btn_pasajeviatico_lista');              
                        btn_pasajeviatico_lista.onclick = function(  )
                        {  
                            //obj.form_form_id_botones(obj);
                            obj.atras_lista(obj);
                        };       
          
          
                    })



                resolve( xhr );
                loader.fin();

            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        xhr.setRequestHeader("path", path );
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( ajax.json  );                       

    })

    return promise;

    
};









PasajeViatico.prototype.form_llenar = function( obj, json  ) {                

    var ojson = JSON.parse(form.json) ; 
    var campos = document.getElementById( form.name ).getElementsByTagName("input");    

    for(var i=0; i< campos.length; i++) {            
        var c = campos[i];                      

        if (typeof(c.dataset.foreign) === "undefined") {

            if (c.type == "text"  ||  c.type == "hidden"  )
            {                
                if (c.className == "num")
                {
                    c.value  = ojson[c.name]; 
                }
                else
                {  
                    c.value  = ojson[c.name]; 
                    if (c.value === 'undefined') {
                        c.value  = ""; 
                    }  
                }                
            }
            else
            {

                var type= c.type;
                switch(type) {

                    case "date":                                                 
                        c.value  = jsonToDate( ojson[c.name] );   
                        break;                        

                    case "password":                                  
                        c.value  = ojson[c.name];                     
                        break;

                    case "radio":                                  
                        //c.value  = oJson[c.name];         
                        form.radios.databool(c.name, ojson[c.name] );
                        break;

                    default:
                        //code block
                }                           
            }            
        }     
    }


    // mes
    document.getElementById('pasajeviatico_mes' ).value  = ojson["mes"]["mes"];
    
    document.getElementById('pasajeviatico_dependencia' ).value  
            = ojson["dependencia"]["dependencia"];
    
    document.getElementById('dependencia_descripcion_out' ).innerHTML  
            = ojson["dependencia"]["nombre"];
    
                        
    var opt = document.createElement('option');            
    opt.value = ojson["departamento"]["departamento"];
    opt.innerHTML = ojson["departamento"]["nombre"];
    document.getElementById('pasajeviatico_departamento').appendChild(opt);    
    
    
    
     obj.form_boolean(json);
    
    
    /*
    var combos = document.getElementById( form.name ).getElementsByTagName("select");  
    
    for (var y=0; y< combos.length; y++) {

        if (combos[y].dataset.vista === "boolean" ) {     

            console.log(combos[y].name);

        }        
        
    }
    */
            
        
};



PasajeViatico.prototype.atras_lista = function( obj ) {                

    //var obj = new PasajeViatico();                    
    obj.dom = 'arti_form';
    obj.main_form(obj);       
    
    //obj.cabecera_detalle(obj, gestru);
                        
    

};




PasajeViatico.prototype.eliminar_acciones = function( obj, id ){      
    

    boton.ini(obj);
    boton.blabels = ["Eliminar", "Cancelar"]
    var strhtml =  boton.get_botton_base();
    document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;
    
    
    
    
    var btn_pasajeviatico_eliminar = document.getElementById('btn_pasajeviatico_eliminar');              
    btn_pasajeviatico_eliminar.onclick = function(  )
    {  
        
        loader.inicio();



        form.name = "form_"+obj.tipo;

        var xhr =  new XMLHttpRequest();     
        var url = arasa.html.url.absolute()  +"/api/"+ obj.recurso +"/"+id;   

        var metodo = "DELETE";        
        

        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     

                switch (xhr.status) {

                  case 200:

                        msg.ok.mostrar("registro eliminado");          
                        reflex.data  = xhr.responseText;

                        // ir a lista. de se posible filtrado
                        obj.atras_lista(obj);

                        

                        break; 

                  default: 
                    msg.error.mostrar("error de acceso");           
                }                        

                //arasa.html.url.redirect( xhr.status );                            
                loader.fin();


            }
        };
        xhr.onerror = function (e) {                    
            reject( xhr );                 
        };  


        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);           
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( null );      



        
        
        
    };       
    

    var btn_pasajeviatico_cancelar = document.getElementById('btn_pasajeviatico_cancelar');              
    btn_pasajeviatico_cancelar.onclick = function(  )
    {  
        alert("cancelar")
    };       




}






PasajeViatico.prototype.editar_acciones = function( obj, id ){      
    
    // debloquear botones    
    form.name = "form_" + obj.tipo;
    form.disabled(true);        
    obj.form_ini_abm(obj);      
    obj.form_ini_abm_accion(obj);   
    
    document.getElementById('pasajeviatico_monto_requerido').disabled =  true;              
    
    form.mostrar_foreign();
    
        
    
    boton.ini(obj);
    boton.blabels = ["Editar", "Cancelar"]
    var strhtml =  boton.get_botton_base();
    document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;
    
    
    var btn_pasajeviatico_editar = document.getElementById('btn_pasajeviatico_editar');              
    btn_pasajeviatico_editar.onclick = function(  )
    {  
        
        loader.inicio();



        form.name = "form_"+obj.tipo;

        var xhr =  new XMLHttpRequest();     
        var url = arasa.html.url.absolute()  +"/api/"+ obj.recurso +"/"+id;   

        var metodo = "PUT";       
        var json = form.datos.getjson() ;

console.log(json);

        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     

                switch (xhr.status) {

                  case 200:

                        msg.ok.mostrar("registro editado");          
                        reflex.data  = xhr.responseText;

                        // ir a lista. de se posible filtrado
                        // obj.atras_lista(obj);

                         obj.form_id_promise(obj, id);

                        break; 

                  default: 
                    msg.error.mostrar("error de acceso");           
                }                        

                //arasa.html.url.redirect( xhr.status );                            
                loader.fin();


            }
        };
        xhr.onerror = function (e) {                    
            reject( xhr );                 
        };  


        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);           
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( json );      



        
        
        
    };       
    

    var btn_pasajeviatico_cancelar = document.getElementById('btn_pasajeviatico_cancelar');              
    btn_pasajeviatico_cancelar.onclick = function(  )
    {          
        obj.form_id_promise(obj, id);
    };       




}








PasajeViatico.prototype.form_boolean = function(json) {     
            
        
    var selbool = document.getElementById("pasajeviatico_obligado");
                
    if (json == null){
        
    
        var opt = document.createElement('option');            
        opt.value = true;
        opt.innerHTML = "Si";                        
        selbool.appendChild(opt);   

        var opt = document.createElement('option');            
        opt.value = false;
        opt.innerHTML = "No";                        
        selbool.appendChild(opt);               
        
        
    }
    else{        
        console.log("bolean no es  null");
        
        var ojson = JSON.parse(json) ; 
        
        var p = ojson["obligado"]; 
        
        if (p == true) {

            var opt = document.createElement('option');            
            opt.value = true;
            opt.innerHTML = "Si";                        
            selbool.appendChild(opt);               
            
            var opt = document.createElement('option');            
            opt.value = false;
            opt.innerHTML = "No";                        
            selbool.appendChild(opt);               
            
        }
        
        if (p == false) {

            var opt = document.createElement('option');            
            opt.value = false;
            opt.innerHTML = "No";                        
            selbool.appendChild(opt);               
            
            var opt = document.createElement('option');            
            opt.value = true;
            opt.innerHTML = "Si";                        
            selbool.appendChild(opt);               
            
        }
        
        
        
        
        
    }
            

    
           

}


