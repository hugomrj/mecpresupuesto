

function Dependencia(){
    
   this.tipo = "dependencia";   
   this.recurso = "dependencias";   
   this.value = 0;
   
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/basica";   
   
   
   this.titulosin = "Dependencia"
   this.tituloplu = "Dependencias"   
      
   
   this.campoid=  'dependencia';
   this.tablacampos =  ['dependencia', 'codigo', 'nombre'];
   
   this.etiquetas =  ['Dependencia', 'Codigo', 'Nombre'];                                  
    
   this.tablaformat = ['C', 'C'];                                  
   
   this.tbody_id = "dependencia-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "dependencia-acciones";   
         
   this.parent = null;
   
   
}





Dependencia.prototype.new = function( obj  ) {                


    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);  
    
    
};





Dependencia.prototype.form_ini = function() {    
    
    var dependencia_codigo = document.getElementById('dependencia_codigo');          
    dependencia_codigo.onblur  = function() {                
        dependencia_codigo.value  = fmtNum(dependencia_codigo.value);
    };     
    dependencia_codigo.onblur();        
    
    
};






Dependencia.prototype.form_validar = function() {    
    
    return true;
};










Dependencia.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            


        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Dependencia.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['departamento'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}




Dependencia.prototype.preedit = function( obj  ) {                

    //document.getElementById('dependencia_dependencia').disabled = false; 
    
    //alert("desbloquear dependencia codigo")
        
};



