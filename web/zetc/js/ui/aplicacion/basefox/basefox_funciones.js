

   
function form_inicio(){    


//console.log(html.url.absolute()+'/contrataciones/ccmigracion/htmf/form_file.html');


    ajax.url = html.url.absolute()+'/aplicacion/basefox/htmf/form_file.html'; 
    ajax.metodo = "GET";            
    document.getElementById( "divroot" ).innerHTML =  ajax.public.html();       



     
     // migracion_file  
    var migracion_file = document.getElementById( "migracion_file" );        
    migracion_file.onchange = function() {
        
        
        var file = document.getElementById("migracion_file").files[0].name;
        
        var fechafile = file.substr(0,file.length - 5);
        var fechafile = fechafile.substr(fechafile.length - 10, fechafile.length );
                
        //SITUACION PRESUPUESTARIA 07-11-2022
        
        const [day, month,  year] = fechafile.split('-');
        
        const date = new Date(+year, +month - 1, +day);
        console.log(date); // 👉️ Sat Sep 24 2022
        
        var strDate = date.toISOString().substring(0,10);
              
        document.getElementById( "fecha" ).value =  strDate;
      
    };    
    
    

     
     

    var btn_migracion_enviar = document.getElementById('btn_migracion_enviar');         
    btn_migracion_enviar.onclick = function( ) {  
        
        
        if (migracion_validar()){


            var file = document.getElementById("migracion_file").files[0];            
            var fecha = document.getElementById("fecha");
            
            //var nombre_archivo = file.name;

            //var form = document.getElementById('form_migracion');
            //var formdata = new FormData(form);
            var formdata = new FormData();
            formdata.append("file", file);        
            formdata.append("fecha", fecha.value);        
            
            // agregar fecha en el formdata

            migracion_proceso_promesa( formdata )
                .then(( xhr ) => {

                    var valor = xhr.responseText;                
            
                    /*
                    if (!isNaN(valor)) {
                        form_cc_datos_migrados( valor );
                    }
                    */
                })
                .catch(( xhr ) => {                                         
                    console.log(xhr.message);                    
                });             
 
        
        }




    }
    
    
    
}
        
        
        
        
      
      
  
function migracion_validar( ) {        
    
        
    var migracion_file = document.getElementById('migracion_file'),
    extension = migracion_file.value.substring(
            migracion_file.value.lastIndexOf('.')
            ,migracion_file.value.length);
    
    
    
    if(migracion_file.getAttribute('accept').split(',').indexOf(extension) < 0) {    
        msg.error.mostrar('Archivo inválido. No se permite la extensión ' + extension);
        return false;        
    }    
    
    

    var migracion_fecha = document.getElementById('fecha');    
    if (migracion_fecha.value == "")         
    {
        msg.error.mostrar("falta fecha");           
        return false;
    }    

    
    
    return true;    
}
   
         
      
      
      
        
        

function migracion_proceso_promesa( formdata ){    

    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var xhr = new XMLHttpRequest();            
        var url = html.url.absolute()+'/Migracion/Base/01'; 
        

        var metodo = "POST";                                 
        xhr.open( metodo.toUpperCase(),   url,  true );      


        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                loader.fin();
                resolve( xhr );
                        
            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        
        //xhr.setRequestHeader("nombre_archivo",nombre_archivo );
        
        var type = "application/x-www-form-urlencoded";
        //xhr.setRequestHeader('Content-Type', type);                 
        
        xhr.send(formdata);


    })

    return promise;

}








function get_datos_cc( id  ){    

    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var xhr = new XMLHttpRequest();            
        var url = html.url.absolute()+'/api/codigoscontrataciones/codigo/'+id; 
        
        var metodo = "GET";                                 
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                form_cargar_datos ( xhr.responseText  );
                
                loader.fin();
                resolve( xhr );
                        
            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);           
        //xhr.setRequestHeader("token", localStorage.getItem('token'));           
        xhr.send( null );       


    })

    return promise;

}







function form_cc_datos_migrados ( id  ){    
    

    ajax.url = html.url.absolute()+'/contrataciones/codigo/htmf/form.html'; 
    ajax.metodo = "GET";            
    document.getElementById( "divroot" ).innerHTML =  ajax.public.html();  


    ajax.url = html.url.absolute()+'/contrataciones/codigo/htmf/det.html'; 
    ajax.metodo = "GET";            
    document.getElementById( "cetificacion_tabla" ).innerHTML =  ajax.public.html();  

    // prueba ws        
    get_datos_cc (id);

}







function form_cargar_datos ( json  ){    
    
    var ojson = JSON.parse(json) ; 
    
    /*
    document.getElementById( "codigo_cc" ).value 
            = ojson["datos"][0]["cc"]; 
    */
    
    var ocab = JSON.stringify(ojson['datos'][0]);
    //var jcab = JSON.parse(ocab) ; 
    
    form.json = ocab;
    form.name = "form_codigo"
    form.campos = ["cc", "fecha_emision", "nombre", "domicilio_legal", "nombre_fantasia", 
                    "representante_legal", "pais_origen", "telefono", "email",
                    "ruc", "nivel_entidad", "entidad", "unidad_compradora",
                    "tipo_procedimiento", "descripcion", "numero_pac",
                    "tipo_contrato", "numero", "monto_total", "moneda",
                    "fecha"];
                
    form.llenar();
    
    form.disabled( false );
    
    
    // cargar tabla detalles


    var jdet = JSON.stringify(ojson['detalles']);


    tabla.json = jdet;    
    
    
        tabla.id = "consulta-tabla";
        tabla.linea = "codigo_contratacion";
        tabla.tbody_id = "consulta-tb";
        tabla.campos = ['tp', 'pg', 'sp', 'py', 'obj', 'ff', 'of', 
                        'dpt', 'monto'];                

    
    tabla.gene();   
    
    var obj = new Object();
    obj.tablaformat = ['N','N','N', 'N','N','N','N','N','N'];                       
    tabla.formato(obj);

    


}
