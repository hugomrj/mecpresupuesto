/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.presupuesto;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */
public class PresupuestoRS {
    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
        
        
        
    
    public PresupuestoRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
    

    
    
    
    
    
    public ResultSet  xls_all (  ) throws Exception {

            statement = conexion.getConexion().createStatement();     

            String sql = new PresupuestoSQL().xls_all();

            resultset = statement.executeQuery(sql);     
           
            conexion.desconectar();                
            return resultset;                 
            
    }
             
    
    public ResultSet  xls_neto (  ) throws Exception {

            statement = conexion.getConexion().createStatement();      

            String sql = new PresupuestoSQL().xls_neto();

            resultset = statement.executeQuery(sql);     
           
            conexion.desconectar();                
            return resultset;                 
            
    }
             
    
    public ResultSet  xls_grupo (  ) throws Exception {

        statement = conexion.getConexion().createStatement();  
        String sql = new PresupuestoSQL().xls_grupo();
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
             

    public ResultSet  xls_objetos (  ) throws Exception {

        statement = conexion.getConexion().createStatement();  
        String sql = new PresupuestoSQL().xls_objetos();
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
             

    public ResultSet  xls_giraduriacentral (  ) throws Exception {

        statement = conexion.getConexion().createStatement();  
        String sql = new PresupuestoSQL().xls_giraduriacentral();
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
             
    


    public ResultSet  qry_obj_ffof_giraduriacentral (  ) throws Exception {

        statement = conexion.getConexion().createStatement();  
        String sql = new PresupuestoSQL().qry_obj_ffof_giraduriacentral();
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
             
    
    

    public ResultSet  linea (  Integer n1, Integer n2, Integer n3, 
            Integer obj, Integer ff, Integer of, Integer dpto    
            ) throws Exception {

        statement = conexion.getConexion().createStatement();  
        
        String sql = new PresupuestoSQL().linea(
              n1, n2, n3,
                        obj, ff, of, dpto);
        
        
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
             
    

    

    public ResultSet  lineagrupo (  Integer n1, Integer n2, Integer n3, 
            Integer grupo, Integer ff, Integer of, Integer dpto    
            ) throws Exception {

        
        statement = conexion.getConexion().createStatement();  
        
        String sql = new PresupuestoSQL().lineagrupo(
              n1, n2, n2,
                        grupo, ff, of, dpto);
                
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
             
    

    

    
        
}
