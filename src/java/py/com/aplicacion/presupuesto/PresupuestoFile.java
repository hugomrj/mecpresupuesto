/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.presupuesto;

import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.file.FileXls;
import nebuleuse.file.FileXlsx;


/**
 *
 * @author hugo
 */
public class PresupuestoFile {
    
    
    
    
    
    public  FileXlsx hoja01 ( FileXlsx filexlsx ) throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("clase");
            cabecera.add("programa");
            cabecera.add("actividad");
            cabecera.add("obj");            
                        
            
            cabecera.add("ff");
            cabecera.add("of");
            cabecera.add("dpto");
            cabecera.add("obj_decripcion");

            cabecera.add("inicial");
            cabecera.add("modifi");
            cabecera.add("vigente");
            
            cabecera.add("pla1");
            cabecera.add("eje1");
            cabecera.add("pla2");
            cabecera.add("eje2");
            cabecera.add("pla3");
            cabecera.add("eje3");
            cabecera.add("pla4");
            cabecera.add("eje4");
            cabecera.add("pla5");
            cabecera.add("eje5");
            cabecera.add("pla6");
            cabecera.add("eje6");
            
            cabecera.add("pla7");
            cabecera.add("eje7");
            cabecera.add("pla8");
            cabecera.add("eje8");
            cabecera.add("pla9");
            cabecera.add("eje9");
            cabecera.add("pla10");
            cabecera.add("eje10");
            cabecera.add("pla11");
            cabecera.add("eje11");
            cabecera.add("pla12");
            cabecera.add("eje12");
            
            
            cabecera.add("ejetotal");
            cabecera.add("pagado");            
            cabecera.add("pendiente");              
            cabecera.add("planificado");    
        
                        
            cabecera.add("total plan financiero");
            cabecera.add("saldo previsionado");            
            cabecera.add("saldo comprometido");
            cabecera.add("reserva");
            
                        
            cabecera.add("obligado");            
            cabecera.add("saldo plan financiero");       
            
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();

            
            
            campos.add("clase");
            campos.add("programa");
            campos.add("actividad");
            campos.add("obj");
            
            campos.add("ff");
            campos.add("of");
            campos.add("dpto");
            campos.add("obj_decripcion");
            
            campos.add("inicial");
            campos.add("modifi");
            campos.add("vigente");
   
            campos.add("pla1");
            campos.add("eje1");
            campos.add("pla2");
            campos.add("eje2");
            campos.add("pla3");
            campos.add("eje3");
            campos.add("pla4");
            campos.add("eje4");
            campos.add("pla5");
            campos.add("eje5");
            campos.add("pla6");
            campos.add("eje6");
            
            campos.add("pla7");
            campos.add("eje7");
            campos.add("pla8");
            campos.add("eje8");
            campos.add("pla9");
            campos.add("eje9");
            campos.add("pla10");
            campos.add("eje10");
            campos.add("pla11");
            campos.add("eje11");
            campos.add("pla12");
            campos.add("eje12");
            
            campos.add("ejetotal");
            campos.add("pagado");            
            campos.add("pendiente");              
            campos.add("planificado");    
                        
            campos.add("platotal");
            campos.add("prevision");            
            campos.add("compromiso");
            campos.add("reserva");
                        
            campos.add("obligado");            
            campos.add("saldo_plan");       

            
            
            filexlsx.setCampos(campos);                                

            PresupuestoRS rs;     
            rs = new PresupuestoRS();
            ResultSet resultset = rs.xls_all();

               
        
           
            filexlsx.newhoja("todo");
            filexlsx.writeCabecera(0);     
            
            
            filexlsx.writeContenido(resultset);           
            
            filexlsx.formato();
        
            return filexlsx;                                                              
            
    }
        
    
    
    public  FileXlsx hoja02 ( FileXlsx filexlsx ) throws SQLException, Exception {

        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("clase");
            cabecera.add("programa");
            cabecera.add("actividad");
            cabecera.add("obj");            
            
            cabecera.add("ff");
            cabecera.add("of");
            cabecera.add("dpto");
            cabecera.add("obj_decripcion");

            cabecera.add("inicial");
            cabecera.add("modifi");
            cabecera.add("vigente");
            
            cabecera.add("pla1");
            cabecera.add("eje1");
            cabecera.add("pla2");
            cabecera.add("eje2");
            cabecera.add("pla3");
            cabecera.add("eje3");
            cabecera.add("pla4");
            cabecera.add("eje4");
            cabecera.add("pla5");
            cabecera.add("eje5");
            cabecera.add("pla6");
            cabecera.add("eje6");
            
            cabecera.add("pla7");
            cabecera.add("eje7");
            cabecera.add("pla8");
            cabecera.add("eje8");
            cabecera.add("pla9");
            cabecera.add("eje9");
            cabecera.add("pla10");
            cabecera.add("eje10");
            cabecera.add("pla11");
            cabecera.add("eje11");
            cabecera.add("pla12");
            cabecera.add("eje12");            
            
            cabecera.add("ejetotal");
            cabecera.add("pagado");            
            cabecera.add("pendiente");              
            cabecera.add("planificado");            
                        
            cabecera.add("total plan financiero");
            cabecera.add("saldo previsionado");            
            cabecera.add("saldo comprometido");
            cabecera.add("reserva");
                        
            cabecera.add("obligado");            
            cabecera.add("saldo plan financiero");       
            

            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();

                        
            campos.add("clase");
            campos.add("programa");
            campos.add("actividad");
            campos.add("obj");
            
            campos.add("ff");
            campos.add("of");
            campos.add("dpto");
            campos.add("obj_decripcion");
            
            campos.add("inicial");
            campos.add("modifi");
            campos.add("vigente");

            campos.add("pla1");
            campos.add("eje1");
            campos.add("pla2");
            campos.add("eje2");
            campos.add("pla3");
            campos.add("eje3");
            campos.add("pla4");
            campos.add("eje4");
            campos.add("pla5");
            campos.add("eje5");
            campos.add("pla6");
            campos.add("eje6");
            
            campos.add("pla7");
            campos.add("eje7");
            campos.add("pla8");
            campos.add("eje8");
            campos.add("pla9");
            campos.add("eje9");
            campos.add("pla10");
            campos.add("eje10");
            campos.add("pla11");
            campos.add("eje11");
            campos.add("pla12");
            campos.add("eje12");
            
            campos.add("ejetotal");
            campos.add("pagado");            
            campos.add("pendiente");              
            campos.add("planificado");            
                        
            campos.add("platotal");
            campos.add("prevision");            
            campos.add("compromiso");
            campos.add("reserva");
                        
            campos.add("obligado");            
            campos.add("saldo_plan");       
            
            
            
            filexlsx.setCampos(campos);                                

            PresupuestoRS rs;     
            rs = new PresupuestoRS();
            ResultSet resultset = rs.xls_neto();

               
        
           
            filexlsx.newhoja("neto");
            filexlsx.writeCabecera(0);
            
            filexlsx.writeContenido(resultset);  
            
            filexlsx.formato();
        
        // Se salva el libro.
        try {

                String ruta = "";
    //            ruta = filexls.getFilePath();
                
      //          FileOutputStream file = new FileOutputStream( ruta );                
                
        //        filexls.getLibro().write(file);
                
          //      file.close();                
            
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        finally{
            return filexlsx;
        }
    }
        
    

    
    
    public  FileXlsx hoja03 ( FileXlsx filexlsx ) throws SQLException, Exception {

        
            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("clase");
            cabecera.add("programa");
            cabecera.add("actividad");
            cabecera.add("obj");            
            
            cabecera.add("ff");
            cabecera.add("of");
            cabecera.add("dpto");
            cabecera.add("obj_decripcion");

            cabecera.add("inicial");
            cabecera.add("modifi");
            cabecera.add("vigente");
            
            cabecera.add("pla1");
            cabecera.add("eje1");
            cabecera.add("pla2");
            cabecera.add("eje2");
            cabecera.add("pla3");
            cabecera.add("eje3");
            cabecera.add("pla4");
            cabecera.add("eje4");
            cabecera.add("pla5");
            cabecera.add("eje5");
            cabecera.add("pla6");
            cabecera.add("eje6");
            
            cabecera.add("pla7");
            cabecera.add("eje7");
            cabecera.add("pla8");
            cabecera.add("eje8");
            cabecera.add("pla9");
            cabecera.add("eje9");
            cabecera.add("pla10");
            cabecera.add("eje10");
            cabecera.add("pla11");
            cabecera.add("eje11");
            cabecera.add("pla12");
            cabecera.add("eje12");            
            
            cabecera.add("ejetotal");
            cabecera.add("pagado");            
            cabecera.add("pendiente");              
            cabecera.add("planificado");            
                        
            cabecera.add("total plan financiero");
            cabecera.add("saldo previsionado");            
            cabecera.add("saldo comprometido");
            cabecera.add("reserva");
                        
            cabecera.add("obligado");            
            cabecera.add("saldo plan financiero");       

            
            filexlsx.setCabecera(cabecera);
            
            ArrayList<String> campos = new ArrayList<String>();

            
            campos.add("clase");
            campos.add("programa");
            campos.add("actividad");
            campos.add("obj");
            
            campos.add("ff");
            campos.add("of");
            campos.add("dpto");
            campos.add("obj_decripcion");
            
            campos.add("inicial");
            campos.add("modifi");
            campos.add("vigente");

            campos.add("pla1");
            campos.add("eje1");
            campos.add("pla2");
            campos.add("eje2");
            campos.add("pla3");
            campos.add("eje3");
            campos.add("pla4");
            campos.add("eje4");
            campos.add("pla5");
            campos.add("eje5");
            campos.add("pla6");
            campos.add("eje6");
            
            campos.add("pla7");
            campos.add("eje7");
            campos.add("pla8");
            campos.add("eje8");
            campos.add("pla9");
            campos.add("eje9");
            campos.add("pla10");
            campos.add("eje10");
            campos.add("pla11");
            campos.add("eje11");
            campos.add("pla12");
            campos.add("eje12");
            
            campos.add("ejetotal");
            campos.add("pagado");            
            campos.add("pendiente");              
            campos.add("planificado");            
                        
            campos.add("platotal");
            campos.add("prevision");            
            campos.add("compromiso");
            campos.add("reserva");
                        
            campos.add("obligado");            
            campos.add("saldo_plan");       


            
            filexlsx.setCampos(campos);                                

            PresupuestoRS rs;     
            rs = new PresupuestoRS();
            ResultSet resultset = rs.xls_giraduriacentral();
              
        
           
            filexlsx.newhoja("giraduriacentral");
            filexlsx.writeCabecera(0);
            
            filexlsx.writeContenido(resultset);   
            
            filexlsx.formato();
        
            return filexlsx;
        
    }
             
    

      
     // objetos
    /*
    public  FileXlsx hoja04 ( FileXlsx filexlsx ) throws SQLException, Exception {

        
            ArrayList<String> cabecera = new ArrayList<String>();
            
            cabecera.add("obj");
            cabecera.add("obj_decripcion");
            
            
            cabecera.add("inicial");
            cabecera.add("modifi");
            cabecera.add("vigente");
            
        
            cabecera.add("eje1");
            cabecera.add("pla1");
            cabecera.add("eje2");
            cabecera.add("pla2");
            cabecera.add("eje3");
            cabecera.add("pla3");
            cabecera.add("eje4");
            cabecera.add("pla4");
            cabecera.add("eje5");
            cabecera.add("pla5");
            cabecera.add("eje6");
            cabecera.add("pla6");
            
            cabecera.add("eje7");
            cabecera.add("pla7");
            cabecera.add("eje8");
            cabecera.add("pla8");
            cabecera.add("eje9");
            cabecera.add("pla9");
            cabecera.add("eje10");
            cabecera.add("pla10");
            cabecera.add("eje11");
            cabecera.add("pla11");
            cabecera.add("eje12");
            cabecera.add("pla12");
            
            cabecera.add("platotal");
            cabecera.add("ejetotal");

            
            cabecera.add("planificado");          
            cabecera.add("prevision");            
            cabecera.add("compromiso");            
            cabecera.add("obligado");            
            cabecera.add("pagado");            
            cabecera.add("pendiente");            
            cabecera.add("saldo_plan");       
            cabecera.add("saldo");              
            
            
            filexlsx.setCabecera(cabecera);

            ArrayList<String> campos = new ArrayList<String>();
            
            
            campos.add("obj");
            campos.add("obj_decripcion");
            
            campos.add("inicial");
            campos.add("modifi");
            campos.add("vigente");
   
            campos.add("eje1");
            campos.add("pla1");
            campos.add("eje2");
            campos.add("pla2");
            campos.add("eje3");
            campos.add("pla3");
            campos.add("eje4");
            campos.add("pla4");
            campos.add("eje5");
            campos.add("pla5");
            campos.add("eje6");
            campos.add("pla6");
            
            campos.add("eje7");
            campos.add("pla7");
            campos.add("eje8");
            campos.add("pla8");
            campos.add("eje9");
            campos.add("pla9");
            campos.add("eje10");
            campos.add("pla10");
            campos.add("eje11");
            campos.add("pla11");
            campos.add("eje12");
            campos.add("pla12");
            
            campos.add("platotal");
            campos.add("ejetotal");


            campos.add("planfinanciero");            
            campos.add("prevision");            
            campos.add("compromiso");                        
            campos.add("obligado");            
            campos.add("pagado");            
            campos.add("pendiente");            
            campos.add("saldo_plan");                            
            campos.add("saldo");    
            
            
            
            filexlsx.setCampos(campos);                                

            PresupuestoRS rs;     
            rs = new PresupuestoRS();
            ResultSet resultset = rs.xls_objetos();
        
           
            filexlsx.newhoja("objetos");
            filexlsx.writeCabecera(0);
            
            filexlsx.writeContenido(resultset);           
        
        // Se salva el libro.
        try {
                String ruta = "";
    //            ruta = filexls.getFilePath();                
      //          FileOutputStream file = new FileOutputStream( ruta );                                
        //        filexls.getLibro().write(file);                
          //      file.close();                            
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
        finally{
            return filexlsx;
        }
    }
        
    */



    
    
    
    public  FileXls hoja06 ( FileXls filexls ) throws SQLException, Exception {

        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("grupo");
            cabecera.add("ff-of");
            
            cabecera.add("clase");
            cabecera.add("programa");
            cabecera.add("actividad");
            cabecera.add("obj");            
                        
            
            cabecera.add("ff");
            cabecera.add("of");
            cabecera.add("dpto");
            cabecera.add("obj_decripcion");

            cabecera.add("inicial");
            cabecera.add("modifi");
            cabecera.add("vigente");
            
        
            cabecera.add("eje1");
            cabecera.add("pla1");
            cabecera.add("eje2");
            cabecera.add("pla2");
            cabecera.add("eje3");
            cabecera.add("pla3");
            cabecera.add("eje4");
            cabecera.add("pla4");
            cabecera.add("eje5");
            cabecera.add("pla5");
            cabecera.add("eje6");
            cabecera.add("pla6");
            
            cabecera.add("eje7");
            cabecera.add("pla7");
            cabecera.add("eje8");
            cabecera.add("pla8");
            cabecera.add("eje9");
            cabecera.add("pla9");
            cabecera.add("eje10");
            cabecera.add("pla10");
            cabecera.add("eje11");
            cabecera.add("pla11");
            cabecera.add("eje12");
            cabecera.add("pla12");
            
            cabecera.add("platotal");
            cabecera.add("ejetotal");
                        
            cabecera.add("planificado");          
            cabecera.add("prevision");            
            cabecera.add("compromiso");            
            cabecera.add("obligado");            
            cabecera.add("pagado");            
            cabecera.add("pendiente");            
            cabecera.add("saldo_plan");       
            cabecera.add("saldo");  

            
            
            
            filexls.setCabecera(cabecera);



            ArrayList<String> campos = new ArrayList<String>();

           // campos.add("grupo");
           // campos.add("ffof");            
            
            campos.add("clase");
            campos.add("programa");
            campos.add("actividad");
            campos.add("obj");
                        
            
            campos.add("ff");
            campos.add("of");
            campos.add("dpto");
            campos.add("obj_decripcion");
            
            campos.add("inicial");
            campos.add("modifi");
            campos.add("vigente");

   
            campos.add("eje1");
            campos.add("pla1");
            campos.add("eje2");
            campos.add("pla2");
            campos.add("eje3");
            campos.add("pla3");
            campos.add("eje4");
            campos.add("pla4");
            campos.add("eje5");
            campos.add("pla5");
            campos.add("eje6");
            campos.add("pla6");
            
            campos.add("eje7");
            campos.add("pla7");
            campos.add("eje8");
            campos.add("pla8");
            campos.add("eje9");
            campos.add("pla9");
            campos.add("eje10");
            campos.add("pla10");
            campos.add("eje11");
            campos.add("pla11");
            campos.add("eje12");
            campos.add("pla12");
            
            campos.add("platotal");
            campos.add("ejetotal");

            campos.add("planfinanciero");            
            campos.add("prevision");            
            campos.add("compromiso");                        
            campos.add("obligado");            
            campos.add("pagado");            
            campos.add("pendiente");            
            campos.add("saldo_plan");                            
            campos.add("saldo");    
            
            
            
            filexls.setCampos(campos);                                

            PresupuestoRS rs;     
            rs = new PresupuestoRS();
            ResultSet resulset = rs.qry_obj_ffof_giraduriacentral();

           
            filexls.newhoja("grupo-ff-of-giraduria");
            filexls.writeCabecera(0);
            
            //filexls.writeContenido(resulset);           
        
        // Se salva el libro.
        try {

            //String ruta = "";
            Integer nrofila = 1;        
            
            String grupo = "0";
            String ffof = "0";
            
            
            while (resulset.next()) {            
                
                
                
                String rs_grupo = resulset.getString("grupo").toString();
                if (!(rs_grupo.equals(grupo))){
                    
                    filexls.newfila(nrofila);
                    nrofila++;
                    grupo = rs_grupo;
                    filexls.getFila().createCell(0).setCellValue(grupo);
                    
                }
                

                String rs_ffof = resulset.getString("ffof").toString();
                if (!(rs_ffof.equals(ffof))){
                    
                    filexls.newfila(nrofila);
                    nrofila++;
                    ffof = rs_ffof;
                    filexls.getFila().createCell(1).setCellValue(rs_ffof);
                    
                }                
                
                
                
                
                int i = 2;
                filexls.newfila(nrofila);
                for (String campo : filexls.getCampos()) {   

                    String em = "";                

                    if (resulset.getString(campo) != null){
                        em = resulset.getString(campo);                    
                        String cad = em;

                        try {
                            Long num = Long.parseLong(cad);
                            filexls.getFila().createCell(i).setCellValue(Long.parseLong(cad));
                        }
                        catch (NumberFormatException nfe) {
                            filexls.getFila().createCell(i).setCellValue( em );
                        }                    

                    }
                    i++;
                }            
                nrofila++;
            }        



                
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        finally{
            return filexls;
        }
    }
        
        
    

    
    public  void gen ( FileXlsx filexlsx )  {    
    
        
        
        try {


                
            this.hoja01(filexlsx);
            this.hoja02(filexlsx);
            this.hoja03(filexlsx);
            
            
            //this.hoja04(filexlsx);
            
            /*
            this.hoja02(filexls);        
                
            //this.hoja03(filexls);        
            
            //this.hoja04(filexls);                    
            
            this.hoja05(filexls);    
            
            this.hoja06(filexls);                
            */
            
            String ruta = "";
            ruta = filexlsx.getFilePath();



            FileOutputStream file = new FileOutputStream( ruta );                

            filexlsx.getLibro().write(file);

            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        
        
        
    }    
    
}
