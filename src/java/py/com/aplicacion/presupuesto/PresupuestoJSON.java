/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.presupuesto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Map;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;


public class PresupuestoJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public PresupuestoJSON ( ) throws IOException  {
    
    }
      
    
    
    
    

    public JsonArray  linea ( Integer n1, Integer n2, Integer n3, 
            Integer obj, Integer ff, Integer of, Integer dpto
                    ) {
        
        Map<String, String> map = null;        
        JsonArray jsonarray = new JsonArray();      
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
        
        try 
        {   
            
            PresupuestoRS rs = new PresupuestoRS();            
            ResultSet resulset = rs.linea(
                n1, n2, n3,
                        obj, ff, of, dpto
            );                
            
            while(resulset.next()) 
            {  
                map = registoMap.convertirHashMap(resulset);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarray.add( element );
            }                    
            //this.total_registros = rs.total_registros  ;   
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonarray ;         
        }
    }      
    
    
    
        

    
    

    public JsonArray  lineagrupo ( Integer n1, Integer n2, Integer n3, 
            Integer grupo, Integer ff, Integer of, Integer dpto
                    ) {
        
        Map<String, String> map = null;        
        JsonArray jsonarray = new JsonArray();      
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
        
        try 
        {   
            
            PresupuestoRS rs = new PresupuestoRS();            
            ResultSet resulset = rs.lineagrupo(
                n1, n2, n2,
                        grupo, ff, of, dpto
            );                
            
            while(resulset.next()) 
            {  
                map = registoMap.convertirHashMap(resulset);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarray.add( element );
            }                    
            //this.total_registros = rs.total_registros  ;   
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonarray ;         
        }
    }      
    
    
    
        
    
    
    
        
}
