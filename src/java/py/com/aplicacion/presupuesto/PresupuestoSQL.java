/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.presupuesto;


import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class PresupuestoSQL {
    
    
    
    public String xls_all (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "xls_all.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }

    

    public String xls_neto (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "xls_neto.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }



    
    
    
    
    public String xls_grupo (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "xls_grupo.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }

    
    
    public String xls_objetos (  ) throws Exception {
    
        String sql = "";                                
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "xls_objetos.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
        
    
    
    public String xls_giraduriacentral (  ) throws Exception {
    
        String sql = "";                                
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "xls_giraduriacentral.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
        
    
    
    public String qry_obj_ffof_giraduriacentral (  ) throws Exception {
    
        String sql = "";                                
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "qry_obj_ffof_giraduriacentral.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }

    
    
    public String linea ( Integer n1, Integer n2, Integer n3, 
            Integer obj, Integer ff, Integer of, Integer dpto ) throws Exception {
    
        String sql = "";                                
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "linea.sql";
        
        sql = reader.get( n1, n2, n3,
                        obj, ff, of, dpto );    
        
        
        return sql ;      
    }
              

    
    
    public String lineagrupo ( Integer n1, Integer n2, Integer n3, 
            Integer grupo, Integer ff, Integer of, Integer dpto ) throws Exception {
    
        String sql = "";                                
        
        ReaderT reader = new ReaderT("Presupuesto");
        reader.fileExt = "linea_grupo.sql";
        
        sql = reader.get( n1, n2, n2,
                        grupo, ff, of, dpto );    
        
        return sql ;      
    }
                  
    

    
}
