/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.aplicacion.presupuesto;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Path;
//import jakarta.ws.rs.client.Entity.json;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;


/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("presupuesto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class PresupuestoWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                      
    private Response.Status status  = Response.Status.OK;
    
    
    
                         
    public PresupuestoWS() {
        
    }

    
    

    
    @GET
    @Path("/linea")
    public  Response proceso (  
            @HeaderParam("token") String strToken,            
            @MatrixParam("n1") Integer n1,
            @MatrixParam("n2") Integer n2,
            @MatrixParam("n3") Integer n3,
            @MatrixParam("obj") Integer obj,
            @MatrixParam("ff") Integer ff,
            @MatrixParam("of") Integer of,
            @MatrixParam("dpto") Integer dpto
    ) {
        
        String json = "";
                    
        try {
                        
            if (n1 == null){n1 = 0;}
            if (n2 == null){n2 = 0;}
            if (n3 == null){n3 = 0;}
            
            if (obj == null){obj = 0;}
            
            if (ff == null){ff = 0;}
            if (of == null){of = 0;}
            if (dpto == null){dpto = 0;}
            
                        
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                           
                JsonArray jsonarray = new PresupuestoJSON().linea(
                    n1, n2, n3,
                        obj, ff, of, dpto);
                
                
                 json = jsonarray.toString().trim();                       
                 
                
                if (jsonarray.size() == 0){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                
                return Response
                        .status( this.status )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();                
                
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }                      
         
        
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

    


    
    

    
    @GET
    @Path("/lineagrupo")
    public  Response lineagrupo (  
            @HeaderParam("token") String strToken,            
            @MatrixParam("n1") Integer n1,
            @MatrixParam("n2") Integer n2,
            @MatrixParam("n3") Integer n3,
            @MatrixParam("grupo") Integer grupo,
            @MatrixParam("ff") Integer ff,
            @MatrixParam("of") Integer of,
            @MatrixParam("dpto") Integer dpto
    ) {
        
        String json = "";
                    
        try {
                        
            if (n1 == null){n1 = 0;}
            if (n2 == null){n2 = 0;}
            if (n3 == null){n3 = 0;}
            
            if (grupo == null){grupo = 0;}
            
            if (ff == null){ff = 0;}
            if (of == null){of = 0;}
            if (dpto == null){dpto = 0;}
            
                        
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                           
                JsonArray jsonarray = new PresupuestoJSON().lineagrupo(
                    n1, n2, n2,
                        grupo, ff, of, dpto);
                
                
                 json = jsonarray.toString().trim();                       
                 
                
                if (jsonarray.size() == 0){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                
                return Response
                        .status( this.status )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();                
                
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }                      
         
        
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

    

       
    
    
    

    
}


