/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.presupuesto.consulta;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;


public class PresupuestoConsultaJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public PresupuestoConsultaJSON ( ) throws IOException  {
    
    }
      
    
    
    
    

    public JsonArray  ovpco ( Integer n1, Integer n2, Integer n3, 
            Integer obj1,  Integer obj2,
            Integer ff, Integer of, Integer dpto
                    ) {
        
        Map<String, String> map = null;        
        JsonArray jsonarray = new JsonArray();      
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
        
        try 
        {   
            
            PresupuestoConsultaRS rs = new PresupuestoConsultaRS();            
            ResultSet resulset = rs.ovpco(
                n1, n2, n2,
                        obj1, obj2, 
                        ff, of, dpto
            );                
            
            while(resulset.next()) 
            {  
                map = registoMap.convertirHashMap(resulset);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarray.add( element );
            }                    
            //this.total_registros = rs.total_registros  ;   
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonarray ;         
        }
    }      
    
    
    
        

    
        
    
    

    public JsonObject  tabdata ( Integer n1, Integer n2, Integer n3, 
            Integer obj,  
            Integer ff, Integer of, Integer dpto
                    ) {
        
        Map<String, String> map = null;
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
        JsonObject jsonObject = new JsonObject();
        
        try 
        {   
            
            PresupuestoConsultaRS rs = new PresupuestoConsultaRS();         
            
            ResultSet rsData = rs.tabdata(
                n1, n2, n3, obj, ff, of, dpto );                
            
            JsonArray jsonarrayData = new JsonArray();
            while(rsData.next()) 
            {  
                map = registoMap.convertirHashMap(rsData);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarrayData.add( element );
            }                    
            //this.total_registros = rs.total_registros  ;   
                        
            
            //PresupuestoConsultaRS objetoRS_suma  = new PresupuestoConsultaRS();
    
            ResultSet rsSuma = rs.tabdata_sum(
                n1, n2, n3, obj, ff, of, dpto );                
            
            JsonArray jsonarraySuma = new JsonArray();
            while(rsSuma.next()) 
            {  
                
                map = registoMap.convertirHashMap(rsSuma);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarraySuma.add( element );

            }                    
            
        
            jsonObject.addProperty("status", true);
            jsonObject.addProperty("message", "");
            
            jsonObject.add("data", jsonarrayData);    
            jsonObject.add("summary", jsonarraySuma);
    


        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    
    
        
}
