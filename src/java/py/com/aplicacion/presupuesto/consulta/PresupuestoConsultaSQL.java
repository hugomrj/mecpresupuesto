/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.presupuesto.consulta;


import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class PresupuestoConsultaSQL {
    

    
    
    public String ovpco ( Integer n1, Integer n2, Integer n3, 
            Integer obj1,  Integer obj2, 
            Integer ff, Integer of, Integer dpto ) 
            throws Exception {
    
        String sql = "";                                
        
        ReaderT reader = new ReaderT("PresupuestoConsulta");
        reader.fileExt = "ovpco.sql";
        
        
        sql = reader.get( 
                        obj1,  obj2, 
                        n1, n2, n2,                        
                        ff, of, dpto );    
        
        return sql ;      
    }
              


    public String tabdata ( Integer n1, Integer n2, Integer n3, 
            Integer obj,  
            Integer ff, Integer of, Integer dpto ) 
            throws Exception {
    
        String sql = "";                                
        
        ReaderT reader = new ReaderT("PresupuestoConsulta");
        reader.fileExt = "tabdata.sql";
        
        
        sql = reader.get();    
        
        String condicion01 = " ";
        if (n1 != 0 ){        
            condicion01 = " and e.clase = " + n1 + " ";
        }
         sql =  sql.replaceAll("condicion01", condicion01 );
        

        String condicion02 = " ";
        if (n2 != 0 ){        
            condicion02 = " and e.programa = " + n2 + " ";
        }
         sql =  sql.replaceAll("condicion02", condicion02 );


        String condicion03 = " ";
        if (n3 != 0 ){                  
            condicion03 = " and e.actividad = " + n3 + " ";
        }
         sql =  sql.replaceAll("condicion03", condicion03 );
         

        String condicion_obj = " ";
        if (obj != 0 ){                  
            condicion_obj = " and e.obj = " + obj + " ";
        }
         sql =  sql.replaceAll("condicion_obj", condicion_obj );

         
        String condicion_ff = " ";
        if (ff != 0 ){                  
            condicion_ff = " and e.ff = " + ff + " ";
        }
         sql =  sql.replaceAll("condicion_ff", condicion_ff );
         
         
        String condicion_of = " ";
        if (of != 0 ){                  
            condicion_of = " and e.of = " + of + " ";
        }
         sql =  sql.replaceAll("condicion_of", condicion_of );
         

        String condicion_dpto = " ";
        if (dpto != 0 ){                  
            condicion_dpto = " and e.dpto = " + dpto + " ";
        }
         sql =  sql.replaceAll("condicion_dpto", condicion_dpto );


        return sql ;      
    }
    
    
    public String tabdata_sum ( Integer n1, Integer n2, Integer n3, 
            Integer obj,  
            Integer ff, Integer of, Integer dpto ) 
            throws Exception {
    
        String sql = "";                                
        
        
        
        sql = sql + " select sum(inicial) sum_inicial, sum(modifi) sum_modifi, sum(vigente) sum_vigente,  sum(planfinanciero) sum_planfinanciero, ";
        sql = sql + " sum(obligado) sum_obligado, sum(saldo) sum_saldo,  sum(saldo_plan) sum_saldo_plan, ";
        sql = sql + " sum(pagado) sum_pagado, sum(pendiente) sum_pendiente, sum(prevision) sum_prevision,  sum(compromiso ) sum_compromiso ";
        sql = sql + " from ( ";
        
        sql = sql + this.tabdata(n1, n2, n3, obj, ff, of, dpto);
        
        sql = sql + " ) as t ;";
         
        return sql ;      
    }
    
    
    
    
    
    
    

    
}
