/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.presupuesto.consulta;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */

public class PresupuestoConsultaRS {
    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
        
    
    public PresupuestoConsultaRS ( ) throws IOException, SQLException  {                      
    }
   
    public void inicio ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();
    }
   
    

    
    

    public ResultSet  ovpco (  Integer n1, Integer n2, Integer n3, 
            Integer obj1,  Integer obj2, 
            Integer ff, Integer of, Integer dpto    
            ) 
            throws Exception {

        this.inicio();
        
        String sql = new PresupuestoConsultaSQL().ovpco(
              n1, n2, n2,
                        obj1,  obj2, 
                        ff, of, dpto);
        
        
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
             
    

    

    
    

    public ResultSet  tabdata (  Integer n1, Integer n2, Integer n3, 
            Integer obj,  
            Integer ff, Integer of, Integer dpto    
            ) 
            throws Exception {

        this.inicio();          
        
        String sql = new PresupuestoConsultaSQL().tabdata(
              n1, n2, n3,
                        obj,  
                        ff, of, dpto);
        
        
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
    
    
    
    
    

    public ResultSet  tabdata_sum (  Integer n1, Integer n2, Integer n3, 
            Integer obj,  
            Integer ff, Integer of, Integer dpto    
            ) 
            throws Exception {

        
        this.inicio();
        
        String sql = new PresupuestoConsultaSQL().tabdata_sum(
              n1, n2, n3,
                        obj,  
                        ff, of, dpto);
        
        
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
    
    
    
    
    

    
    
        
}
