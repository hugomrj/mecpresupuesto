/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import py.com.aplicacion.migracion.ejecucion.Ejecucion;
import py.com.aplicacion.migracion.ejecucion.EjecucionDAO;
import py.com.aplicacion.migracion.ejecucion.EjecucionFile;
import py.com.aplicacion.migracion.inicial.Inicial;
import py.com.aplicacion.migracion.inicial.InicialFile;
import py.com.aplicacion.migracion.planfinanciero.PlanFinanciero;
import py.com.aplicacion.migracion.planfinanciero.PlanFinancieroFile;
import py.com.aplicacion.migracion.prevcom.Prevcom;
import py.com.aplicacion.migracion.prevcom.PrevcomFile;

/**
 *
 * @author hugo
 */
public class Migracion {
    
    private String carpeta = "";
    private String[] ficheros ;
    

    public String getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(String carpeta) {
        this.carpeta = carpeta;
    }

    public String[] getFicheros() {
        return ficheros;
    }

    public void setFicheros() {        
        File dir = new File(this.carpeta); 
        this.ficheros = dir.list();                
    }
 
 
    public String quitarExt(  String c) {                
        return c.substring( 0,  c.length() - 4);
    }
 
 
    
    
    
    
public void recorrerArchivos () {
 
    
    
        String[] ficheros = this.ficheros;
 
        for (String fichero : ficheros) {        
            

            
            if (fichero.equals("01-plan.txt")) {
                PlanFinanciero planfinanciero = new PlanFinanciero();                
                new PlanFinancieroFile().readfile(   this.carpeta,   fichero, planfinanciero);                                 
            }    
       
                        
            
            if (fichero.equals("02-ejec.txt")) {                        
                Ejecucion ejecucion = new Ejecucion();                
                new EjecucionFile().readfile(   this.carpeta, fichero, ejecucion );                                 
            }    
            
            
            
            if (fichero.equals("03-inicial.txt")) {            
                
                Inicial inicial = new Inicial();
                new InicialFile().readfile(   this.carpeta, fichero, inicial );                                 
            }                
            
            
            if (fichero.equals("04-prevision.txt")) {
                Prevcom prevcom = new Prevcom();
                new PrevcomFile().readfile(   this.carpeta, fichero, prevcom );                                 
            }                
            
            
            
//System.out.println(fichero);
            


            
        }
        
     }    
    
    
    
    
    
    public String  quitarpunto (String line) {
        
            String ret = "";
            String str = line;
            
            for (int n = 0; n <str.length(); n++ ) { 
                char c = str.charAt (n);                         
                if (!(c == '.')){                                      
                    ret = ret + c;
                }                
            }            
            return ret;
            
    }    
    
        
    
    
    
    public String  recorrerLinea (String line) {
        
        String ret = "";
        String str = line;
        char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);                         
            if (!(c == ' ')){                                      
                ret = ret + c;
            }
            else{
                if (!(c == a)){                                      
                    ret = ret + ";";
                }              
            }
            a = c;
        }            
        return ret;
    }    
              
    
    
    


    public String formatearLinea (String line, String ch) {
        
        String ret = line;
        Integer m = this.maxCh(line, ' ');
        String rep = " ";
        
        /*
    String str = line;        
      for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);      
            System.out.println(c);
      }        
        */
        
        
        
        
        for (int i = m; i > 1; i-- ) {             
            
            //System.out.println(i);
            
            String camb = rep.repeat(i);            
            ret = ret.replaceAll(rep.repeat(i), ch );     
            
        }
        
        return ret;
    }    
              

    
    public Integer maxCh (String line, char h) {
        
        Integer ret = 0;
        Integer max = 0;
        
        String str = line;
        //char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);                         
            if ((c == ' ')){                                      
                max++;                
                if (max > ret){
                    ret = max;
                }
            }
            else{
                max=0;
            }            
        }    
        return ret;
    }    
              

    
    
    
    
    
    public String  eliminarCaracteres (String line) {
        
        String ret = "";
        String str = line;
        char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);      
            
            
            if (c == ' '){                                      
                ret = ret + c;
            }
            else{
                
                if (c == '-'){                                      
                    ret = ret + "-&";
                }
                else{
                
                    if (Character.isDigit(c)){
                        ret = ret + c;
                    }                                  
                
                }

            }
            
        }            
        return ret;

    }    
              
    

    public String  eliminarCharMenos (String line) {
        
        String ret = "";
        String str = line;
        
        str = str.replace("MÉDICO-", "");
        ret = str;
        
        return ret;

    }    
              
    
    
    
      
    



    
}
