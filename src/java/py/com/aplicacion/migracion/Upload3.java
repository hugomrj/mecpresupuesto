package py.com.aplicacion.migracion;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import nebuleuse.util.Recurso;


//import org.apache.log4j.Logger;

@WebServlet(name = "Upload3", urlPatterns = {"/upload3"})

public class Upload3 extends HttpServlet {
    
     
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
         

    Recurso path = new Recurso();
    path.pathWebInf("files");
    String nombre_archivo = "";      
    
    
    String save_dir = path.path;
    int buffer_size = 4096;
        
        
              
        
        
        // Gets file name for HTTP header
        //String fileName = request.getHeader("fileName");

        // prints out all header values
        System.out.println("===== Begin headers =====");
        Enumeration<String> names = request.getHeaderNames();
        while (names.hasMoreElements()) {
            String headerName = names.nextElement();
            System.out.println(headerName + " = " + request.getHeader(headerName));        
            
            if (headerName.equals("nombre_archivo")){
                nombre_archivo = request.getHeader(headerName);
            }
            
        }
        System.out.println("===== End headers =====\n");
               
          
        File saveFile = new File(save_dir + nombre_archivo);
        saveFile.setWritable(true);
        saveFile.setReadable(true);         
        
               
        // opens input stream of the request for reading data
        InputStream inputStream = request.getInputStream();
         
        // opens an output stream for writing file
        FileOutputStream outputStream = new FileOutputStream(saveFile);
         
        byte[] buffer = new byte[buffer_size];
        int bytesRead = -1;
        System.out.println("Receiving data...");
         
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
         
        System.out.println("Data received.");
        outputStream.close();
        inputStream.close();
         
        System.out.println("File written to: " + saveFile.getAbsolutePath());
         
        // sends response to client
        response.getWriter().print("UPLOAD DONE");
    }
}
