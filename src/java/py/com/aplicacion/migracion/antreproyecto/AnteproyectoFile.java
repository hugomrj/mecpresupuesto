/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.antreproyecto;


import py.com.aplicacion.migracion.prevcom.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;
import py.com.aplicacion.migracion.Migracion;

/**
 *
 * @author hugo
 */
public class AnteproyectoFile {
    
    
    
    public void readfile (  String carpeta, String file, Anteproyecto anteproyecto ) {    
    
        String nombreFichero = carpeta +  file ;

        BufferedReader br = null;
        try {

           br = new BufferedReader(new FileReader(nombreFichero));

           String texto = br.readLine();
           
           while(texto != null)
           {
               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() !=0 ){       
                    anteproyecto = leerLinea (sSubCadena, anteproyecto ) ;      
                }                              
               texto = br.readLine();
               
           }
        }
        
                
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
     
    }
    
     
        
        
        
    
    
    
    
    
    
    


    public Anteproyecto leerLinea (String line,   Anteproyecto anteproyecto ) throws IOException {

        
//System.out.println(line);


        String caract = line.substring( 0, 1);

        String control = line.toString().trim();          
        String patron = "";

        // si no empiezar por digito    
        if (!(caract.matches("\\d{1}"))) {
               

            patron = "Actividad:";
            if ( control.startsWith(patron) ) {
                
                line = line.replaceAll(patron, "").trim();
                Integer val = Integer.parseInt(line.trim().substring( 0, 2).trim());
                                          
                anteproyecto.setSub_codigo(val);

            }   


        }                                                               
        else{
                       
System.out.println(control);
            
            String regexp = "";            
            //String regexp = "(^)[1|2]{1}[ ]{3}[.]{1}.*";            
            
            String patronpres;
            patronpres = ".PRESUPUESTO DE";
            
            
            
            int resultado = control.indexOf(patronpres);
System.out.println(resultado);


            if ( control.contains(patronpres) ) {
                      
           //System.out.println(line);
                String obj = line.trim().substring( 0, 1).trim();
                anteproyecto.setTip_codigo(Integer.parseInt(obj));
            }
            else{
                                          
                
                regexp = "(^)[00]\\d{1}.*";            
                if(Pattern.matches(regexp,  line )){

                    String obj = line.trim().substring( 0, 3).trim();            
                    anteproyecto.setPro_codigo(Integer.parseInt(obj));
                }                
                else{
                
                //System.out.println(line);
                    
                    regexp = "\\d{3}.*";            
                    if(Pattern.matches(regexp,  line )){

                        String obj = line.trim().substring( 0, 3).trim();
                        //System.out.println(obj);

                        String linecontrol = line.replace(obj, "").trim();    
                        //System.out.println(linecontrol);         

                        if (linecontrol.trim().length() != 0 ){

                            String ff = linecontrol.trim().substring( 0, 2).trim();
                            //System.out.println(ff);

                            regexp = "\\d{2}.*";            
                            if(Pattern.matches(regexp,  ff )){

                //System.out.println(line);           
                
                                anteproyecto = new AnteproyectoDAO().insert(anteproyecto, line);   


                            }
                        }    
                    }                    

                
                
                }
                
                

            
            }
        

        }             
         
         return anteproyecto;
         
    }    
    
    
        
    
    
    
  
 
    
    
}
