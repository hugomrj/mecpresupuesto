/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.antreproyecto;

import py.com.aplicacion.migracion.prevcom.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.aplicacion.migracion.Migracion;




/**
 *
 * @author hugo
 */
public class AnteproyectoSQL {
    

      
    
    
    public String insert ( Anteproyecto ante )
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("Anteproyecto");
        readerSQL.fileExt = "insert.sql";
        
        
        sql = readerSQL.get( 
                ante.getTip_codigo(), ante.getPro_codigo(), 
                ante.getSub_codigo(), ante.getObj_codigo(),
                ante.getFue_codigo(), ante.getFin_codigo(),
                ante.getDpt_codigo()
                );
        
        sql = sql.replaceAll("v7", ante.getPresupuesto2021().toString());          
        
        return sql ;             
    }        
        
    
}




