/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.aplicacion.migracion.antreproyecto;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import nebuleuse.ORM.db.Persistencia;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 *
 * @author hugo
 */
public class Migracion_migradorxls_main {


    
    public static void main(String args[]) throws IOException, SQLException {
        
        
        
        
            Persistencia persistencia = new Persistencia();
            
            persistencia.ejecutarSQL ("DELETE FROM aplicacion.migradorxls") ;   
    
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(
                    new FileInputStream("/home/hugo/Descargas/migracion.xls"));
        
            // We chose the sheet is passed as parameter.
            // Elegimos la hoja que se pasa por parámetro.
            HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
            // An object that allows us to read a row of the excel sheet, and extract from it the cell contents.
            // Objeto que nos permite leer un fila de la hoja excel, y de aquí extraer el contenido de las celdas.
            HSSFRow hssfRow;
            // Initialize the object to read the value of the cell
            // Inicializo el objeto que leerá el valor de la celda
            HSSFCell cell;
            // I get the number of rows occupied on the sheet
            // Obtengo el número de filas ocupadas en la hoja
            int rows = hssfSheet.getLastRowNum();
            // I get the number of columns occupied on the sheet
            // Obtengo el número de columnas ocupadas en la hoja
            int cols = 0;
            // A string used to store the reading cell
            // Cadena que usamos para almacenar la lectura de la celda
            String cellValue;
            // A string used to store the reading cell
            // Cadena que usamos para almacenar la lectura de la celda

            // For this example we'll loop through the rows getting the data we want
            // Para este ejemplo vamos a recorrer las filas obteniendo los datos que queremos
            
            String sql = "";
            
            
            for (int r = 1; r <= rows; r++) {
                                
                sql = "";
                
                hssfRow = hssfSheet.getRow(r);
                                
                if (hssfRow == null){
                    break;
                }
                else                
                {
                    
                    //System.out.print("Row: " + r + " -> ");
                    System.out.println("----   " +  r );
                    
                    sql = " INSERT INTO aplicacion.migradorxls\n" +
                            " (ejes, comp, tip_codigo, pro_codigo, sub_codigo, obj_codigo, fue_codigo, fin_codigo, dpt_codigo, objds, ley, modi, vig, pl1, ej1, pl2, ej2, pl3, ej3, pl4, ej4, pl5, ej5, pl6, ej6, pl7, ej7, pl8, ej8, pl9, ej9, pl10, ej10, pl11, ej11, pl12, ej12, pagado, oblipenpa, pac, total_e, planf, total_pl, obligado, saldoprev, saldocom, reserva, saldoplan)\n" +
                            " VALUES(  ";
                    
                                                           
                    for (int c = 0; c < (cols = hssfRow.getLastCellNum()); c++) {
                        /*
                        We have those cell types (tenemos estos tipos de celda):
                        CELL_TYPE_BLANK, CELL_TYPE_NUMERIC, CELL_TYPE_BLANK, CELL_TYPE_FORMULA, CELL_TYPE_BOOLEAN, CELL_TYPE_ERROR
                        */
                        
/*
                        cellValue = hssfRow.getCell(c) == null?"":
                                (hssfRow.getCell(c).getCellType() == Cell.CELL_TYPE_STRING)?hssfRow.getCell(c).getStringCellValue():
                                (hssfRow.getCell(c).getCellType() == Cell.CELL_TYPE_NUMERIC)?"" + hssfRow.getCell(c).getNumericCellValue():
                                (hssfRow.getCell(c).getCellType() == Cell.CELL_TYPE_BOOLEAN)?"" + hssfRow.getCell(c).getBooleanCellValue():
                                (hssfRow.getCell(c).getCellType() == Cell.CELL_TYPE_BLANK)?"BLANK":
                                (hssfRow.getCell(c).getCellType() == Cell.CELL_TYPE_FORMULA)?"FORMULA":
                                (hssfRow.getCell(c).getCellType() == Cell.CELL_TYPE_ERROR)?"ERROR":"";


*/


                            if (c == 9){
                                //System.out.println( hssfRow.getCell(c).toString().trim());

                                sql = sql +" , '" + hssfRow.getCell(c).toString().trim() +"' ";
                                //System.out.println( hssfRow.getCell(c).getCellType() );
                            }
                            else{

                                if (c == 0){
                                    sql = sql +" " + hssfRow.getCell(c);
                                }
                                else {
                                    sql = sql +" , " + hssfRow.getCell(c);
                                }    
                            }




                        
                        //System.out.print("[Column " + c + ": " + cellValue + "] ");
                    }
                    
                }
                sql = sql + " ); ";
                System.out.println(sql);
                
                persistencia.ejecutarSQL (sql) ;   
                
                
            } 

        
    }

        
        

}

