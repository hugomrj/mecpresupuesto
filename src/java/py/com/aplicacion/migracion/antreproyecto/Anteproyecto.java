/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.antreproyecto;





/**
 *
 * @author hugo
 */
public class Anteproyecto {
    
    private Integer id;
    private Integer tip_codigo;
    private Integer pro_codigo;
    private Integer sub_codigo;
    
    private Integer obj_codigo;
    private Integer fue_codigo;
    private Integer fin_codigo;
    private Integer dpt_codigo;
    
    private Long presupuesto2021;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTip_codigo() {
        return tip_codigo;
    }

    public void setTip_codigo(Integer tip_codigo) {
        this.tip_codigo = tip_codigo;
    }

    public Integer getPro_codigo() {
        return pro_codigo;
    }

    public void setPro_codigo(Integer pro_codigo) {
        this.pro_codigo = pro_codigo;
    }

    public Integer getSub_codigo() {
        return sub_codigo;
    }

    public void setSub_codigo(Integer sub_codigo) {
        this.sub_codigo = sub_codigo;
    }

    public Integer getObj_codigo() {
        return obj_codigo;
    }

    public void setObj_codigo(Integer obj_codigo) {
        this.obj_codigo = obj_codigo;
    }

    public Integer getFue_codigo() {
        return fue_codigo;
    }

    public void setFue_codigo(Integer fue_codigo) {
        this.fue_codigo = fue_codigo;
    }

    public Integer getFin_codigo() {
        return fin_codigo;
    }

    public void setFin_codigo(Integer fin_codigo) {
        this.fin_codigo = fin_codigo;
    }

    public Integer getDpt_codigo() {
        return dpt_codigo;
    }

    public void setDpt_codigo(Integer dpt_codigo) {
        this.dpt_codigo = dpt_codigo;
    }

    public Long getPresupuesto2021() {
        return presupuesto2021;
    }

    public void setPresupuesto2021(Long presupuesto2021) {
        this.presupuesto2021 = presupuesto2021;
    }
    
    
    
}

