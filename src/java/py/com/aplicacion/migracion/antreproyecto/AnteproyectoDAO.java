/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.antreproyecto;


import py.com.aplicacion.migracion.prevcom.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import py.com.aplicacion.migracion.Migracion;


/**
 *
 * @author hugom_000
 */

public class AnteproyectoDAO  {
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
    
    public AnteproyectoDAO ( ) throws IOException  {

    }
       

    
    
    
    
      
    public Anteproyecto insert ( Anteproyecto anteproyecto, String line ) {
        

        try {

            if ( true ){
            
                                
                String lin  ="";                                  
                lin  =  new Migracion().eliminarCaracteres(line);  
                
//System.out.println("--------");                                
             

                lin =  new Migracion().recorrerLinea(lin);         
//System.out.println(lin);
                lin = lin.replaceAll("&;", "");
                
                lin = lin.replaceAll("-", "");                
                
//System.out.println(lin);

                String[] parts = lin.split(";");                                       

//System.out.println(parts.length);                
                

                
                // primera linea
                if ( parts.length == 5 ) {                    


                    anteproyecto.setObj_codigo(Integer.parseInt(parts[0]));
                    

                    anteproyecto.setFue_codigo(Integer.parseInt(parts[1]));                    

                    anteproyecto.setFin_codigo(Integer.parseInt(parts[2]));                    

                    anteproyecto.setDpt_codigo(Integer.parseInt(parts[3]));                    
                                                            

                    anteproyecto.setPresupuesto2021(Long.parseLong(parts[4]));                    
                                                                                
   
                    String sql = new AnteproyectoSQL().insert( anteproyecto );
                    
System.out.println(sql);
                    

                    Integer cod  =  0;
                    cod = persistencia.ejecutarSQL ( sql, "id") ;                                        

                }                       
    
            }            
        }   
       

        catch (SQLException ex) {
            //System.err.println(ex.getMessage());
            throw new SQLException(ex);
        } 

        
        catch (Exception ex) {
            throw new Exception(ex);        
        }
        
        finally{
            return anteproyecto;
        }

    }
        

       
    
    
    
    
    
    
      
    
        
}
