/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.aplicacion.migracion.basefox_fechas;

import java.util.Date;


public class BaseFoxFecha {
    
    private Integer fecha_id;
    private Date fecha;

    public Integer getFecha_id() {
        return fecha_id;
    }

    public void setFecha_id(Integer fecha_id) {
        this.fecha_id = fecha_id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }


    
}
