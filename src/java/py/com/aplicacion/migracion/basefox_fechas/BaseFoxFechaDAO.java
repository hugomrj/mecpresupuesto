/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.basefox_fechas;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import py.com.aplicacion.migracion.basefox_historico.BaseFoxHistoricoDAO;


/**
 *
 * @author hugom_000
 */

public class BaseFoxFechaDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public BaseFoxFechaDAO ( ) throws IOException  {
    }

    

    public BaseFoxFecha insert ( String fecha ) 
              throws SQLException, Exception{
        
        String sql = new BaseFoxFechaSQL().insert(fecha);

        Integer cod  =  0;
        cod = persistencia.ejecutarSQL ( sql, "fecha_id") ;

        BaseFoxFecha foxfecha = new BaseFoxFecha();          
        
        foxfecha = (BaseFoxFecha) persistencia.filtrarId(foxfecha, cod) ; 

        return foxfecha;
    }
        
      
          
    
    

    public void delete ( String fecha ) 
              throws SQLException, Exception{
        
        Integer fecha_id = this.getId(fecha);
        
        if (fecha_id != 0 ) {
            // borrar 
            this.delete_id(fecha_id);
            
            BaseFoxHistoricoDAO daohistirico = new BaseFoxHistoricoDAO();
            daohistirico.delete(fecha_id);
            
        }


    }
        
      
          
    
    
    public Integer getId ( String fecha ) 
              throws SQLException, Exception{
        
        Integer cod  =  0;
        
        String sql = new BaseFoxFechaSQL().getId(fecha);
        
        cod = persistencia.ejecutarSQL ( sql, "fecha_id") ;        
        
        return cod;
    }
        
              


    public void delete_id ( Integer cod ) 
              throws SQLException, Exception{
        
        //  Integer cod  =  0;        
        String sql = new BaseFoxFechaSQL().delete_id(cod);
        persistencia.ejecutarSQL ( sql);             
        
    }
    
    
    
    public BaseFoxFecha ultimafecha (  ) 
              throws SQLException, Exception{
        
        BaseFoxFecha ret = new BaseFoxFecha();                      
        
        String sql = new BaseFoxFechaSQL().ultimafecha();
  
        ret = (BaseFoxFecha) persistencia.sqlToObject(sql, ret);         
        
        return ret;
    }
            
    
    
    
        
}
