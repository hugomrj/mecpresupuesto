/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.aplicacion.migracion.basefox_fechas;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Webinf;




/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("basefoxfecha")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class BaseFoxFechaWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    
    String json = "";
    
    BaseFoxFecha com = new BaseFoxFecha();           
    
                         
    public BaseFoxFechaWS() {
        
    }

    
    

    
    @GET
    @Path("/ultimafecha")
    public  Response proceso (  @HeaderParam("token") String strToken ) {
                   
        try {
         
            BaseFoxFechaDAO dao = new BaseFoxFechaDAO();
        
            this.com = dao.ultimafecha();
            
            String json = gson.toJson(this.com);
            
            return Response
                    .status(Response.Status.OK)
                    .entity(json)
                    .header("token", strToken )
                    .build();                        

        }                      
         
        
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

    

        
    
    
    

    
}


