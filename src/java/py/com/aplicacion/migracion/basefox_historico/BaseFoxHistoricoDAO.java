/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.basefox_historico;

import py.com.aplicacion.migracion.basefox_fechas.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;


/**
 *
 * @author hugom_000
 */

public class BaseFoxHistoricoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public BaseFoxHistoricoDAO ( ) throws IOException  {
    }

    

    public void insert_datos_actual () 
              throws SQLException, Exception{
        
        String sql = "";
        
        sql = new BaseFoxHistoricoSQL().insert_datos_actual();
        
        persistencia.ejecutarSQL ( sql ) ;
        
        
    }
        
      
          
    
    

    
    public void delete ( Integer fecha_id ) 
              throws SQLException, Exception{
        
        
        String sql = "";
        
        sql = new BaseFoxHistoricoSQL().delete_id(fecha_id);
        
        persistencia.ejecutarSQL ( sql ) ;
        
        
    }
        
     
    
    
    
        
}
