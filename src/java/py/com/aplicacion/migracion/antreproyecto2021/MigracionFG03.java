/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.antreproyecto2021;


import java.io.File;
import py.com.aplicacion.migracion.inicial.fg03.FG03;
import py.com.aplicacion.migracion.inicial.fg03.FG03File;

/**
 *
 * @author hugo
 */
public class MigracionFG03 {
    
    private String carpeta = "";
    private String[] ficheros ;
    

    public String getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(String carpeta) {
        this.carpeta = carpeta;
    }

    public String[] getFicheros() {
        return ficheros;
    }

    public void setFicheros() {        
        File dir = new File(this.carpeta); 
        this.ficheros = dir.list();                
    }
 
 
    public String quitarExt(  String c) {                
        return c.substring( 0,  c.length() - 4);
    }
 
 
    
    
    
    
public void recorrerArchivos () {
 

    
        String[] ficheros = this.ficheros;
        
        
        
 
        for (String fichero : ficheros) {                    
             
System.out.println(fichero);

            
            if (fichero.equals("fg03csv.csv")) {                                
            //if (fichero.equals("fg03csv01.csv")) {                                
                FG03 fg03 = new FG03();
                new FG03File().readfile(   this.carpeta, fichero, fg03 );    
            }                


System.out.println("fin");            


        }
        
     }    
    
    
    
    
    
    public String  quitarpunto (String line) {
        
            String ret = "";
            String str = line;
            
            for (int n = 0; n <str.length(); n++ ) { 
                char c = str.charAt (n);                         
                if (!(c == '.')){                                      
                    ret = ret + c;
                }                
            }            
            return ret;
            
    }    
    
        
    
    
    
    public String  recorrerLinea (String line) {
        
        String ret = "";
        String str = line;
        char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);                         
            if (!(c == ' ')){                                      
                ret = ret + c;
            }
            else{
                if (!(c == a)){                                      
                    ret = ret + ";";
                }              
            }
            a = c;
        }            
        return ret;

    }    
              
    
    
    
    public String  eliminarCaracteres (String line) {
        
        String ret = "";
        String str = line;
        char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);      
            
            
            if (c == ' '){                                      
                ret = ret + c;
            }
            else{
                
                if (c == '-'){                                      
                    ret = ret + "-&";
                }
                else{
                
                    if (Character.isDigit(c)){
                        ret = ret + c;
                    }                                  
                
                }

            }
            
        }            
        return ret;

    }    
              
    

      
    



    
}
