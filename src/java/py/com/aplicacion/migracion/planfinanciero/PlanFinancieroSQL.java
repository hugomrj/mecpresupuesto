/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.planfinanciero;

//import py.com.base.sistema.selector.*;
import py.com.aplicacion.migracion.ejecucion.*;
import py.com.base.sistema.usuario.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.aplicacion.migracion.Migracion;




/**
 *
 * @author hugo
 */
public class PlanFinancieroSQL {
    

      
    
    
    public String insert ( PlanFinanciero plan )
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("PlanFinanciero");
        readerSQL.fileExt = "insert.sql";


        sql = readerSQL.get( plan.getClase(), plan.getPrograma(), plan.getActividad(),
                plan.getObj(), plan.getFf(), plan.getOf(), plan.getDpto()
                );

        sql = sql.replaceAll("pa01",  plan.getPla1().toString());  
        sql = sql.replaceAll("pa02", plan.getPla2().toString());  
        sql = sql.replaceAll("pa03", plan.getPla3().toString());  
        sql = sql.replaceAll("pa04", plan.getPla4().toString());  
        sql = sql.replaceAll("pa05", plan.getPla5().toString());  
        sql = sql.replaceAll("pa06", plan.getPla6().toString());  
        
        sql = sql.replaceAll("pa07", plan.getPla7().toString());  
        sql = sql.replaceAll("pa08", plan.getPla8().toString());  
        sql = sql.replaceAll("pa09", plan.getPla9().toString());  
        sql = sql.replaceAll("pa10", plan.getPla10().toString());  
        sql = sql.replaceAll("pa11", plan.getPla11().toString());  
        sql = sql.replaceAll("pa12", plan.getPla12().toString());  
        
        sql = sql.replaceAll("paTT", plan.getPlatotal().toString());  

        
        return sql ;             
    }        
        
    
    
    
}




