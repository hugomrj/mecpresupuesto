/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.planfinanciero;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import py.com.aplicacion.migracion.Migracion;

/**
 *
 * @author hugo
 */
public class PlanFinancieroFile {
    
 
    public void readfile (  String carpeta, String file, PlanFinanciero planfinanciero ) {
    
        String nombreFichero = carpeta +  file ;

        BufferedReader br = null;
        try {

           br = new BufferedReader(new FileReader(nombreFichero));

           String texto = br.readLine();
           
           while(texto != null)
           {
               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() !=0 ){                    
                                
                    planfinanciero = leerLinea (sSubCadena, planfinanciero ) ;      
                    
                }                              
               texto = br.readLine();
               
           }
        }
        
                
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
     
    }
    
    
    
    
    


    public PlanFinanciero leerLinea (String line,   PlanFinanciero planfinanciero ) throws IOException {

        String sobj = line.substring( 0, 1);

        String control = line.toString().trim();  
        
        String patron = "";

        //if(sobj.matches("\\d{1}")) {                                                       

            patron = "Clase de Programa:";
             if ( control.startsWith(patron) ) {
                
                line = line.replaceAll(patron, "");
                Integer val = Integer.parseInt(line.trim().substring( 0, 1));
                planfinanciero.setClase(  val  );
                
                
             }
             else{             
                 
                patron = "Programa:";
                if ( control.startsWith(patron) )  {

                   line = line.replaceAll(patron, "");
                   Integer val = Integer.parseInt(line.trim().substring( 0, 3));
                   planfinanciero.setPrograma(val  );
                   
                   
                }
                else{
                    
                    patron = "Actividad:";
                    if ( control.startsWith(patron) ) {

                        line = line.replaceAll(patron, "");
                        
                        if (line.length() != 0){
                            Integer val = Integer.parseInt(line.trim().substring( 0, 2));
                                planfinanciero.setActividad(val);                        
                        }                        
                    }                    
                    else{
                        patron = "Proyecto:";
                        if ( control.startsWith(patron) ) {
//System.out.println(line);                                              
                            line = line.replaceAll(patron, "");
               
                            if (line.length() != 0){
                                Integer val = Integer.parseInt(line.trim().substring( 0, 2));                                
//System.out.println(val);                                                          
                                    planfinanciero.setActividad(val);                        
                            }                            
                            
                            
                            /*
                            line = line.replaceAll(patron, "").trim();
                            Integer val = Integer.parseInt(line.trim().substring( 0, 2).trim());
                            planfinanciero.setActividad(val);                        
                            */

                        }        
                    
                    
                        else{                    
                            //line = new Migracion().eliminarCaracteres(line).trim();         
                            if (line.trim().length() !=0 ){                                                                                       
                                    planfinanciero = new PlanFinancieroDAO().insert(planfinanciero, line);            
                            
                            }           
                        }        
                    }
                }
             }


         
         
         return planfinanciero;
         
    }    
    
    
    
    
            
 
    
    
}
