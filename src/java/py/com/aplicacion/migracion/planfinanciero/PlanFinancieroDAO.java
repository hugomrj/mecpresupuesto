/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.planfinanciero;




import py.com.aplicacion.migracion.ejecucion.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import py.com.aplicacion.migracion.Migracion;


/**
 *
 * @author hugom_000
 */

public class PlanFinancieroDAO  {
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
    
    public PlanFinancieroDAO ( ) throws IOException  {

    }
       
      
      
      
    public PlanFinanciero insert ( PlanFinanciero planfinanciero, String line ) {
                
        
        
        try {

            
            if (planfinanciero.getActividad() != null){
                                
                String lin  ="";                  
                lin  =  new Migracion().eliminarCaracteres(line.trim());                         
//System.out.println(lin);                
                lin =  new Migracion().recorrerLinea(lin.trim());         
//System.out.println(lin);
                lin = lin.replaceAll("&;", "");
//System.out.println(lin);
                
                String[] parts = lin.split(";");            




                
                
                // primera linea
                if ( parts.length == 10 ) {
/*
System.out.println("-   1 ");                    
System.out.println("-   "+lin);                    
*/
                    planfinanciero.setObj(Integer.parseInt(parts[0]));
                    planfinanciero.setFf(Integer.parseInt(parts[1]));
                    planfinanciero.setOf(Integer.parseInt(parts[2]));
                    planfinanciero.setDpto(Integer.parseInt(parts[3]));
                    
                    planfinanciero.setPla1(Long.parseLong(parts[4]));
                    planfinanciero.setPla2(Long.parseLong(parts[5]));
                    planfinanciero.setPla3(Long.parseLong(parts[6]));
                    planfinanciero.setPla4(Long.parseLong(parts[7]));
                    planfinanciero.setPla5(Long.parseLong(parts[8]));
                    planfinanciero.setPla6(Long.parseLong(parts[9]));

                }
                else{
                //System.out.println("--              "+line);                    
                //System.out.println("--              "+lin);                    
                }
                
                if ( parts.length == 7 ) {

                    if (planfinanciero.getObj() !=  0 ){

   /*                     
System.out.println("--      2 ");                    
System.out.println("--      "+lin);                    
      */                  
                        planfinanciero.setPla7(Long.parseLong(parts[0]));
                        planfinanciero.setPla8(Long.parseLong(parts[1]));
                        planfinanciero.setPla9(Long.parseLong(parts[2]));
                        planfinanciero.setPla10(Long.parseLong(parts[3]));
                        planfinanciero.setPla11(Long.parseLong(parts[4]));
                        planfinanciero.setPla12(Long.parseLong(parts[5]));

                        planfinanciero.setPlatotal(Long.parseLong(parts[6]));                    


                        String sql = new PlanFinancieroSQL().insert(planfinanciero);


                        Integer cod  =  0;
                        cod = persistencia.ejecutarSQL ( sql, "id") ;

                        planfinanciero.setObj(0);                        
                    
                    }

                    
                }
            
            
            }
            
                       
            
            
        } 
        
        
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
            throw new SQLException(ex);
        } 
        
        catch (Exception ex) {
            throw new Exception(ex);        
        }
        
        finally{
            return planfinanciero;
        }

    }
        

    
        
}
