
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.planfinanciero;



/**
 *
 * @author hugo
 */
public class PlanFinanciero {
    
    private Integer id;
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    private Integer obj;
    private Integer ff;
    private Integer of;
    private Integer dpto;
    
    private Long pla1;
    private Long pla2;
    private Long pla3;
    private Long pla4;
    private Long pla5;
    private Long pla6;
    private Long pla7;
    private Long pla8;
    private Long pla9;
    private Long pla10;
    private Long pla11;
    private Long pla12;
    
    private Long platotal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public Long getPla1() {
        return pla1;
    }

    public void setPla1(Long pla1) {
        this.pla1 = pla1;
    }

    public Long getPla2() {
        return pla2;
    }

    public void setPla2(Long pla2) {
        this.pla2 = pla2;
    }

    public Long getPla3() {
        return pla3;
    }

    public void setPla3(Long pla3) {
        this.pla3 = pla3;
    }

    public Long getPla4() {
        return pla4;
    }

    public void setPla4(Long pla4) {
        this.pla4 = pla4;
    }

    public Long getPla5() {
        return pla5;
    }

    public void setPla5(Long pla5) {
        this.pla5 = pla5;
    }

    public Long getPla6() {
        return pla6;
    }

    public void setPla6(Long pla6) {
        this.pla6 = pla6;
    }

    public Long getPla7() {
        return pla7;
    }

    public void setPla7(Long pla7) {
        this.pla7 = pla7;
    }

    public Long getPla8() {
        return pla8;
    }

    public void setPla8(Long pla8) {
        this.pla8 = pla8;
    }

    public Long getPla9() {
        return pla9;
    }

    public void setPla9(Long pla9) {
        this.pla9 = pla9;
    }

    public Long getPla10() {
        return pla10;
    }

    public void setPla10(Long pla10) {
        this.pla10 = pla10;
    }

    public Long getPla11() {
        return pla11;
    }

    public void setPla11(Long pla11) {
        this.pla11 = pla11;
    }

    public Long getPla12() {
        return pla12;
    }

    public void setPla12(Long pla12) {
        this.pla12 = pla12;
    }

    public Long getPlatotal() {
        return platotal;
    }

    public void setPlatotal(Long platotal) {
        this.platotal = platotal;
    }
    
}
