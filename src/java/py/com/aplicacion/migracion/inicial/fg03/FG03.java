/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.inicial.fg03;

import py.com.aplicacion.migracion.inicial.*;

/**
 *
 * @author hugo
 */
public class FG03 {
    
    private Integer id;
    private Integer lineaid;
    
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    private Integer depen;
    
    private Integer objsg;
    private Integer obj;
    private Integer ff;
    private Integer of;
    private Integer dpto;
    
    private String fundamentacion;
    private String descrip1;
    private String descrip2;
    
    private Long precio_unitario;
    private Long cantidad;
    private Integer meses;
    private Long monto_total;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLineaid() {
        return lineaid;
    }

    public void setLineaid(Integer lineaid) {
        this.lineaid = lineaid;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getDepen() {
        return depen;
    }

    public void setDepen(Integer depen) {
        this.depen = depen;
    }

    public Integer getObjsg() {
        return objsg;
    }

    public void setObjsg(Integer objsg) {
        this.objsg = objsg;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public String getFundamentacion() {
        return fundamentacion;
    }

    public void setFundamentacion(String fundamentacion) {
        this.fundamentacion = fundamentacion;
    }

    public String getDescrip1() {
        return descrip1;
    }

    public void setDescrip1(String descrip1) {
        this.descrip1 = descrip1;
    }

    public String getDescrip2() {
        return descrip2;
    }

    public void setDescrip2(String descrip2) {
        this.descrip2 = descrip2;
    }

    public Long getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(Long precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getMeses() {
        return meses;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }

    public Long getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(Long monto_total) {
        this.monto_total = monto_total;
    }
    
    
    
}

