/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.inicial.fg03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;
import py.com.aplicacion.migracion.Migracion;

/**
 *
 * @author hugo
 */
public class FG03File {
    
 
    
    
    public void readfile (  String carpeta, String file, FG03 objeto ) {    
    
        String nombreFichero = carpeta +  file ;

        BufferedReader br = null;
        try {

            
            
           //br = new BufferedReader(new FileReader(nombreFichero) );
           
           
           br = new BufferedReader(new InputStreamReader(
                   new FileInputStream(nombreFichero), "ISO-8859-1"));

           String texto = br.readLine();
           
           while(texto != null)
           {
               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() !=0 ){       
                    objeto = leerLinea (sSubCadena, objeto ) ;      
                    
                }                              
               texto = br.readLine();
               
           }
        }
        
                
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
     
    }
    
     
        
        
        
    
    
    
    
    
    
    


    public FG03 leerLinea (String line,   FG03 obj ) throws IOException {

        obj = new FG03DAO().insert(obj, line);   
         
        return obj;
         
    }    
    
    
        

    
    
  
 
    
    
}
