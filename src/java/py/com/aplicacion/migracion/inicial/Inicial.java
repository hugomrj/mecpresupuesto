/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.inicial;

/**
 *
 * @author hugo
 */
public class Inicial {
    
    private Integer id;
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    
    private Integer obj;
    private Integer ff;
    private Integer of;
    private Integer dpto;
    
    private Long inicial;
    private Long modifi;
    private Long vigente;
    private Long obligado;
    private Long saldopresupuestario;
    private Long pagado;
    private Long pendiente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public Long getInicial() {
        return inicial;
    }

    public void setInicial(Long inicial) {
        this.inicial = inicial;
    }

    public Long getModifi() {
        return modifi;
    }

    public void setModifi(Long modifi) {
        this.modifi = modifi;
    }

    public Long getVigente() {
        return vigente;
    }

    public void setVigente(Long vigente) {
        this.vigente = vigente;
    }

    public Long getObligado() {
        return obligado;
    }

    public void setObligado(Long obligado) {
        this.obligado = obligado;
    }

    public Long getPagado() {
        return pagado;
    }

    public void setPagado(Long pagado) {
        this.pagado = pagado;
    }

    public Long getPendiente() {
        return pendiente;
    }

    public void setPendiente(Long pendiente) {
        this.pendiente = pendiente;
    }

    /**
     * @return the saldopresupuestario
     */
    public Long getSaldopresupuestario() {
        return saldopresupuestario;
    }

    /**
     * @param saldopresupuestario the saldopresupuestario to set
     */
    public void setSaldopresupuestario(Long saldopresupuestario) {
        this.saldopresupuestario = saldopresupuestario;
    }
    
    
    
}

