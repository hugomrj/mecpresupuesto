/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.inicial;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import py.com.aplicacion.migracion.Migracion;


/**
 *
 * @author hugom_000
 */

public class InicialDAO  {
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
    
    public InicialDAO ( ) throws IOException  {

    }
       

    
    
    
    
      
    public Inicial insert ( Inicial inicial, String line, InicialFile inicialfile ) {
                
    
        
        
        
                          
        try {

            if ( true ){



                String lin  ="";                                  
        
                
                lin  =  new Migracion().eliminarCharMenos(line);  

                
                lin  =  new Migracion().eliminarCaracteres(lin);  
                
                

                
//System.out.println("--------");                                
//System.out.println(line);                
//System.out.println(lin);




                lin =  new Migracion().recorrerLinea(lin);         
                
                lin = lin.replaceAll("&;", "");
                
//System.out.println(lin);

                String[] parts = lin.split(";");                       
                
                
                // primera linea
                if ( parts.length == 11 ) {
                    
                    String strdpto  = inicialfile.linea_final_pagina.substring( 0, 2);
 



                    inicial.setObj(Integer.parseInt(parts[0]));
                    inicial.setFf(Integer.parseInt(parts[1]));
                    inicial.setOf(Integer.parseInt(parts[2]));
                    inicial.setDpto(Integer.parseInt( strdpto.trim() ));   
                    
                    inicial.setInicial(Long.parseLong(parts[4]));   
                    inicial.setModifi(Long.parseLong(parts[5]));   
                    inicial.setVigente(Long.parseLong(parts[6]));   
                    inicial.setObligado(Long.parseLong(parts[7]));   
                    inicial.setSaldopresupuestario(Long.parseLong(parts[8]));   
                    inicial.setPagado(Long.parseLong(parts[9]));   
                    inicial.setPendiente(Long.parseLong(parts[10]));   
                    
                    
                    String sql = new InicialSQL().insert(inicial);
//System.out.println(sql);
                    
                    
                    Integer cod  =  0;
                    cod = persistencia.ejecutarSQL ( sql, "id") ;

                }           

                if ( parts.length == 12 ) {


                    inicial.setObj(Integer.parseInt(parts[0]));
                    inicial.setFf(Integer.parseInt(parts[1]));
                    inicial.setOf(Integer.parseInt(parts[2]));
                    inicial.setDpto(Integer.parseInt(parts[4]));      

                    inicial.setInicial(Long.parseLong(parts[5]));   
                    inicial.setModifi(Long.parseLong(parts[6]));   
                    inicial.setVigente(Long.parseLong(parts[7]));  
                    inicial.setObligado(Long.parseLong(parts[8]));   
                    inicial.setSaldopresupuestario(Long.parseLong(parts[9]));   
                    inicial.setPagado(Long.parseLong(parts[10]));   
                    inicial.setPendiente(Long.parseLong(parts[11]));   

                    String sql = new InicialSQL().insert(inicial);

                    Integer cod  =  0;
                    cod = persistencia.ejecutarSQL ( sql, "id") ;


                }                

                if ( parts.length == 13 ) {
                    
                    inicial.setObj(Integer.parseInt(parts[0]));
                    inicial.setFf(Integer.parseInt(parts[1]));
                    inicial.setOf(Integer.parseInt(parts[2]));
                    inicial.setDpto(Integer.parseInt(parts[4]));   
                    
                    inicial.setInicial(Long.parseLong(parts[5]));   
                    inicial.setModifi(Long.parseLong(parts[6]));   
                    inicial.setVigente(Long.parseLong(parts[7]));   
                    inicial.setObligado(Long.parseLong(parts[8]));   
                    inicial.setSaldopresupuestario(Long.parseLong(parts[9]));   
                    inicial.setPagado(Long.parseLong(parts[10]));   
                    inicial.setPendiente(Long.parseLong(parts[11]));   
                    

                    String sql = new InicialSQL().insert(inicial);


                    Integer cod  =  0;
                    cod = persistencia.ejecutarSQL ( sql, "id") ;


                }           
                
                
            }
            
        } 
   
       
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
            throw new SQLException(ex);
        } 
        
        catch (Exception ex) {
            System.err.println(ex.getMessage());
            throw new Exception(ex);        
        }
        
        finally{
            return inicial;
        }

    }
        

       
    
    
    
    
    
    
      
    
        
}
