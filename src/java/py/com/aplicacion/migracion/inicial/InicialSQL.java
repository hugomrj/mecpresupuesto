/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.inicial;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.aplicacion.migracion.Migracion;




/**
 *
 * @author hugo
 */
public class InicialSQL {
    

      
    
    
    public String insert ( Inicial e )
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("Inicial");
        readerSQL.fileExt = "insert.sql";
        
        
        sql = readerSQL.get( e.getClase(), e.getPrograma(), e.getActividad(), 
                e.getObj(), e.getFf(), e.getOf(), e.getDpto()
                );
                
        
        
        sql = sql.replaceAll("ee01", e.getInicial().toString());  
        sql = sql.replaceAll("ee02", e.getModifi().toString());  
        sql = sql.replaceAll("ee03", e.getVigente().toString());  
        
        sql = sql.replaceAll("ee04", e.getObligado().toString());  
        sql = sql.replaceAll("ee05", e.getSaldopresupuestario().toString());  
        sql = sql.replaceAll("ee06", e.getPagado().toString());  
        
        sql = sql.replaceAll("ee07", e.getPendiente().toString());  
        
        return sql ;             
    }        
        
    
}




