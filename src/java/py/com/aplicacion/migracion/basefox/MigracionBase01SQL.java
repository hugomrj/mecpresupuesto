/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.basefox;

//import py.com.base.sistema.selector.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.com.aplicacion.migracion.ejecucion.*;
import nebuleuse.ORM.sql.ReaderT;




/**
 *
 * @author hugo
 */
public class MigracionBase01SQL {
    

      
    
    
    public String insert ( String[] ar, Integer migracion_cod, 
            Integer fecha_id )
     {
        String sql = "";
         
        
        //System.out.println(  ar[0].toString() );
        //System.out.println(  Integer.parseInt( ar[0] ) );
        
        
        Integer auxi ;
        
        try {
            
            
            
            ReaderT readerSQL = new ReaderT("MigracionBase01");
            readerSQL.fileExt = "insert.sql";
            
            
            sql = readerSQL.get();
            
            
            sql = sql.replaceAll("co0", migracion_cod.toString() );

            
            auxi = (int) Float.parseFloat(ar[2]);            
            sql = sql.replaceAll("es1", auxi.toString() );  

            auxi = (int) Float.parseFloat(ar[3]);            
            sql = sql.replaceAll("es2", auxi.toString() );              

            auxi = (int) Float.parseFloat(ar[4]);            
            sql = sql.replaceAll("es3", auxi.toString() );                          
            
            auxi = (int) Float.parseFloat(ar[6]);            
            sql = sql.replaceAll("es4", auxi.toString() );                          
                        
            auxi = (int) Float.parseFloat(ar[7]);            
            sql = sql.replaceAll("es5", auxi.toString() );                                      
            
            auxi = (int) Float.parseFloat(ar[8]);            
            sql = sql.replaceAll("es6", auxi.toString() );                                                  
            
            auxi = (int) Float.parseFloat(ar[9]);            
            sql = sql.replaceAll("es7", auxi.toString() );          
            
            
            //sql = sql.replaceAll("cc01",  ar[14] );          
                        
            sql = sql.replaceAll("ii01",  ar[15] );          
            sql = sql.replaceAll("ii02",  ar[16] );          
            sql = sql.replaceAll("ii03",  ar[17] ); 
            
            sql = sql.replaceAll("pp01",  ar[18] ); 
            sql = sql.replaceAll("ee01",  ar[19] ); 
            
            sql = sql.replaceAll("pp02",  ar[20] ); 
            sql = sql.replaceAll("ee02",  ar[21] ); 
            
            sql = sql.replaceAll("pp03",  ar[22] ); 
            sql = sql.replaceAll("ee03",  ar[23] ); 
            
            sql = sql.replaceAll("pp04",  ar[24] ); 
            sql = sql.replaceAll("ee04",  ar[25] ); 
            
            sql = sql.replaceAll("pp05",  ar[26] ); 
            sql = sql.replaceAll("ee05",  ar[27] ); 
            
            sql = sql.replaceAll("pp06",  ar[28] ); 
            sql = sql.replaceAll("ee06",  ar[29] ); 
            
            sql = sql.replaceAll("pp07",  ar[30] ); 
            sql = sql.replaceAll("ee07",  ar[31] ); 
            
            sql = sql.replaceAll("pp08",  ar[32] ); 
            sql = sql.replaceAll("ee08",  ar[33] ); 
            
            sql = sql.replaceAll("pp09",  ar[34] ); 
            sql = sql.replaceAll("ee09",  ar[35] ); 
            
            sql = sql.replaceAll("pp10",  ar[36] ); 
            sql = sql.replaceAll("ee10",  ar[37] ); 
            
            sql = sql.replaceAll("pp11",  ar[38] ); 
            sql = sql.replaceAll("ee11",  ar[39] ); 
            
            sql = sql.replaceAll("pp12",  ar[40] ); 
            sql = sql.replaceAll("ee12",  ar[41] ); 
            

            
                        
            sql = sql.replaceAll("zz01",  ar[42] );             
            sql = sql.replaceAll("zz02",  ar[43] );             
            sql = sql.replaceAll("zz03",  ar[44] );             
            sql = sql.replaceAll("zz04",  ar[45] );             
            sql = sql.replaceAll("zz05",  ar[46] );             
            
            sql = sql.replaceAll("zz06",  ar[47] );             
            sql = sql.replaceAll("zz07",  ar[48] );             
            sql = sql.replaceAll("zz08",  ar[49] );             
            sql = sql.replaceAll("zz09",  ar[50] );             
            sql = sql.replaceAll("zz10",  ar[51] );             
            
            sql = sql.replaceAll("zz11",  ar[52] );             


            if (ar.length > 53){
                sql = sql.replaceAll("cc02",  ar[53] );             
            }
            
            sql = sql.replaceAll("fc",  fecha_id.toString() );             


        
            
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        finally{
            return sql ;
        }
    }        
        
    
    
    
}




