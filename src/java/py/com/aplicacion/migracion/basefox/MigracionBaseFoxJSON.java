/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.basefox;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;


public class MigracionBaseFoxJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public MigracionBaseFoxJSON ( ) throws IOException  {
    
    }
      
    
    

    public JsonObject  linea (  Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of , 
            Integer dpto ) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        MigracionBaseFoxSQL oSql = new MigracionBaseFoxSQL();
        
        
        
        try 
        {   
  

            String sql = oSql.linea(
                    clase, programa, actividad, obj, ff, of ,
                    dpto);
                     
            
            
            ResultadoSet resSet = new ResultadoSet(); 
            ResultSet rsData = resSet.resultset(sql);               
            
         
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            

            // union de partes
            jsonObject.add("datos", jsonarrayDatos);                                        
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    

    
    
    
    

    

        
    
    
    
        
}
