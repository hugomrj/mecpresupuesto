/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.basefox;




import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.util.Datetime;


/**
 *
 * @author hugom_000
 */

public class MigracionBase01DAO  {
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
    
    public MigracionBase01DAO ( ) throws IOException  {

    }
       
      
      
      
    public Integer insert ( String[] ar, Integer migracion_cod, 
            Integer fecha_id ) {
                
        try {            
  

            //if ( ar.length == 54 ) {
            if ( ar.length == 53  ||  ar.length == 54 ) {
                
                
                String sql = new MigracionBase01SQL().insert( ar, migracion_cod,
                        fecha_id );

                Integer cod  =  0;                
                cod = persistencia.ejecutarSQL ( sql, "id") ;

            }
        } 
        
        

        catch (Exception ex) {
            throw new Exception(ex);        
        }
        
        finally{
            return 0;
        }

    }
        
    
    
    
    
    

    public Integer insert_fecha ( String fecha ) throws SQLException {
        
        Integer cod  =  0;
           
        String sql =
                "INSERT INTO migracion.basefox_fechas\n " +
                "(fecha)\n " +
                "VALUES('"+fecha+"')\n " +
                "returning migracion ";
                
        cod = persistencia.ejecutarSQL ( sql, "migracion") ;
        
        return cod;
    }
        


    public Integer delete_fecha ( String fecha ) throws SQLException {
        
        Integer cod  =  0;
           
        String sql =
                "DELETE FROM migracion.basefox_fechas\n " +
                "WHERE \n" +
                " fecha = '"+fecha+"' \n" +
                " returning  migracion";
                
        cod = persistencia.ejecutarSQL ( sql, "migracion") ;
        
        return cod;
    }
            
    
    
    
    

        
    
    
    
        
}
