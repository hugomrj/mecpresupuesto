package py.com.aplicacion.migracion.basefox;


import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.util.Recurso;
import nebuleuse.file.FileIO;
import nebuleuse.util.Datetime;
import py.com.aplicacion.migracion.basefox_fechas.BaseFoxFecha;
import py.com.aplicacion.migracion.basefox_fechas.BaseFoxFechaDAO;
import py.com.aplicacion.migracion.basefox_historico.BaseFoxHistoricoDAO;


@WebServlet(
        name = "MigracionBase01", 
        urlPatterns = {"/Migracion/Base/01"}
)


@MultipartConfig(
  fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
  maxFileSize = 1024 * 1024 * 10,      // 10 MB
  maxRequestSize = 1024 * 1024 * 100   // 100 MB
)



public class MigracionBase01 extends HttpServlet {
    
    //static final int BUFFER_SIZE = 1024;
    
     
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)  {
         

        try {
            
            Recurso path = new Recurso();
            path.pathWebInf("files");
            String dir_files = path.path;
            
            long now = System.currentTimeMillis();
            String file_id = String.valueOf(now);            
            String pathfile = dir_files + file_id +".xlsx" ;
                        
System.out.println(pathfile);

            FileIO fileio = new FileIO();
            fileio.write(request, pathfile, "file");

            
            String fecha = request.getParameter("fecha");
            BaseFoxFechaDAO dao = new BaseFoxFechaDAO();        

            
            // borrar fechas en basefox_fechas
            dao.delete(fecha);

                        
            // fecha de base fox            
            
            BaseFoxFecha basefoxfecha = dao.insert(fecha);     
System.out.println(basefoxfecha.getFecha_id());                       
            
            
            //Integer retcod = ccfile.leer(pathfile);
            MigracionBase01File base01File = new MigracionBase01File();
            
            base01File.eliminarDatos();
            base01File.leer(pathfile, 0,
                    basefoxfecha.getFecha_id() );
                        
            
            // volcar datos de actual a historico
            BaseFoxHistoricoDAO daohistoricos = new BaseFoxHistoricoDAO();
            daohistoricos.insert_datos_actual();
            
            
            
            
            
            System.out.println("-------fin------");
            
            
            //response.getWriter().print(retcod);
        } catch (IOException ex) {
            //Logger.getLogger(MigracionBase01.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("IOException ex");
            System.out.println(ex.getMessage());
            
        } catch (ServletException ex) {
            //Logger.getLogger(MigracionBase01.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("ServletException ex");
            System.out.println(ex.getMessage());            
            
        } catch (Exception ex) {
            Logger.getLogger(MigracionBase01.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
}
