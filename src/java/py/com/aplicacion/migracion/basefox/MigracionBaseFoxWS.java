/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.aplicacion.migracion.basefox;




import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;


/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("migracionbasefox")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class MigracionBaseFoxWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    
    
                         
    public MigracionBaseFoxWS() {
        
    }

    
    

    
    @GET
    @Path("/linea")
    public  Response proceso (  
            @HeaderParam("token") String strToken,            
            @MatrixParam("clase") Integer clase,
            @MatrixParam("prog") Integer prog,
            @MatrixParam("acti") Integer acti,
            @MatrixParam("obj") Integer obj,            
            @MatrixParam("ff") Integer ff,
            @MatrixParam("of") Integer of,
            @MatrixParam("dpto") Integer dpto            
    ) {
        
                
        String json = "";
                    
        try {
                        
            if (clase == null){clase = 0;}
            if (prog == null){prog = 0;}
            if (acti == null){acti = 0;}
            
            if (obj == null){obj = 0;}
            if (ff == null){ff = 0;}
            if (of == null){of = 0;}
            if (dpto == null){dpto = 0;}
            
                        
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                MigracionBaseFoxJSON base = new MigracionBaseFoxJSON();
                           
                
                JsonObject jsonarray =  base.linea(clase, prog, acti,
                        obj, ff, of,
                        dpto);     

                
                return Response
                        .status( this.status )
                        .entity( jsonarray.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                
                
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }                      
         
        
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

    
       

    

        
    

    
}


