/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.aplicacion.migracion.basefox;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Persistencia;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class MigracionBase01File {
    
       
     
    public Integer leer (  String pathfile, Integer migra_cod_nuevo,
                Integer fecha_id) {
        
        
        try {
            
            File file = new File(pathfile); 
            
            FileInputStream inputStream = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet firstSheet = workbook.getSheetAt(0);
            
            int npag = workbook.getNumberOfSheets();            
            Iterator iterator = firstSheet.iterator();
            
            MigracionBase01File basef = new MigracionBase01File();
            basef.recorrerXlsx(iterator, migra_cod_nuevo, fecha_id );
         
            
        } catch (Exception e) {
            e.printStackTrace();
        }        
               
        
        return 0;
    }
    

    
    public void eliminarDatos ( )   {    
        
        
        try {
            
            Persistencia persistencia = new Persistencia();                
            persistencia.ejecutarSQL(" DELETE FROM migracion.basefox_actual; ") ;
            
            
        } catch (SQLException ex) {
            //Logger.getLogger(MigracionBase01File.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getMessage());
        }
        
    }
    
      
    
    
    
    
    public void recorrerXlsx ( Iterator iterator, Integer migra_cod_nuevo,
            Integer fecha_id )   {    
            
        
        try {
            
            
            Integer i = 1;
            
            iterator.next();
            MigracionBase01DAO dao = new MigracionBase01DAO();
            
            String lineareg = "";
            while (iterator.hasNext()) {
                
                Row nextRow = (Row) iterator.next();
                Iterator cellIterator = nextRow.cellIterator();                     
                lineareg = "";
                
                
                while(cellIterator.hasNext()) {
                    Cell cell = (Cell) cellIterator.next();
                    Cell contenidoCelda = cell;
                    lineareg = lineareg +(contenidoCelda.toString().trim() + ";");
                }
                
//System.out.println(lineareg);

                String[] arraylinea = lineareg.split(";");
                
                dao.insert( arraylinea, migra_cod_nuevo, fecha_id);                
                
            }
            
            
        } 
        catch (IOException ex) {
            try { 
                throw new Exception (ex);
            } catch (Exception ex1) {
                Logger.getLogger(MigracionBase01File.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }           
    
    }
    
    
    
}
