/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.ejecucion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import py.com.aplicacion.migracion.Migracion;

/**
 *
 * @author hugo
 */
public class EjecucionFile {
    
 
    
    
    public void readfile (  String carpeta, String file, Ejecucion ejecucion ) {    
    
        String nombreFichero = carpeta +  file ;

        BufferedReader br = null;
        try {

           br = new BufferedReader(new FileReader(nombreFichero));

           String texto = br.readLine();
           
           while(texto != null)
           {
               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() !=0 ){       
                    ejecucion = leerLinea (sSubCadena, ejecucion ) ;      
                }                              
               texto = br.readLine();
               
           }
        }
        
                
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
     
    }
    
     
        
        
        
    
    
    
    
    
    
    


    public Ejecucion leerLinea (String line,   Ejecucion ejecucion ) throws IOException {

        
     
        String caract = line.substring( 0, 1);
        


        String control = line.toString().trim();          
        String patron = "";

        // si no empiezar por digito    
        if (!(caract.matches("\\d{1}"))) {
        
        
            patron = "Clase:";
             if ( control.startsWith(patron) ) {
                 

                 line = line.replaceAll(patron, "").trim();
                 //System.out.println(line);
                 Integer val = Integer.parseInt(line.trim().substring( 0, 1) );
                 //System.out.println(val);                
                 
                 ejecucion.setClase(  val  );
                
             }   
             
             
             else{             
                 
                patron = "Programa:";
                if ( control.startsWith(patron) )  {

                    //System.out.println(line);
                    
                   line = line.replaceAll(patron, "").trim();                   
                   
                   //System.out.println(line);                   
                   //System.out.println(line.trim().substring( 0, 3));         
                   
                   Integer val = Integer.parseInt(line.trim().substring( 0, 3).trim());                    
                   ejecucion.setPrograma(val );
                  //  System.out.println(ejecucion.getPrograma());
                   
                }
                
                else{
                    
                    patron = "Actividad:";
                    if ( control.startsWith(patron) ) {

                        line = line.replaceAll(patron, "").trim();                        
                        Integer val = Integer.parseInt(line.trim().substring( 0, 2).trim());
                        ejecucion.setActividad(val);                        
                      //System.out.println(ejecucion.getActividad());
                    }                            
                    else{
                        patron = "Proyecto:";
                        if ( control.startsWith(patron) ) {
                            
                            line = line.replaceAll(patron, "").trim();                        
                            Integer val = Integer.parseInt(line.trim().substring( 0, 2).trim());
                            ejecucion.setActividad(val);                        
                            
                        }        
                    }                    
                }                
             }        
        }                                                               
        else{
        
                //line = new Migracion().eliminarCaracteres(line).trim();         
                if (line.trim().length() !=0 ){           


                    ejecucion = new EjecucionDAO().insert(ejecucion, line);   

//System.out.println(line);                            

                }        
        
/*
            String sobj = line.substring( 0, 3);
        
            if(sobj.matches("\\d{3}")) {                   
                
                // borrar caracteres
                line = line.replaceAll("-", ""); 
                String lin =  new Migracion().recorrerLinea(line);        

//System.out.println(lin);
            
                //String[] parts = lin.split(";");            

//System.out.println(parts.length);                
                 

                ejecucion = new EjecucionDAO().insert(ejecucion, line);   

                //dao.insert( file, lin );

            }          
        
 */      
        
        
        }


    
    

/*        
        
        
        //if(sobj.matches("\\d{1}")) {                                                       

            patron = "Clase de Programa:";
             if ( control.startsWith(patron) ) {
                
                line = line.replaceAll(patron, "");
                Integer val = Integer.parseInt(line.trim().substring( 0, 1));
                //ejecucion.setClase(  val  );
                
                
             }
             else{             
                 
                patron = "Programa:";
                if ( control.startsWith(patron) )  {

                   line = line.replaceAll(patron, "");
                   Integer val = Integer.parseInt(line.trim().substring( 0, 3));
                  // ejecucion.setPrograma(val  );
                   
                   
                }
                else{
                    
                    patron = "Actividad:";
                    if ( control.startsWith(patron) ) {

                        line = line.replaceAll(patron, "");
                        
                        if (line.length() != 0){
                            Integer val = Integer.parseInt(line.trim().substring( 0, 2));
//                                ejecucion.setActividad(val);                        
                        }
                        
                    }
                    
                    
                    else{
                    
                        line = new Migracion().eliminarCaracteres(line).trim();         
                        if (line.length() !=0 ){           
                            
                            
                            //ejecucion = new ejecucionDAO().insert(ejecucion, line);                                               
                            
//System.out.println(line);                            
                            
                        }
        
                    }
                }
             }

*/
         
         
         return ejecucion;
         
    }    
    
    
        
    
    
    
  
 
    
    
}
