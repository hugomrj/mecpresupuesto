/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.ejecucion;




import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import py.com.aplicacion.migracion.Migracion;


/**
 *
 * @author hugom_000
 */

public class EjecucionDAO  {
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
    
    public EjecucionDAO ( ) throws IOException  {

    }
       

    
    
    
    
      
    public Ejecucion insert ( Ejecucion ejecucion, String line ) {
                
        

        try {

            if ( true ){
            //if (planfinanciero.getActividad() != null){
                                
                String lin  ="";                                  
                lin  =  new Migracion().eliminarCaracteres(line);                   
//System.out.println(line);                
//System.out.println(lin);
                lin =  new Migracion().recorrerLinea(lin);                   
//System.out.println(lin);
                lin = lin.replaceAll("&;", "");
                String[] parts = lin.split(";");                       
                
                // primera linea
                if ( parts.length == 18 ) {
                    
                    
                    ejecucion.setObj(Integer.parseInt(parts[0]));
                    ejecucion.setFf(Integer.parseInt(parts[1]));
                    ejecucion.setOf(Integer.parseInt(parts[2]));
                    ejecucion.setDpto(Integer.parseInt(parts[3]));   
                    
                    
                    //ejecucion.setVigente(Long.parseLong(parts[4]));   
                    
                    
                    
                    ejecucion.setEje1(Long.parseLong(parts[5]));
                    ejecucion.setEje2(Long.parseLong(parts[6]));
                    ejecucion.setEje3(Long.parseLong(parts[7]));
                    ejecucion.setEje4(Long.parseLong(parts[8]));
                    ejecucion.setEje5(Long.parseLong(parts[9]));
                    ejecucion.setEje6(Long.parseLong(parts[10]));                    
                    
                    ejecucion.setEje7(Long.parseLong(parts[11]));
                    ejecucion.setEje8(Long.parseLong(parts[12]));
                    ejecucion.setEje9(Long.parseLong(parts[13]));
                    ejecucion.setEje10(Long.parseLong(parts[14]));
                    ejecucion.setEje11(Long.parseLong(parts[15]));
                    ejecucion.setEje12(Long.parseLong(parts[16]));                                        
                    
                    ejecucion.setEjetotal(Long.parseLong(parts[17]));   
                    

                    String sql = new EjecucionSQL().insert(ejecucion);

                    Integer cod  =  0;
                    cod = persistencia.ejecutarSQL ( sql, "id") ;


                }        
        
                
        
            }
            
            
            
        } 
   
        
        
        catch (SQLException ex) {
            //System.err.println(ex.getMessage());
            throw new SQLException(ex);
        } 
        
        
        catch (Exception ex) {
            throw new Exception(ex);        
        }
        
        finally{
            return ejecucion;
        }

    }
        

       
    
    
    
    
    
    
      
    
        
}
