/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.ejecucion;

//import py.com.base.sistema.selector.*;
import py.com.base.sistema.usuario.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.aplicacion.migracion.Migracion;




/**
 *
 * @author hugo
 */
public class EjecucionSQL {
    

      
    
    
    public String insert ( Ejecucion e )
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("Ejecucion");
        readerSQL.fileExt = "insert.sql";
        
        
        sql = readerSQL.get( e.getClase(), e.getPrograma(), e.getActividad(), 
                e.getObj(), e.getFf(), e.getOf(), e.getDpto()
                );
                
        
        
        //sql = sql.replaceAll("eeVI",  e.getVigente().toString());  
        
        
        sql = sql.replaceAll("ee01", e.getEje1().toString());  
        sql = sql.replaceAll("ee02", e.getEje2().toString());  
        sql = sql.replaceAll("ee03", e.getEje3().toString());  
        sql = sql.replaceAll("ee04", e.getEje4().toString());  
        sql = sql.replaceAll("ee05", e.getEje5().toString());  
        sql = sql.replaceAll("ee06", e.getEje6().toString());  
        
        sql = sql.replaceAll("ee07", e.getEje7().toString());  
        sql = sql.replaceAll("ee08", e.getEje8().toString());  
        sql = sql.replaceAll("ee09", e.getEje9().toString());  
        sql = sql.replaceAll("ee10", e.getEje10().toString());  
        sql = sql.replaceAll("ee11", e.getEje11().toString());  
        sql = sql.replaceAll("ee12", e.getEje12().toString());  
        
        sql = sql.replaceAll("eeTT", e.getEjetotal().toString());  
        
   
        return sql ;             
    }        
        
    
    
    
}




