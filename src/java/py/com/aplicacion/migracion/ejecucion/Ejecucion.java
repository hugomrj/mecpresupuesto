
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.ejecucion;

/**
 *
 * @author hugo
 */
public class Ejecucion {
    
    private Integer id;
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    
    private Integer obj;
    private Integer ff;
    private Integer of;
    private Integer dpto;
        
    private Long eje1;
    private Long eje2;
    private Long eje3;
    private Long eje4;
    private Long eje5;
    private Long eje6;
    private Long eje7;
    private Long eje8;
    private Long eje9;
    private Long eje10;
    private Long eje11;
    private Long eje12;
    
    private Long ejetotal;    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public Long getEje1() {
        return eje1;
    }

    public void setEje1(Long eje1) {
        this.eje1 = eje1;
    }

    public Long getEje2() {
        return eje2;
    }

    public void setEje2(Long eje2) {
        this.eje2 = eje2;
    }

    public Long getEje3() {
        return eje3;
    }

    public void setEje3(Long eje3) {
        this.eje3 = eje3;
    }

    public Long getEje4() {
        return eje4;
    }

    public void setEje4(Long eje4) {
        this.eje4 = eje4;
    }

    public Long getEje5() {
        return eje5;
    }

    public void setEje5(Long eje5) {
        this.eje5 = eje5;
    }

    public Long getEje6() {
        return eje6;
    }

    public void setEje6(Long eje6) {
        this.eje6 = eje6;
    }

    public Long getEje7() {
        return eje7;
    }

    public void setEje7(Long eje7) {
        this.eje7 = eje7;
    }

    public Long getEje8() {
        return eje8;
    }

    public void setEje8(Long eje8) {
        this.eje8 = eje8;
    }

    public Long getEje9() {
        return eje9;
    }

    public void setEje9(Long eje9) {
        this.eje9 = eje9;
    }

    public Long getEje10() {
        return eje10;
    }

    public void setEje10(Long eje10) {
        this.eje10 = eje10;
    }

    public Long getEje11() {
        return eje11;
    }

    public void setEje11(Long eje11) {
        this.eje11 = eje11;
    }

    public Long getEje12() {
        return eje12;
    }

    public void setEje12(Long eje12) {
        this.eje12 = eje12;
    }

    public Long getEjetotal() {
        return ejetotal;
    }

    public void setEjetotal(Long ejetotal) {
        this.ejetotal = ejetotal;
    }
    
}


