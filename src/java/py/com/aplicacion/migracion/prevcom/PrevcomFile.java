/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.prevcom;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;


/**
 *
 * @author hugo
 */
public class PrevcomFile {
    
 
    
    
    public void readfile (  String carpeta, String file, Prevcom prevcom ) {    
    
        String nombreFichero = carpeta +  file ;

        BufferedReader br = null;
        try {

           br = new BufferedReader(new FileReader(nombreFichero));

           String texto = br.readLine();
           
           while(texto != null)
           {
               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() !=0 ){       
                    prevcom = leerLinea (sSubCadena, prevcom ) ;      
                }                              
               texto = br.readLine();
               
           }
        }
        
                
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
     
    }
    
     
        
        
        
    
    
    
    
    
    
    


    public Prevcom leerLinea (String line,   Prevcom prevcom ) throws IOException {
        
//System.out.println(line);

        String caract = line.substring( 0, 1);

        String control = line.toString().trim();          
        String patron = "";

        // si no empiezar por digito    
        if (!(caract.matches("\\d{1}"))) {
               
        
            patron = "Clase:";
             if ( control.startsWith(patron) ) {
                 
                 line = line.replaceAll(patron, "").trim();
                 Integer val = Integer.parseInt(line.trim().substring( 0, 1) );
                 prevcom.setClase(  val  );
                 
             }   

             else{             
                 
                patron = "Programa:";
                if ( control.startsWith(patron) )  {

                   line = line.replaceAll(patron, "").trim();                   
                   Integer val = Integer.parseInt(line.trim().substring( 0, 2).trim());                    

                   prevcom.setPrograma(val );
                   
                }
                
                else{

                    patron = "Actividad:";
                    if ( control.startsWith(patron) ) {
                        line = line.replaceAll(patron, "").trim();
                        Integer val = Integer.parseInt(line.trim().substring( 0, 2).trim());
                        prevcom.setActividad(val);                        
                                                
                    }        
                    else{
                        patron = "Proyecto:";
                        if ( control.startsWith(patron) ) {
                            line = line.replaceAll(patron, "").trim();
                            Integer val = Integer.parseInt(line.trim().substring( 0, 2).trim());
                            prevcom.setActividad(val);                        

                        }        
                    }
                }                
             }
        }                                                               
        else{
                       
            //System.out.println(line);
            
            String regexp = "\\d{3}.*";            
            if(Pattern.matches(regexp,  line )){
                
                String obj = line.trim().substring( 0, 3).trim();
                //System.out.println(obj);
                
                String linecontrol = line.replace(obj, "").trim();    
                //System.out.println(linecontrol);         
                
                if (linecontrol.trim().length() != 0 ){
                    
                    String ff = linecontrol.trim().substring( 0, 2).trim();
                    //System.out.println(ff);
                    
                    regexp = "\\d{2}.*";            
                    if(Pattern.matches(regexp,  ff )){
                        
                        prevcom = new PrevcomDAO().insert(prevcom, line);   
                                                
                    }                
                }    
            }        
        }             
         
         return prevcom;
         
    }    
    
    
        
    
    
    
  
 
    
    
}
