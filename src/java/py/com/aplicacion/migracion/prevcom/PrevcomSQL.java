/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.prevcom;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.aplicacion.migracion.Migracion;




/**
 *
 * @author hugo
 */
public class PrevcomSQL {
    

      
    
    
    public String insert ( Prevcom e )
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("Prevcom");
        readerSQL.fileExt = "insert.sql";
        
        
        sql = readerSQL.get( e.getClase(), e.getPrograma(), e.getActividad(), 
                e.getObj(), e.getFf(), e.getOf(), e.getDpto()
                );
                
               
        sql = sql.replaceAll("ee01", e.getPlanfinanciero().toString());  
        sql = sql.replaceAll("ee02", e.getPrevision().toString());  
        sql = sql.replaceAll("ee03", e.getCompromiso().toString());  
        sql = sql.replaceAll("ee04", e.getSaldo_plan().toString());  
        
        
        return sql ;             
    }        
        
    
}




