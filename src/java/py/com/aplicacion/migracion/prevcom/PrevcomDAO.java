/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.prevcom;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import py.com.aplicacion.migracion.Migracion;


/**
 *
 * @author hugom_000
 */

public class PrevcomDAO  {
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
    
    public PrevcomDAO ( ) throws IOException  {

    }
       

    
    
    
    
      
    public Prevcom insert ( Prevcom prevcom, String line ) {
        
        
        try {

            if ( true ){
            
                                
                String lin  ="";                                  
                lin  =  new Migracion().eliminarCaracteres(line);  
                lin =  new Migracion().recorrerLinea(lin);         
                
//System.out.println(lin);
                lin = lin.replaceAll("&;", "");

                String[] parts = lin.split(";");                                       

                
//System.out.println(parts.length);                

                if ( Integer.parseInt(parts[0]) == 358){                    
                    parts[5] = parts[5].replace("-", "");
                }    

             
                // primera linea
                if ( parts.length == 12 ) {      
                    
                    
                    prevcom.setObj(Integer.parseInt(parts[0]));
                    prevcom.setFf(Integer.parseInt(parts[1]));
                    prevcom.setOf(Integer.parseInt(parts[2]));
                    prevcom.setDpto(Integer.parseInt(parts[4]));   
                    
                    
                    prevcom.setPlanfinanciero(Long.parseLong(parts[5]));
                    prevcom.setPrevision(Long.parseLong(parts[6]));   
                    prevcom.setCompromiso(Long.parseLong(parts[7]));   
                    
                    prevcom.setSaldo_plan( Long.parseLong(parts[9]) );

                    String sql = new PrevcomSQL().insert(prevcom);

                    Integer cod  =  0;
                    cod = persistencia.ejecutarSQL ( sql, "id") ;                                        

                    
                }                       
                
                else{
                
                    if ( parts.length == 10 ) {         
                        
System.out.println(lin);                          




                        //if (Integer.parseInt(parts[0]) == 852 )
                        if (parts[0].length() == 3)
                        {
                            
                            prevcom.setObj(Integer.parseInt(parts[0]));
                            prevcom.setFf(Integer.parseInt(parts[1]));
                            prevcom.setOf(Integer.parseInt(parts[2]));
                            prevcom.setDpto(99);   


                            prevcom.setPlanfinanciero(Long.parseLong(parts[3]));
                            prevcom.setPrevision(Long.parseLong(parts[4]));   
                            prevcom.setCompromiso(Long.parseLong(parts[5]));   

                            prevcom.setSaldo_plan( Long.parseLong(parts[7]) );

                            String sql = new PrevcomSQL().insert(prevcom);
                            
                            Integer cod  =  0;
                            cod = persistencia.ejecutarSQL ( sql, "id") ;                                                                   
                        
                        }
                    }    
                }
            }            
        }   
       

        catch (SQLException ex) {
            //System.err.println(ex.getMessage());
            throw new SQLException(ex);
        } 

        
        catch (Exception ex) {
            throw new Exception(ex);        
        }
        
        finally{
            return prevcom;
        }

    }
        

       
    
    
    
    
    
    
      
    
        
}
