/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.prevcom;



/**
 *
 * @author hugo
 */
public class Prevcom {
    
    private Integer id;
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    
    private Integer obj;
    private Integer ff;
    private Integer of;
    private Integer dpto;
    
    private Long prevision;
    private Long planfinanciero;
    private Long compromiso;    
    private Long saldo_plan;
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public Long getPrevision() {
        return prevision;
    }

    public void setPrevision(Long prevision) {
        this.prevision = prevision;
    }

    public Long getCompromiso() {
        return compromiso;
    }

    public void setCompromiso(Long compromiso) {
        this.compromiso = compromiso;
    }


    public Long getPlanfinanciero() {
        return planfinanciero;
    }

    public void setPlanfinanciero(Long planfinanciero) {
        this.planfinanciero = planfinanciero;
    }

    public Long getSaldo_plan() {
        return saldo_plan;
    }

    public void setSaldo_plan(Long saldo_plan) {
        this.saldo_plan = saldo_plan;
    }
    
    
}

