/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.migracion.redundancia;


import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.file.FileXls;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;


/**
 *
 * @author hugo
 */
public class RedundanciaFile {
    
    
    
    
    
    public  FileXlsx hoja01 ( FileXlsx filexlsx ) throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("clase");
            cabecera.add("programa");
            cabecera.add("actividad");
            cabecera.add("obj");            
                        
            
            cabecera.add("ff");
            cabecera.add("of");
            cabecera.add("dpto");
            

            cabecera.add("inicial1");
            cabecera.add("inicial2");
            cabecera.add("dif_inicial");

            cabecera.add("modifi1");
            cabecera.add("modifi2");
            cabecera.add("dif_modifi");
                                    
            cabecera.add("vigente1");
            cabecera.add("vigente2");
            cabecera.add("dif_vigente");
                                    
            cabecera.add("pagado1");
            cabecera.add("pagado2");
            cabecera.add("dif_pagado");
                                    
            cabecera.add("pendiente1");
            cabecera.add("pendiente2");
            cabecera.add("dif_pendiente");
                                    
            cabecera.add("ejetotal1");
            cabecera.add("ejetotal2");
            cabecera.add("dif_ejetotal");
                                    
            cabecera.add("planificado1");
            cabecera.add("planificado2");
            cabecera.add("dif_planificado");
                                    
            cabecera.add("platotal1");
            cabecera.add("platotal2");
            cabecera.add("dif_platotal");
            
            cabecera.add("obligado1");
            cabecera.add("obligado2");
            cabecera.add("dif_obligado");
                                    
            cabecera.add("prevision1");
            cabecera.add("prevision2");
            cabecera.add("dif_prevision");
                                    
            cabecera.add("compromiso1");
            cabecera.add("compromiso2");
            cabecera.add("dif_compromiso");
                                    
            cabecera.add("reserva1");
            cabecera.add("reserva2");
            cabecera.add("dif_reserva");
                                    
            cabecera.add("saldoplan1");
            cabecera.add("saldoplan2");
            cabecera.add("dif_saldoplan");
                                    
            
            
            
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();

            
            
            campos.add("clase");
            campos.add("programa");
            campos.add("actividad");
            campos.add("obj");
            
            campos.add("ff");
            campos.add("of");
            campos.add("dpto");
                       
            campos.add("inicial1");
            campos.add("inicial2");
            campos.add("dif_inicial");
   
            campos.add("modifi1");
            campos.add("modifi2");
            campos.add("dif_modifi");            
            
            campos.add("vigente1");
            campos.add("vigente2");
            campos.add("dif_vigente");

            campos.add("pagado1");
            campos.add("pagado2");
            campos.add("dif_pagado");
                                               
            campos.add("pendiente1");
            campos.add("pendiente2");
            campos.add("dif_pendiente");
                                                
            campos.add("ejetotal1");
            campos.add("ejetotal2");
            campos.add("dif_ejetotal");            
            
            campos.add("planificado1");
            campos.add("planificado2");
            campos.add("dif_planificado");
            
            campos.add("platotal1");
            campos.add("platotal2");
            campos.add("dif_platotal");            
            
            campos.add("obligado1");
            campos.add("obligado2");
            campos.add("dif_obligado");
                                                
            campos.add("prevision1");
            campos.add("prevision2");
            campos.add("dif_prevision");            
            
            campos.add("compromiso1");
            campos.add("compromiso2");
            campos.add("dif_compromiso");
            
            campos.add("reserva1");
            campos.add("reserva2");
            campos.add("dif_reserva");
                                                
            campos.add("saldoplan1");
            campos.add("saldoplan2");
            campos.add("dif_saldoplan");


            
            
            filexlsx.setCampos(campos);                                

            RedundanciaRS rs;     
            rs = new RedundanciaRS();
            ResultSet resultset = rs.xls_all();

                      
           
            filexlsx.newhoja("todo");
            filexlsx.writeCabecera(0);     
            
            
            filexlsx.writeContenido(resultset);           
            
            this.formato(filexlsx);
        
            return filexlsx;                                                              
            
    }
        
    
    


    
    public  void gen ( FileXlsx filexlsx )  {    
    
        
        
        try {


                
            this.hoja01(filexlsx);

            String ruta = "";
            ruta = filexlsx.getFilePath();



            FileOutputStream file = new FileOutputStream( ruta );                

            filexlsx.getLibro().write(file);

            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        
        
        
    }    
    
    
    
    
    public  void formato ( FileXlsx filexlsx   ) {        
        
        //this.newfila(0);

        int i = 0;        
        for (String titulo : filexlsx.getCabecera()) {
                        
            Cell cell = filexlsx.getHoja().getRow(0).getCell(i);
            
            // formato 
            
            //System.out.println(this.hoja.getRow(0).getCell(i+1).getCellType());
                        
            XSSFCellStyle cellStyle = filexlsx.getHoja().getWorkbook().createCellStyle();
            
            java.awt.Color color = new java.awt.Color(220, 220, 220);            
            cellStyle.setFillForegroundColor(new XSSFColor(color, new DefaultIndexedColorMap()));
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
            cell.setCellStyle(cellStyle);
            
            
            i++;
        }
        
        
        
        
  
        int y = 1;
        
        while (y > 0) {
            
            try {      
                
                
                
                Cell cell0 = filexlsx.getHoja().getRow(y).getCell(0);
                
                Row row = cell0.getRow();
                //System.out.println(row.getRowNum());
                                
                
                XSSFCellStyle MistyRoseCellStyle = filexlsx.getHoja().getWorkbook().createCellStyle();
                java.awt.Color MistyRose = new java.awt.Color(255, 228, 225);
                MistyRoseCellStyle.setFillForegroundColor(new XSSFColor(MistyRose, new DefaultIndexedColorMap()));
                MistyRoseCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
                
                
                Cell cell = row.getCell(9);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(12);
                cell.setCellStyle(MistyRoseCellStyle);         
                                
                cell = row.getCell(15);
                cell.setCellStyle(MistyRoseCellStyle);         
                                
                cell = row.getCell(18);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                cell = row.getCell(21);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(24);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(27);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                cell = row.getCell(30);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                                
                cell = row.getCell(33);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(36);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(39);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(42);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                cell = row.getCell(45);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                
            }
            catch (Exception ex)
            {
                //System.out.println(ex.getMessage() );                
                y = -1;
                break;
                
            }  
            
            y++;
        }
  
        
        
        
        
        
    }            


        
    
}
