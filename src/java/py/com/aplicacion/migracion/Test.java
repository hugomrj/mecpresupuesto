package py.com.aplicacion.migracion;

import test.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Iterator;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.util.Recurso;
import nebuleuse.util.Webinf;
import nebuleuse.file.FileIO;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.com.aplicacion.migracion.basefox.MigracionBase01DAO;
import py.com.aplicacion.migracion.basefox.MigracionBase01File;

/**
 *
 * @author hugo
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
        throws FileNotFoundException, IOException, InvalidFormatException, SQLException {


        Persistencia persistencia = new Persistencia();   

           
            Webinf webinf = new Webinf();
            webinf.setCarpeta("files");
            webinf.ini();
            
            String ruta = webinf.getPath();      
            
        System.out.println(ruta);            
    
            ruta = "/home/hugo/Escritorio/27julio/migra/";
        
        System.out.println(ruta);             
            
            Migracion migra = new Migracion();
         
            
            migra.setCarpeta(ruta);
            migra.setFicheros();
    
            
            persistencia.ejecutarSQL (" DELETE FROM migracion.ejecucion_migracion; ") ;            
            persistencia.ejecutarSQL (" DELETE FROM migracion.planfinanciero_migracion; ") ;        
            persistencia.ejecutarSQL (" DELETE FROM migracion.inicial_migracion; ") ;        
            persistencia.ejecutarSQL (" DELETE FROM migracion.prevcom_migracion; ") ;   
                        
            migra.recorrerArchivos();                


    System.out.println("-------fin------");        
    }
}
