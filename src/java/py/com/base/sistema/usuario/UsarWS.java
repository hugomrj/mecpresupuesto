/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.sistema.usuario;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;

/**
 * REST Web Service
 * @author hugo
 */


@Path("uso")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class UsarWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
            
    public UsarWS() {
    }

    
    
    
    @GET    
    public Response list ( ) {
        
        try {                    
           
                
                return Response
                        .status(Response.Status.OK)
                        .entity("funciona")        
                        .build();                       
              
        }     
        catch (Exception ex) {
            
     System.out.println(ex.getLocalizedMessage());
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
    
    
    
    
    
}