/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.REST;

import java.util.Set;
import jakarta.ws.rs.core.Application;

/**
 *
 * @author hugo
 */


@jakarta.ws.rs.ApplicationPath("api")
public class ApplicationVersion1 extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
    
    

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        
        resources.add(py.com.aplicacion.migracion.MigracionWS.class);
        
        resources.add(py.com.aplicacion.migracion.basefox_fechas.BaseFoxFechaWS.class);
        resources.add(py.com.aplicacion.migracion.basefox.MigracionBaseFoxWS.class);
        
        
        resources.add(py.com.aplicacion.presupuesto.PresupuestoWS.class);
        resources.add(py.com.aplicacion.presupuesto.consulta.PresupuestoConsultaWS.class);
        
        
        resources.add(py.com.base.sistema.rol.RolWS.class);
        resources.add(py.com.base.sistema.rol_selector.RolSelectorWS.class);
        resources.add(py.com.base.sistema.selector.SelectorWS.class);
        resources.add(py.com.base.sistema.usuario.UsarWS.class);
        resources.add(py.com.base.sistema.usuario.UsuarioWS.class);
        resources.add(py.com.base.sistema.usuario_rol.UsuarioRolWS.class);
        
        resources.add(py.mec.contrataciones.codigocontratacion.CodigoContratacionWS.class);
        resources.add(py.mec.contrataciones.proveedor.ProveedorWS.class);
        resources.add(py.mec.contrataciones.certificacionpresupuestariadetalle.CertificacionPresupuetariaDetalleWS.class);
        
        resources.add(py.mec.ejecucion.ejecuciondocumento.EjecucionDocumentoWS.class);
        
        resources.add(py.mec.basica.departamento.DepartamentoWS.class);
        resources.add(py.mec.basica.dependencia.DependenciaWS.class);
        resources.add(py.mec.basica.meses.MesWS.class);
        
        
        resources.add(py.mec.certificaciones.certificacionetipo.CertificacionTipoWS.class);
        resources.add(py.mec.certificaciones.certificacionplanfin.CertificacionPlanfinWS.class);
        resources.add(py.mec.certificaciones.pasajeviatico.PasajeViaticoWS.class);
        resources.add(py.mec.certificaciones.pasajeviatico.PasajeViaticoWSquery.class);
        resources.add(py.mec.certificaciones.pasajeviatico.PasajeViaticoWSxlsx.class);
        resources.add(py.mec.certificaciones.cajachica.CajaChicaWS.class);
        resources.add(py.mec.certificaciones.cajachica.CajaChicaWSquery.class);
        resources.add(py.mec.certificaciones.cajachica.CajaChicaWSxlsx.class);
        
        
        resources.add(py.mec.certificaciones.consulta.CertificacionesWS.class);
        

    }
    
}
