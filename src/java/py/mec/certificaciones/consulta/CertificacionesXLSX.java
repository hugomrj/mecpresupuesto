/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.consulta;




import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletContext;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.file.FileXlsx;

/**
 *
 * @author hugo
 */
@WebServlet(name = "CertificacionesXLSX", 
        urlPatterns = {"/certificaciones.xlsx"})
public class CertificacionesXLSX extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    

        try {         
        
            String strToken = "";
            String strCC = "";
        
            strToken = request.getHeader("token");
            strCC = request.getHeader("cc");
            



            Autentificacion autorizacion = new Autentificacion();
            
            if (autorizacion.verificar(strToken))
            {  
            

                FileXlsx filexlsx = new FileXlsx();
                
                filexlsx.Iniciar(request);
                filexlsx.folder = "/files";                
                filexlsx.name = "/base.xlsx";                
                        
                
                filexlsx.newlibro();
                
                
                CertificacionesFile redundanciafile = new CertificacionesFile();

                
                
                redundanciafile.gen1(filexlsx, strCC);
                
                filexlsx.newFileStream();
                                
                
                ServletContext context = getServletContext();
                response.setHeader("token", autorizacion.encriptar());
                filexlsx.getServeltFile(request, response, context);
                
                filexlsx.close();
                
                
                
            }
            else{   
                //System.out.println("no autorizado");                
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
            
            
        } 
        
        
        
        catch (IOException ex) {
            Logger.getLogger(CertificacionesXLSX.class.getName()).log(Level.SEVERE, null, ex);
        }  catch (Exception ex) {
            Logger.getLogger(CertificacionesXLSX.class.getName()).log(Level.SEVERE, null, ex);
        }            
            
                
    }
    
    
    
    

}
