/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.consulta;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class CertificacionesJSON  {


    
    
    public CertificacionesJSON ( ) throws IOException  {
    
    }
      
    

    public JsonObject  codigocontratacion ( Integer page, String buscar) {
        
        
        JsonObject jsonObject = new JsonObject();        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create(); 
        
        Map<String, String> map = null; 
        RegistroMap registoMap = new RegistroMap();     
        
        
        
        try 
        {   
            
            String sql = "";                                  
            CertificacionesSQL codigoSQL = new CertificacionesSQL();               
            sql = codigoSQL.codigocontratacion( buscar);       
            

            ResultSet rsData = new ResultadoSet().resultset(sql, page); 
            
            JsonArray jsonDatos = new JsonArray();      
            
            
            while(rsData.next()) 
            {  
                map = registoMap.convertirHashMap(rsData);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonDatos.add( element );
            }                 
            
            
           
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);

            

    
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
            
            rsData.close();
        }         
        catch (Exception ex) {                                    
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    
    
    public JsonObject  baseinicialsaldo  (  Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of , 
            Integer dpto ) throws Exception {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        CertificacionesSQL oSql = new CertificacionesSQL();


        String sql = oSql.baseinicialsaldo(
                clase, programa, actividad, obj, ff, of ,
                dpto);


        ResultadoSet resSet = new ResultadoSet(); 
        ResultSet rsData = resSet.resultset(sql);               


        JsonArray jsonarrayDatos = new JsonArray();
        jsonarrayDatos = new JsonObjeto().array_datos(rsData);



        jsonObject.add("datos", jsonarrayDatos);             
    
        return jsonObject ;   
    
    }
    
        
}
