/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.consulta;



import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.file.FileXls;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;


/**
 *
 * @author hugo
 */
public class CertificacionesFile {
    
    
    
    
    
    public  FileXlsx hoja01 ( FileXlsx filexlsx, String cc ) throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("clase");
            cabecera.add("programa");
            cabecera.add("actividad");
            cabecera.add("obj"); 
            cabecera.add("ff");
            cabecera.add("of");
            cabecera.add("dpto");
            
            cabecera.add("inicial");
            cabecera.add("vigente");
            
            cabecera.add("");
            
            cabecera.add("agno");
            cabecera.add("fecha_emision");
            cabecera.add("nombre");
            cabecera.add("adjudicacion");
            cabecera.add("descripcion");
            cabecera.add("obj");
            cabecera.add("cc");
            cabecera.add("monto");
            
            
            
            
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();

            
            campos.add("clase");
            campos.add("programa");
            campos.add("actividad");
            campos.add("obj"); 
            campos.add("ff");
            campos.add("of");
            campos.add("dpto");            

            campos.add("inicial");
            campos.add("vigente");
            
            campos.add("esp");
                        
            campos.add("agno");
            campos.add("fecha_emision");
            campos.add("nombre");
            campos.add("adjudicacion");
            campos.add("descripcion");
            campos.add("obj");
            campos.add("cc");
            campos.add("monto");
            

            
            filexlsx.setCampos(campos);                                

            CertifiacionesRS rs;     
            rs = new CertifiacionesRS();
            ResultSet resultset = rs.inicial_certificacion(cc);

                      
           
            filexlsx.newhoja("todo");
            filexlsx.writeCabecera(0);     
            
            
            filexlsx.writeContenido(resultset);           
            
            //this.formato(filexlsx);
        
            return filexlsx;                                                              
            
    }
        
    
    


    
    public  void gen1 ( FileXlsx filexlsx, String cc  )  {    
    
        
        
        try {


                
            this.hoja01(filexlsx, cc);

            String ruta = "";
            ruta = filexlsx.getFilePath();



            FileOutputStream file = new FileOutputStream( ruta );                

            filexlsx.getLibro().write(file);

            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        
        
        
    }    
    
    
    
    
    public  void formato ( FileXlsx filexlsx   ) {        
        
        //this.newfila(0);

        int i = 0;        
        for (String titulo : filexlsx.getCabecera()) {
                        
            Cell cell = filexlsx.getHoja().getRow(0).getCell(i);
            
            // formato 
            
            //System.out.println(this.hoja.getRow(0).getCell(i+1).getCellType());
                        
            XSSFCellStyle cellStyle = filexlsx.getHoja().getWorkbook().createCellStyle();
            
            java.awt.Color color = new java.awt.Color(220, 220, 220);            
            cellStyle.setFillForegroundColor(new XSSFColor(color, new DefaultIndexedColorMap()));
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
            cell.setCellStyle(cellStyle);
            
            
            i++;
        }
        
        
        
        
  
        int y = 1;
        
        while (y > 0) {
            
            try {      
                
                
                
                Cell cell0 = filexlsx.getHoja().getRow(y).getCell(0);
                
                Row row = cell0.getRow();
                //System.out.println(row.getRowNum());
                                
                
                XSSFCellStyle MistyRoseCellStyle = filexlsx.getHoja().getWorkbook().createCellStyle();
                java.awt.Color MistyRose = new java.awt.Color(255, 228, 225);
                MistyRoseCellStyle.setFillForegroundColor(new XSSFColor(MistyRose, new DefaultIndexedColorMap()));
                MistyRoseCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
                
                
                Cell cell = row.getCell(9);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(12);
                cell.setCellStyle(MistyRoseCellStyle);         
                                
                cell = row.getCell(15);
                cell.setCellStyle(MistyRoseCellStyle);         
                                
                cell = row.getCell(18);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                cell = row.getCell(21);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(24);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(27);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                cell = row.getCell(30);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                                
                cell = row.getCell(33);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(36);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(39);
                cell.setCellStyle(MistyRoseCellStyle);                         
                                
                cell = row.getCell(42);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                cell = row.getCell(45);
                cell.setCellStyle(MistyRoseCellStyle);                         
                
                
            }
            catch (Exception ex)
            {
                //System.out.println(ex.getMessage() );                
                y = -1;
                break;
                
            }  
            
            y++;
        }
  
        
        
        
        
        
    }            


        
    
}
