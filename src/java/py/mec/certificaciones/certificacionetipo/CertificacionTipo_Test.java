/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.mec.certificaciones.certificacionetipo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import py.mec.contrataciones.codigocontratacion.*;
import java.io.IOException;
import java.util.List;


/**
 *
 * @author hugo
 */
public class CertificacionTipo_Test {


    
    public static void main(String args[]) throws IOException {
        
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        CertificacionTipoDAO dao = new CertificacionTipoDAO();  

        List<CertificacionTipo> lista = dao.all();                
        String json = gson.toJson( lista );           

        System.out.println( json );

        
    }

        
        

}

