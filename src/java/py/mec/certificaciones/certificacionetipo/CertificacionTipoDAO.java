/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.certificacionetipo;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CertificacionTipoDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public CertificacionTipoDAO ( ) throws IOException  {
   
    }
             
    

    public List<CertificacionTipo>  all () {
                
        List<CertificacionTipo>  lista = null;        
        try {                        
                        
            CertificacionTipoRS rs = new CertificacionTipoRS();            
                        
            lista = new Coleccion<CertificacionTipo>().resultsetToList(
                    new CertificacionTipo(),
                    rs.all()
            );                        
            
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
        
    }      
          
    
    
    
        
    
    
}
