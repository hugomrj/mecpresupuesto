/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.certificacionplanfin;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Map;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class CertificacionPlanfinJSON  {


    
    
    public CertificacionPlanfinJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer tipocertificacion) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
            
            
            ResultadoSet resSet = new ResultadoSet();          
            CertificacionPlanfinSQL codigoSQL = new CertificacionPlanfinSQL();          
            
            String sql = "";                        
            sql = codigoSQL.lista(tipocertificacion);
            
            

            ResultSet rsData = resSet.resultset(sql);
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            
            
            /*
            // detalles
            sql = codigoSQL.detalles(id);            
            ResultSet rsDet = resSet.resultset(sql);
            //rsData = resSet.resultset(sql);
            JsonArray jsonDetalles = new JsonArray();            
            jsonDetalles = new JsonObjeto().array_datos(rsDet);
            */
    
                        
            //jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("detalles", jsonDetalles);            
            
            
            resSet.close();
        }         
        catch (Exception ex) {                                    
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    
    
        
}
