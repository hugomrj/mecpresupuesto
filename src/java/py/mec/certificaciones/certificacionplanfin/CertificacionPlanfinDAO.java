/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.certificacionplanfin;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import py.mec.certificaciones.cajachica.CajaChicaDAO;
import py.mec.certificaciones.pasajeviatico.PasajeViaticoDAO;

/**
 *
 * @author hugom_000
 */
public class CertificacionPlanfinDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public CertificacionPlanfinDAO ( ) throws IOException  {
   
    }
      
    
    public void actualizarPlanFinaciero (CertificacionPlanfin pf)  {
       
                      
        
        try {
            

            
            Long monto = 0L;
            PasajeViaticoDAO pasajesDAO = new PasajeViaticoDAO();
            CajaChicaDAO cajachicaDAO = new CajaChicaDAO();            
            
                
            for (int i = 1; i < 13; i++) {
                
                if (pf.getCertificacion_tipo() == 1 ) {
                    
                    monto = pasajesDAO.getTotalRequerido(
                            i,
                            pf.getClase(),
                            pf.getPrograma(),
                            pf.getActividad(),
                            pf.getObj(),
                            pf.getFf(),
                            pf.getOf(),
                            pf.getDpto());
                    
                    
                }
                else if (pf.getCertificacion_tipo() == 2 ) {
                    
                    monto = cajachicaDAO.getTotalRequerido(
                            i,
                            pf.getClase(),
                            pf.getPrograma(),
                            pf.getActividad(),
                            pf.getObj(),
                            pf.getFf(),
                            pf.getOf(),
                            pf.getDpto());
                }
                
                this.updateMeses(pf, monto, i);
                this.uptadeSaldos(pf);
            }
            
        } 
        catch (IOException ex) {
            //Logger.getLogger(CertificacionPlanfinDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            //Logger.getLogger(CertificacionPlanfinDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }

        
        
        
    }
        
    
    
    
    
    public void updateMeses (CertificacionPlanfin pf, Long monto, Integer mes ) 
              throws SQLException, Exception{
                        
        String sql = new CertificacionPlanfinSQL().updateMeses(
                mes, monto, 
                pf.getCertificacion_tipo(),
                pf.getClase(),
                pf.getPrograma(),
                pf.getActividad(),
                pf.getObj(),
                pf.getFf(),
                pf.getOf(),
                pf.getDpto()
        );

        boolean bo = persistencia.ejecutarSQL ( sql);
            
    }
        
          
 
        

    public void uptadeSaldos (CertificacionPlanfin pf) throws Exception {
        
        String sql = new CertificacionPlanfinSQL().uptadeSaldos(         
            pf.getCertificacion_tipo(),
            pf.getClase(),
            pf.getPrograma(),
            pf.getActividad(),
            pf.getObj(),
            pf.getFf(),
            pf.getOf(),
            pf.getDpto()
        );

        boolean bo = persistencia.ejecutarSQL ( sql);

    }
        
    
       
    
    public void devolucionMesSaldo ( Integer mes,  Long monto, Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto )
              throws SQLException, Exception{
                        
        String sql = new CertificacionPlanfinSQL().devolucionMesSaldo(
                mes, monto, 
                certitipo,
                clase,
                programa,
                actividad,
                obj,
                ff,
                of,
                dpto
        );

        
        boolean bo = persistencia.ejecutarSQL ( sql);
            
    }    
    
    
    public void devolucionMesActual ( Integer mes,  Long monto, Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto )
              throws SQLException, Exception{
                        
        String sql = new CertificacionPlanfinSQL().devolucionMesActual(
                mes, monto, 
                certitipo,
                clase,
                programa,
                actividad,
                obj,
                ff,
                of,
                dpto
        );

        
        boolean bo = persistencia.ejecutarSQL ( sql);
            
    }    
    
        



    
    
    public CertificacionPlanfin filtrar ( Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto) 
            throws Exception {      

          CertificacionPlanfin ret = new CertificacionPlanfin();  

          
          String sql = new CertificacionPlanfinSQL().filtrar1(
                  certitipo, clase, programa, actividad, obj, ff, of, dpto);
          
                
          ret = (CertificacionPlanfin) persistencia.sqlToObject(sql, ret);

          return ret;          

      }
             
    
    
    
    
    
}
