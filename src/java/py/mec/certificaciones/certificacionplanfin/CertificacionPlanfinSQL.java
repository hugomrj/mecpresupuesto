/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.certificacionplanfin;



import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.util.Datetime;

/**
 *
 * @author hugom_000
 */
public class CertificacionPlanfinSQL {
    
    
    
    
    public String lista ( Integer tipocertificacion  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CertificacionPlanfin");
        reader.fileExt = "lista_tipocertificacion.sql";
        
        sql = reader.get( tipocertificacion );    
        
        return sql ;      
    }

    

    public String updateMeses ( Integer mes,  Long monto, Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto )
            throws Exception {
    
        String nombreMes = Datetime.nombreMes(mes)  ;
        
        
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CertificacionPlanfin");
        reader.fileExt = "updateMeses.sql";
        
        sql = reader.get( nombreMes, monto, certitipo,
                clase, programa, actividad,
                obj, ff, of, dpto
                );    
        
        return sql ;      
    }
    
    
    

    public String uptadeSaldos ( Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto )
            throws Exception {
    
        
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CertificacionPlanfin");
        reader.fileExt = "updateSaldos.sql";
        
        sql = reader.get( certitipo,
                clase, programa, actividad,
                obj, ff, of, dpto
                );    
        
        return sql ;      
    }
        
    
    public String filtrar1 ( Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto )
            throws Exception {
            
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CertificacionPlanfin");
        reader.fileExt = "filtrar1.sql";
        
        sql = reader.get( certitipo,
                clase, programa, actividad,
                obj, ff, of, dpto
                );    
        
        return sql ;      
    }
        
    
    
        
    public String devolucionMesSaldo ( Integer mes,  Long monto, Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto )
            throws Exception {

        
        String nombreMes = Datetime.nombreMes(mes)  ;
        
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CertificacionPlanfin");
        reader.fileExt = "devolucionMesSaldo.sql";
        
        sql = reader.get( nombreMes, monto, certitipo,
                clase, programa, actividad,
                obj, ff, of, dpto
                );    
        
        return sql ;      
    }
                    
      
        
    public String devolucionMesActual ( Integer mes,  Long monto, Integer certitipo,
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto )
            throws Exception {

        
        String nombreMes = Datetime.nombreMes(mes)  ;
        
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CertificacionPlanfin");
        reader.fileExt = "devolucionMesActual.sql";
        
        sql = reader.get( nombreMes, monto, certitipo,
                clase, programa, actividad,
                obj, ff, of, dpto
                );    
        
        return sql ;      
    }
                    
                
        
    
    
    
}
