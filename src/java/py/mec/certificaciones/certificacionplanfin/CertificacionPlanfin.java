/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.certificacionplanfin;


/* 
 * @author hugo
 */
public class CertificacionPlanfin {

    private Integer id;
    private Integer certificacion_tipo;
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    private Integer obj;
    private Integer ff;
    private Integer of;
    private Integer dpto;
    
    private Long enero;
    private Long febrero;
    private Long marzo;
    private Long abril;
    private Long mayo;
    private Long junio;
    private Long julio;
    private Long agosto;
    private Long septiembre;
    private Long octubre;
    private Long noviembre;
    private Long diciembre;
    
    
    private Long enero_suma;
    private Long febrero_suma;
    private Long marzo_suma;
    private Long abril_suma;
    private Long mayo_suma;
    private Long junio_suma;
    private Long julio_suma;
    private Long agosto_suma;
    private Long septiembre_suma;
    private Long octubre_suma;
    private Long noviembre_suma;
    private Long diciembre_suma;
        
    
    
    private Long enero_saldo;
    private Long febrero_saldo;
    private Long marzo_saldo;
    private Long abril_saldo;
    private Long mayo_saldo;
    private Long junio_saldo;
    private Long julio_saldo;
    private Long agosto_saldo;
    private Long septiembre_saldo;
    private Long octubre_saldo;
    private Long noviembre_saldo;
    private Long diciembre_saldo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCertificacion_tipo() {
        return certificacion_tipo;
    }

    public void setCertificacion_tipo(Integer certificacion_tipo) {
        this.certificacion_tipo = certificacion_tipo;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public Long getEnero() {
        return enero;
    }

    public void setEnero(Long enero) {
        this.enero = enero;
    }

    public Long getFebrero() {
        return febrero;
    }

    public void setFebrero(Long febrero) {
        this.febrero = febrero;
    }

    public Long getMarzo() {
        return marzo;
    }

    public void setMarzo(Long marzo) {
        this.marzo = marzo;
    }

    public Long getAbril() {
        return abril;
    }

    public void setAbril(Long abril) {
        this.abril = abril;
    }

    public Long getMayo() {
        return mayo;
    }

    public void setMayo(Long mayo) {
        this.mayo = mayo;
    }

    public Long getJunio() {
        return junio;
    }

    public void setJunio(Long junio) {
        this.junio = junio;
    }

    public Long getJulio() {
        return julio;
    }

    public void setJulio(Long julio) {
        this.julio = julio;
    }

    public Long getAgosto() {
        return agosto;
    }

    public void setAgosto(Long agosto) {
        this.agosto = agosto;
    }

    public Long getSeptiembre() {
        return septiembre;
    }

    public void setSeptiembre(Long septiembre) {
        this.septiembre = septiembre;
    }

    public Long getOctubre() {
        return octubre;
    }

    public void setOctubre(Long octubre) {
        this.octubre = octubre;
    }

    public Long getNoviembre() {
        return noviembre;
    }

    public void setNoviembre(Long noviembre) {
        this.noviembre = noviembre;
    }

    public Long getDiciembre() {
        return diciembre;
    }

    public void setDiciembre(Long diciembre) {
        this.diciembre = diciembre;
    }

    public Long getEnero_saldo() {
        return enero_saldo;
    }

    public void setEnero_saldo(Long enero_saldo) {
        this.enero_saldo = enero_saldo;
    }

    public Long getFebrero_saldo() {
        return febrero_saldo;
    }

    public void setFebrero_saldo(Long febrero_saldo) {
        this.febrero_saldo = febrero_saldo;
    }

    public Long getMarzo_saldo() {
        return marzo_saldo;
    }

    public void setMarzo_saldo(Long marzo_saldo) {
        this.marzo_saldo = marzo_saldo;
    }

    public Long getAbril_saldo() {
        return abril_saldo;
    }

    public void setAbril_saldo(Long abril_saldo) {
        this.abril_saldo = abril_saldo;
    }

    public Long getMayo_saldo() {
        return mayo_saldo;
    }

    public void setMayo_saldo(Long mayo_saldo) {
        this.mayo_saldo = mayo_saldo;
    }

    public Long getJunio_saldo() {
        return junio_saldo;
    }

    public void setJunio_saldo(Long junio_saldo) {
        this.junio_saldo = junio_saldo;
    }

    public Long getJulio_saldo() {
        return julio_saldo;
    }

    public void setJulio_saldo(Long julio_saldo) {
        this.julio_saldo = julio_saldo;
    }

    public Long getAgosto_saldo() {
        return agosto_saldo;
    }

    public void setAgosto_saldo(Long agosto_saldo) {
        this.agosto_saldo = agosto_saldo;
    }

    public Long getSeptiembre_saldo() {
        return septiembre_saldo;
    }

    public void setSeptiembre_saldo(Long septiembre_saldo) {
        this.septiembre_saldo = septiembre_saldo;
    }

    public Long getOctubre_saldo() {
        return octubre_saldo;
    }

    public void setOctubre_saldo(Long octubre_saldo) {
        this.octubre_saldo = octubre_saldo;
    }

    public Long getNoviembre_saldo() {
        return noviembre_saldo;
    }

    public void setNoviembre_saldo(Long noviembre_saldo) {
        this.noviembre_saldo = noviembre_saldo;
    }

    public Long getDiciembre_saldo() {
        return diciembre_saldo;
    }

    public void setDiciembre_saldo(Long diciembre_saldo) {
        this.diciembre_saldo = diciembre_saldo;
    }

    public Long getEnero_suma() {
        return enero_suma;
    }

    public void setEnero_suma(Long enero_suma) {
        this.enero_suma = enero_suma;
    }

    public Long getFebrero_suma() {
        return febrero_suma;
    }

    public void setFebrero_suma(Long febrero_suma) {
        this.febrero_suma = febrero_suma;
    }

    public Long getMarzo_suma() {
        return marzo_suma;
    }

    public void setMarzo_suma(Long marzo_suma) {
        this.marzo_suma = marzo_suma;
    }

    public Long getAbril_suma() {
        return abril_suma;
    }

    public void setAbril_suma(Long abril_suma) {
        this.abril_suma = abril_suma;
    }

    public Long getMayo_suma() {
        return mayo_suma;
    }

    public void setMayo_suma(Long mayo_suma) {
        this.mayo_suma = mayo_suma;
    }

    public Long getJunio_suma() {
        return junio_suma;
    }

    public void setJunio_suma(Long junio_suma) {
        this.junio_suma = junio_suma;
    }

    public Long getJulio_suma() {
        return julio_suma;
    }

    public void setJulio_suma(Long julio_suma) {
        this.julio_suma = julio_suma;
    }

    public Long getAgosto_suma() {
        return agosto_suma;
    }

    public void setAgosto_suma(Long agosto_suma) {
        this.agosto_suma = agosto_suma;
    }

    public Long getSeptiembre_suma() {
        return septiembre_suma;
    }

    public void setSeptiembre_suma(Long septiembre_suma) {
        this.septiembre_suma = septiembre_suma;
    }

    public Long getOctubre_suma() {
        return octubre_suma;
    }

    public void setOctubre_suma(Long octubre_suma) {
        this.octubre_suma = octubre_suma;
    }

    public Long getNoviembre_suma() {
        return noviembre_suma;
    }

    public void setNoviembre_suma(Long noviembre_suma) {
        this.noviembre_suma = noviembre_suma;
    }

    public Long getDiciembre_suma() {
        return diciembre_suma;
    }

    public void setDiciembre_suma(Long diciembre_suma) {
        this.diciembre_suma = diciembre_suma;
    }
    

    
}




