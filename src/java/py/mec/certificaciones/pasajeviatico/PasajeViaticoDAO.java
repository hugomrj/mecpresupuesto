/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.pasajeviatico;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import py.mec.certificaciones.certificacionplanfin.CertificacionPlanfinDAO;

/**
 *
 * @author hugom_000
 */
public class PasajeViaticoDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public PasajeViaticoDAO ( ) throws IOException  {
   
    }
      
        

    
    public List<PasajeViatico>  lista_estructura ( Integer page, ResultSet resultSet ) {
                
        List<PasajeViatico>  lista = null;        
            
        try {                        
        
            lista = new Coleccion<PasajeViatico>().resultsetToList(
                        new PasajeViatico(), 
                        resultSet
                    );                            
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
    
    
    

    public Long getTotalRequerido ( Integer mes, 
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto   ) 
            throws SQLException, Exception{
                
        Long ret = 0L;        
        

        String sql = new PasajeViaticoSQL().getTotalRequerido(
                mes, clase, programa, actividad, obj, ff, of, dpto);
        
        ResultSet resultset = new ResultadoSet().resultset(sql);
        
        if (resultset.next() ){
            if (resultset.getString("monto_requerido") != null){
                ret = Long.parseLong( resultset.getString("monto_requerido") );
            }
        }
        
        return ret;
    }
    
    
    
    public PasajeViatico migrarSaldoMensual( PasajeViatico com ) throws Exception{
        
        // registro tiene que estar en true
        // nuevo en false
        
        
        PasajeViatico old = new PasajeViatico();
        old = (PasajeViatico) persistencia.filtrarId(
                old, com.getId()  );  
        
        
        
        if ( old.getObligado() == true ) {
            
            if ( com.getObligado() == false ) {

                int mesActual = old.getMes().getMes();                
                int mesSiguiente = mesActual;
                Long requerido = old.getMonto_requerido();
                
                
                if ( mesActual < 12 ) {
                    mesSiguiente++;
                }
                
                
                CertificacionPlanfinDAO planDao = new CertificacionPlanfinDAO();
                
                
                planDao.devolucionMesActual(mesActual, requerido, 
                        1,
                        old.getClase(),
                        old.getPrograma(),
                        old.getActividad(),
                        old.getObj(),
                        old.getFf(),
                        old.getOf(),
                        old.getDpto()
                );                
                
                
                
                
                planDao.devolucionMesSaldo(mesSiguiente, requerido, 
                        1,
                        old.getClase(),
                        old.getPrograma(),
                        old.getActividad(),
                        old.getObj(),
                        old.getFf(),
                        old.getOf(),
                        old.getDpto()
                );

                
                com.setMonto_requerido(
                    com.getMonto_obligado()                        
                );
    
                System.out.println("editar obligado");        

            }    
        }
        else
        {   
            com.setObligado( false );
            
            com.setMonto_obligado(
               old.getMonto_obligado()                    
            );            
            
        }
        
        return com;
    }
    
    
    
    
    
}
