/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.mec.certificaciones.pasajeviatico;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import py.mec.certificaciones.certificacionplanfin.CertificacionPlanfin;
import py.mec.certificaciones.certificacionplanfin.CertificacionPlanfinDAO;




/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("pasajesviaticos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class PasajeViaticoWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    String json = "";    
    PasajeViatico com = new PasajeViatico();       
    
    
                         
    public PasajeViaticoWS() {
        
    }

    


 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
   

        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                                 
                
                PasajeViatico req = gson.fromJson(json, PasajeViatico.class);    
                                
                req.setObligado(true);
                req.setMonto_obligado( req.getMonto_requerido() );
                
                this.com = (PasajeViatico) persistencia.insert(req);    
                
                // actualizar saldos
                CertificacionPlanfinDAO  dao = new CertificacionPlanfinDAO();
                CertificacionPlanfin pf = dao.filtrar(1, 
                        req.getClase(), 
                        req.getPrograma(), 
                        req.getActividad(), 
                        req.getObj(), 
                        req.getFf(), 
                        req.getOf(), 
                        req.getDpto()
                );
                dao.actualizarPlanFinaciero(pf);
                               
                
                
                                                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(this.status)
                        .entity( json )   
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        
        }     
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")   
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
        


    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                  
                               
                this.com = (PasajeViatico) persistencia.filtrarId(this.com, id);  
                
                String json = gson.toJson(this.com);
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )                        
                        .entity(json)  
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")   
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
        
    

    @GET    
    @Path("/lista/{mes}/{clase}/{programa}/{actividad}/{objeto}/{ff}/{of}/{dpto}")
    public Response listaTipoCertificacion(     
            @HeaderParam("token") String strToken,
            @PathParam ("mes") Integer mes,
            @PathParam ("clase") Integer clase,
            @PathParam ("programa") Integer programa,
            @PathParam ("actividad") Integer actividad,
            @PathParam ("objeto") Integer objeto,
            @PathParam ("ff") Integer ff,
            @PathParam ("of") Integer of,
            @PathParam ("dpto") Integer dpto,
            @QueryParam("page") Integer page,
            @QueryParam("q") String q
            ) {
        
        
        try 
        {                  

            if (page == null) {                
                page = 1;
            }            
            
            
            if (autorizacion.verificar(strToken))            
            {
                autorizacion.actualizar();             
                
                String json = "";    
                

                JsonObject jsonObject 
                        = new PasajeViaticoJSON().lista_estructura( mes, clase, 
                            programa, actividad, objeto, ff, of, dpto, 
                            page, q);

                

                
                return Response
                        .status( this.status )
                        .entity( jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
            
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }        
        
        
    }    
      
        
    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                                  
                
                
                this.com = (PasajeViatico) persistencia.filtrarId(this.com, id);  
                PasajeViatico req = this.com;                  
                
                            
                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;    
                

                
                // actualizar saldos
                CertificacionPlanfinDAO  dao = new CertificacionPlanfinDAO();
                CertificacionPlanfin pf = dao.filtrar(1, 
                        req.getClase(), 
                        req.getPrograma(), 
                        req.getActividad(), 
                        req.getObj(), 
                        req.getFf(), 
                        req.getOf(), 
                        req.getDpto()
                );
                dao.actualizarPlanFinaciero(pf);                
                
                
                
                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)   
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)        
                            .header("token", autorizacion.encriptar())
                            .build();          
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage()) 
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
      
    
    
    
         

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json 
            ) {

        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                                  
                
                PasajeViatico req = new Gson().fromJson(json, PasajeViatico.class);                                                      
                req.setId(id);
                
                
                
                PasajeViaticoDAO comDao = new PasajeViaticoDAO();        
                req =  comDao.migrarSaldoMensual(req);           
                this.com = (PasajeViatico) persistencia.update(req);
                
                
                // actualizar saldos
                CertificacionPlanfinDAO  dao = new CertificacionPlanfinDAO();
                CertificacionPlanfin pf = dao.filtrar(1, 
                        req.getClase(), 
                        req.getPrograma(), 
                        req.getActividad(), 
                        req.getObj(), 
                        req.getFf(), 
                        req.getOf(), 
                        req.getDpto()
                );
                dao.actualizarPlanFinaciero(pf);                
                
                                
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)     
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")      
                    .header("token", autorizacion.encriptar())
                    .build();                    
    
        }        
    }    
    
    
    
    
    
    
    
    
            
    
    

    
}


