/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.pasajeviatico;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class PasajeViaticoJSON  {


    
    
    public PasajeViaticoJSON ( ) throws IOException  {
    
    }
      
    

    public JsonObject  lista_estructura ( Integer mes, Integer clase, Integer programa, 
            Integer actividad, Integer objeto, Integer ff, 
            Integer of, Integer dpto, Integer page, String buscar) {
        
        
        JsonObject jsonObject = new JsonObject();        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        
        
        
        try 
        {   
            
            String sql = "";                                  
            PasajeViaticoSQL codigoSQL = new PasajeViaticoSQL();   
            
            sql = codigoSQL.lista_estructura(mes, clase, programa, 
                    actividad, objeto, ff, of, dpto, buscar);
            

            ResultSet rsData = new ResultadoSet().resultset(sql, page); 
            
            
            PasajeViaticoDAO dao = new PasajeViaticoDAO();                
            List<PasajeViatico> lista = dao.lista_estructura(page, rsData);                  
            
            
            //ResultadoSet resSet = new ResultadoSet();
            
            
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));  
            
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);

            
            
            /*
            // detalles
            sql = codigoSQL.detalles(id);            
            ResultSet rsDet = resSet.resultset(sql);
            //rsData = resSet.resultset(sql);
            JsonArray jsonDetalles = new JsonArray();            
            jsonDetalles = new JsonObjeto().array_datos(rsDet);
            */
    
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
            
            rsData.close();
        }         
        catch (Exception ex) {                                    
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    

    
    

    public JsonObject  dependenciameses ( Integer depe ) {
        
        Map<String, String> map = null;        
        RegistroMap registoMap = new RegistroMap();             

        
        JsonObject jsonObject = new JsonObject();        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        
       
        
        try 
        {   
            
            String sql = "";                                  
            PasajeViaticoSQL codigoSQL = new PasajeViaticoSQL();   
            
            sql = codigoSQL.dependenciameses(depe);            
            ResultSet rsData = new ResultadoSet().resultset(sql); 
            
            
            JsonArray jsonArrayDatos = new JsonArray();
            while(rsData.next()) 
            {  
                map = registoMap.convertirHashMap(rsData);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonArrayDatos.add( element );
            }              
   
            
            
            sql = codigoSQL.dependenciameses_suma(depe);   
            ResultSet rsSummary = new ResultadoSet().resultset(sql); 
        
            JsonArray jsonarraySuma = new JsonArray();     
            while(rsSummary.next()) 
            {  
                map = registoMap.convertirHashMap(rsSummary);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarraySuma.add( element );
            }   
                        
                
            // union de partes
            //jsonObject.add("paginacion", jsonPaginacion);  
            jsonObject.add("datos", jsonArrayDatos);    
            jsonObject.add("summary", jsonarraySuma);            
            
            
            rsData.close();
        }         
        catch (Exception ex) {                                    
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
        
    
    
    
        
}
