/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.pasajeviatico;

import java.util.Date;
import py.mec.basica.departamento.Departamento;
import py.mec.basica.dependencia.Dependencia;
import py.mec.basica.meses.Mes;


public class PasajeViatico {
    
    private Integer id;     
    private Mes mes; 
    private Date fecha;
        
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    private Integer obj;
    private Integer ff;
    private Integer of;
    private Integer dpto;
    
    private Long saldo_disponible;
    private Long monto_requerido;
    private Long monto_restante;
    
    private String expediente;
    private Departamento departamento;
    private Dependencia dependencia;

    
    private Boolean obligado;
    private Long monto_obligado;
    
    
    private Integer memo_numero;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Mes getMes() {
        return mes;
    }

    public void setMes(Mes mes) {
        this.mes = mes;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public Long getSaldo_disponible() {
        return saldo_disponible;
    }

    public void setSaldo_disponible(Long saldo_disponible) {
        this.saldo_disponible = saldo_disponible;
    }

    public Long getMonto_requerido() {
        return monto_requerido;
    }

    public void setMonto_requerido(Long monto_requerido) {
        this.monto_requerido = monto_requerido;
    }

    public Long getMonto_restante() {
        return monto_restante;
    }

    public void setMonto_restante(Long monto_restante) {
        this.monto_restante = monto_restante;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Dependencia getDependencia() {
        return dependencia;
    }

    public void setDependencia(Dependencia dependencia) {
        this.dependencia = dependencia;
    }


    public Integer getMemo_numero() {
        return memo_numero;
    }

    public void setMemo_numero(Integer memo_numero) {
        this.memo_numero = memo_numero;
    }

    public void setObligado(Boolean obligado) {
        this.obligado = obligado;
    }

    public Boolean getObligado() {
        return obligado;
    }

    public Long getMonto_obligado() {
        return monto_obligado;
    }

    public void setMonto_obligado(Long monto_obligado) {
        this.monto_obligado = monto_obligado;
    }
    
    
    
}






