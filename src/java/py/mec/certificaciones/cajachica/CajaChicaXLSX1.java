/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.mec.certificaciones.cajachica;

import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.file.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author hugo
 */
public class CajaChicaXLSX1 {
    
    
    public  void gen ( FileXlsx filexlsx, Integer dep )  {    
           
        try {
            
            this.hoja01(filexlsx, dep);
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    
    
    public  FileXlsx hoja01 ( FileXlsx filexlsx, Integer dep  ) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("mes");
            cabecera.add("monto_certificado");
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            campos.add("mes");
            campos.add("monto_certificado");
            
            filexlsx.setCampos(campos);                                
               
            String sql = new CajaChicaSQL().dependenciameses(dep);                    
            ResultSet resultset = new ResultadoSet().resultset(sql);             
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            this.formato(filexlsx);
        
            return filexlsx;                                                              
            
    }
     
    

    
    public  void formato ( FileXlsx filexlsx  ) {        
        
        //this.newfila(0);
        

        int i = 0;
        for (String titulo : filexlsx.getCabecera()) {     
                       
            
            Cell cell = filexlsx.getHoja().getRow(0).getCell(i);
            
            // formato             
            //System.out.println(this.hoja.getRow(0).getCell(i+1).getCellType());
            
            XSSFCellStyle cellStyle = filexlsx.getHoja().getWorkbook().createCellStyle();
            
            
            java.awt.Color color = new java.awt.Color(220, 220, 220);            
            cellStyle.setFillForegroundColor(new XSSFColor(color, new DefaultIndexedColorMap()));
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
            cell.setCellStyle(cellStyle);
            
            
            i++;
        }
        
        
      /*  
        int y = 1;
        
        while (y > 0) {
        
            
            try {      
                
                Cell cell0 = this.hoja.getRow(y).getCell(0);
                
                Row row = cell0.getRow();
                //System.out.println(row.getRowNum());
                                
                
                XSSFCellStyle MistyRoseCellStyle = this.hoja.getWorkbook().createCellStyle();
                java.awt.Color MistyRose = new java.awt.Color(255, 228, 225);
                MistyRoseCellStyle.setFillForegroundColor(new XSSFColor(MistyRose, new DefaultIndexedColorMap()));
                MistyRoseCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
                
                
                Cell cell = row.getCell(39);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(40);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(41);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(42);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(43);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                
                XSSFCellStyle BlueWebCellStyle = this.hoja.getWorkbook().createCellStyle();
                java.awt.Color BlueWeb = new java.awt.Color(206, 231, 255);
                BlueWebCellStyle.setFillForegroundColor(new XSSFColor(BlueWeb, new DefaultIndexedColorMap()));
                BlueWebCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                   
                
                cell = row.getCell(44);
                cell.setCellStyle(BlueWebCellStyle);                
                
                
                
            }
            catch (Exception ex)
            {
                //System.out.println(ex.getMessage() );                
                y = -1;
                break;
                
            }  
            
            y++;
        }
        */
        
        
        
        
        
    }            


        
        
    
}
