/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.mec.certificaciones.cajachica;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import java.io.File;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.file.FileXlsx;
import nebuleuse.util.Recurso;
import py.com.aplicacion.presupuesto.PresupuestoFile;


/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("cajachica/xlsx")




public class CajaChicaWSxlsx {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    String json = "";    
    CajaChica com = new CajaChica();       
        
                         
    public CajaChicaWSxlsx() {
        
    }

    
    




  
  
    @GET
    @Path("/dependenciameses/{dep}")
    @Produces("aapplication/vnd.ms-excel")
    public Response dependenciameses(
            @HeaderParam("token") String strToken,
            @PathParam ("dep") Integer dep ) {
 
        
        String path = "";


        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                  

                
                FileXlsx filexlsx = new FileXlsx();                
                filexlsx.iniciar();
                filexlsx.folder = "/files";                
                filexlsx.name = "/base.xlsx";      
                filexlsx.newlibro();
                
                                
                CajaChicaXLSX1 xlsx1 = new CajaChicaXLSX1();                
                xlsx1.gen(filexlsx, dep);
                
                
                
                filexlsx.newFileStream();
                path = filexlsx.getFilePath();
        
                File file = new File(path);

                ResponseBuilder response = Response.ok((Object) file);
                response.header("Content-Disposition", "attachment; filename=\"test_excel_file.xlsx\"");
                response.header("token", autorizacion.encriptar());

                return response.build();                
                
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")   
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }  




        
        

    }
  
  

  

    
}



































