/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.certificaciones.cajachica;

import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugom_000
 */
public class CajaChicaSQL {
    
    
    
    
    public String lista_estructura ( Integer mes, Integer clase, Integer programa, 
            Integer actividad, Integer objeto, Integer ff, 
            Integer of, Integer dpto, String buscar)
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CajaChica");
        reader.fileExt = "lista_estructura.sql";
        
        sql = reader.get( mes, clase,  programa, 
             actividad, objeto,  ff, 
             of,  dpto, buscar );    
        
        
        return sql ;      
    }

    

    public String getTotalRequerido ( Integer mes, 
            Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of, Integer dpto  ) 
            throws Exception {
    
        
        
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CajaChica");
        reader.fileExt = "getTotalRequerido.sql";
        
        sql = reader.get( mes,
                clase, programa, actividad,
                obj, ff, of, dpto
                );    
        
        return sql ;      
    }
       
    
    
    
    

    public String dependenciameses ( Integer depe  ) 
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CajaChica");
        reader.fileExt = "dependenciameses.sql";
        
        sql = reader.get( depe );    
        
        return sql ;      
    }
           
    
    
    public String dependenciameses_suma ( Integer depe  ) 
            throws Exception {
    
        String sql = "";                                 
        
        sql = sql + " select sum(monto_certificado) monto_certificado  ";
        sql = sql + " from ( ";
        sql = sql + this.dependenciameses(depe);
        sql = sql + " ) as t";
        
        return sql ;      
    }
           
    
    
    
    
}
