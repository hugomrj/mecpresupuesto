/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.basica.dependencia;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;

/**
 *
 * @author hugom_000
 */
public class DependenciaDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public DependenciaDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Dependencia>  all () {
                
        List<Dependencia>  lista = null;        
        try {                        
                        
            DependenciaRS rs = new DependenciaRS();            
            lista = new Coleccion<Dependencia>().resultsetToList(
                    new Dependencia(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
