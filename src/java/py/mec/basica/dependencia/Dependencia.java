package py.mec.basica.dependencia;

/**
 *
 * @author hugom_000
 */
public class Dependencia {
    
    private Integer dependencia;    
    private Integer codigo;    
    private String nombre;

    public Integer getDependencia() {
        return dependencia;
    }

    public void setDependencia(Integer dependencia) {
        this.dependencia = dependencia;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
}

