/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.basica.meses;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class MesDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public MesDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Mes>  all () throws IOException, SQLException, Exception {
               
        
        
            ResultadoSet resSet = new ResultadoSet();        
            String sql = SentenciaSQL.select(new Mes()); 
            sql = sql + " order by mes " ;


            ResultSet rsData = resSet.resultset(sql);             
        
        
        List<Mes>  lista = null;        
        try {                        
                        
            //DepartamentoRS rs = new DepartamentoRS();            
            
            
            lista = new Coleccion<Mes>().resultsetToList(
                    new Mes(),
                    rsData
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
