/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.ejecucion.ejecuciondocumento;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 *
 * @author hugo
 */
public class EjecucionDocumentoMigracion {
    
    
    
    public void leer (String pathfile) {
    
        
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        System.out.println("HH:mm:ss-> "+dtf.format(LocalDateTime.now()));        
        
    
        
        EjecucionDocumento ejedoc = new EjecucionDocumento();
        EjecucionDocumentoFile ejeFile = new EjecucionDocumentoFile();
        
        
        
        String nombreFichero = pathfile ;
        BufferedReader br = null;
    
               
        
        try {

           br = new BufferedReader(new FileReader(nombreFichero));
           String texto = br.readLine();
           
           while(texto != null)
           {
               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() !=0 ){                    
                                
                    //planfinanciero = leerLinea (sSubCadena, planfinanciero ) ;      
                    
                    ejedoc = ejeFile.leerLinea(sSubCadena, ejedoc);
                    
                    
                    
                }                              
               texto = br.readLine();               
           }
           
           System.out.println("HH:mm:ss-> "+dtf.format(LocalDateTime.now()));        
           
        }
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }        
        
     }    
    
    

    
    
    

      
    



    
}
