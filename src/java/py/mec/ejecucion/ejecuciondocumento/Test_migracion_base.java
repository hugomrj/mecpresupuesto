package py.mec.ejecucion.ejecuciondocumento;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import nebuleuse.ORM.db.Persistencia;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author hugo
 */
public class Test_migracion_base {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
        throws FileNotFoundException, IOException, InvalidFormatException, SQLException {

        
        
        Persistencia persistencia = new Persistencia();   
        
        //String pathfile = "/home/hugo/NetBeansProjects/mecpresupuesto/web/WEB-INF/doc/ejecucion_presupuestaria_completo.txt" ;
        //String pathfile = "/home/hugo/NetBeansProjects/mecpresupuesto/web/WEB-INF/doc/ejecdoc_enero.txt" ;
        String pathfile = "/home/hugo/NetBeansProjects/mecpresupuesto/web/WEB-INF/doc/ejecucion_20220318.txt" ;
        
        EjecucionDocumentoMigracion migra = new EjecucionDocumentoMigracion();
        
        migra.leer(pathfile);
            

        //System.out.println( Datetime.castDate("22/01/2021") );        
        System.out.println("-------fin------");        
    
    }
}
