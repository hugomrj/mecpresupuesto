/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.ejecucion.ejecuciondocumento;




import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.util.Cadena;
import nebuleuse.util.Datetime;
import py.com.aplicacion.estructura.Estructura;
import py.com.aplicacion.migracion.Migracion;


/**
 *
 * @author hugom_000
 */

public class EjecucionDocumentoLinea  {
        
   
    
      
    public EjecucionDocumento insert ( EjecucionDocumento ejedoc, String linea ) throws SQLException {
     
        
        String line2  ="";                                  
        //line2  =  new Migracion().eliminarCaracteres(linea);  

        line2 =  new Migracion().formatearLinea(linea, "~");            
//        line2 =  line2.replaceAll("   ", ";");            
        String[] pa = line2.split("~");   
        
          
 
        
        
        if (pa.length >= 4) {
        
        //System.out.println(linea);   
        
            
            // los 4 elementos son int 
            if (Cadena.isInt(pa[0]) && Cadena.isInt(pa[1]) 
                    && Cadena.isInt(pa[2]) 
                    && Cadena.isInt(pa[3])
                    && (pa[3].length() == 3)                         
                    && (Cadena.isLong(pa[pa.length-1].replaceAll("\\.","")))
                    ){
                
                
                ejedoc = this.convertidor(line2, ejedoc);                     
                
                /*
                
                System.out.println("---A");   
                System.out.println(linea);   
                System.out.println(line2);   
                System.out.println("");   
                */
                
                
            }
            
            // los 3 elementos son int mas el 4 que es la descripcion no separada del objeto
            else if (Cadena.isInt(pa[0]) && Cadena.isInt(pa[1]) 
                    && Cadena.isInt(pa[2]) 
                    && (pa[3].length() > 3)                    
                    && (Cadena.isInt(pa[3].substring(0, 3)))                                        
                    && (Cadena.isLong(pa[pa.length-1].replaceAll("\\.","")))
                    ){             
                
                    ejedoc = this.convertidor(line2, ejedoc);                     
                    
                /*
                
                    System.out.println("---B");   
                    System.out.println(linea);                       
                    System.out.println(line2);                       
                    System.out.println("");                       
                  */  
                
            }
            else if (Cadena.isInt(pa[0]) && Cadena.isInt(pa[1]) 
                    && Cadena.isInt(pa[2])                     
                    && (pa[2].length() == 3) 
                    && (Cadena.isLong(pa[pa.length-1].replaceAll("\\.","")))
                    ){

                    ejedoc = this.convertidor(line2, ejedoc);                
                
                /*
                    
                    System.out.println("---C");   
                    System.out.println(linea);                       
                    System.out.println(line2);                       
                    System.out.println("");                       
                */
                
            }
            
            else if (Cadena.isInt(pa[0]) && Cadena.isInt(pa[1])                     
                    && (pa[1].length() == 3) 
                    && (!(Cadena.isInt(pa[2]))&& (pa[2].length() != 1))    
                    //&& (pa[2].matches("[+-]?\\d*(\\.\\d+)?"))    
                    ){

                    ejedoc = this.convertidor(line2, ejedoc);   
                    
                /*
                    
                    System.out.println("---D");   
                    System.out.println(linea);                                       
                    System.out.println(line2);                                       
                    System.out.println("");                                       
                */
            }            
            
            
            // empieza con objeto luego es separado por un espacio
            else if ((Cadena.isInt(linea.substring(0, 3)))                                        
                    && linea.charAt(3) == ' '
                    && (pa[1].length() != 1)
                    ){             

                    ejedoc = this.convertidor(line2, ejedoc);   
                    
                /*
                    
                    System.out.println("---F");   
                    System.out.println(linea);                   
                    System.out.println(line2);                   
                    System.out.println("");                   
                */
            }          
            
            
            // empieza con of y sigue con dato O o E
            // 302   O     79830                16/12/2021 
            else if ( (Cadena.isInt(pa[0]))                                        
                    && (pa[1].charAt(0) == 'O' || pa[1].charAt(0) == 'E' )                    
                    && (Cadena.isInt(pa[2]))
                    ){             

                    ejedoc = this.convertidor(line2, ejedoc); 
                
      /*          
                    System.out.println("---FF");   
                    System.out.println(linea);                   
                    System.out.println(line2);                   
                    System.out.println("");                   
        */        
                    
      
            }               
            
            

            // empieza con objeto luego es separado por un espacio y termina con un ff
            // 834 OTRAS TRANSFERENCIAS AL SECTOR PÚBLICO Y A 30~
            else if ((Cadena.isInt(linea.substring(0, 3)))                                        
                    && linea.charAt(3) == ' '
                    && ( Cadena.isInt(pa[0].substring(pa[0].length()-2, pa[0].length()) ))
                    ){             

                    ejedoc = this.convertidor(line2, ejedoc);
                
                /*
                    System.out.println("---FFF");   
                    System.out.println(linea);                   
                    System.out.println(line2);                   
                    System.out.println("");                   
                */
                    

            }            
            


            
            // fuente de financiamiento
            else if ( Cadena.isInt(pa[0]) && Cadena.isInt(pa[1]) 
                    && ( Integer.parseInt(pa[0]) == 10 
                    || Integer.parseInt(pa[0]) == 20 
                    || Integer.parseInt(pa[0]) == 30 )
                    )
                    {
                  
                    ejedoc = this.convertidor(line2, ejedoc);
                        
                /*                        
                    
                    System.out.println("---G");   
                    System.out.println(linea);                   
                    System.out.println(line2);                   
                    System.out.println("");                            
  */                 
      

            }            
            // organismo fin
            else if (( pa[1].charAt(0) == 'E' ||  pa[1].charAt(0)  == 'O' )
                    && ( pa[1].length() == 1 )   
                    && (Cadena.isLong(pa[pa.length-1].replaceAll("\\.","")))
                    )
                    {
                    
                    ejedoc = this.convertidor(line2, ejedoc);                        
                        
                /*
                    
                    System.out.println("---H");   
                    System.out.println(linea);                           
                    System.out.println(line2);      
                    System.out.println("");      
                */

            }
            
            
            else if (line2.charAt(0) == 'E' ||  line2.charAt(0) == 'O'){

                ejedoc = this.convertidor(line2, ejedoc);            
                
                /*
                
                System.out.println("---I");   
                System.out.println(linea);                           
                System.out.println(line2);             
                System.out.println("");             
*/

            }
            //6~111 SUELDOS~10~1~O~265~25/01/2022~PERSONAL~OBLIG. POR SUELDOS CORRESPONDIENTE AL MES DE~907.004.465~0
            else if (Cadena.isInt(pa[0])
                    && (Cadena.isLong(pa[pa.length-1].replaceAll("\\.","")))
                    && (Cadena.isInt( pa[1].substring(0, 3) ))                    
                    )
                {
                    
                    ejedoc = this.convertidor(line2, ejedoc);   
/*
                    
                    System.out.println("---J");   
                    System.out.println(linea);                    
                    System.out.println(line2);                    
                    System.out.println("");                                    
  */              



            }            
            
            
            else{
            

            
                    String[] line3 = line2.split("~");               
                    if (line3.length <= 6 ){
                        
                        if (!(Cadena.isInt(line3[0]) && Cadena.isInt(line3[1]) 
                            && Cadena.isInt(line3[2]) && Cadena.isInt(line3[3]))) 
                        {
                            
                            ejedoc = this.actualizarDescripcion(linea, ejedoc); 
                        
/*
                            System.out.println("---K");   
                            System.out.println(linea);                    
                            System.out.println(line2);   
                            System.out.println(line3.length);                          
                            System.out.println("");                                                                        
  */                          
                            
                        }
                    }
                    
                    // es mayor a 6
                    else{
                    
                        if (Cadena.isInt(line3[0]) && Cadena.isInt(line3[1]) 
                            && Cadena.isInt(line3[2]))
                        {
                            
                            ejedoc = this.actualizarDescripcion(linea, ejedoc); 
                            
/*
                            System.out.println("---K");   
                            System.out.println(linea);                    
                            System.out.println(line2);   
                            System.out.println(line3.length);                          
                            System.out.println("");                                                                        
*/
                            
                        }                       
                        
                    }
                    
                    
            

        
            }    
            
        } 
        else{         
          
            ejedoc = this.actualizarDescripcion(linea, ejedoc); 

               
            /*
            
            System.out.println("---L");   
            System.out.println(ejedoc.getId());   
            System.out.println(linea);                    
            System.out.println(line2);                    
            System.out.println(""); 
            
            
            */
            
        }
        
        
        

        
        
        
        return ejedoc;
    }
    
    
    
    
    
    
    
    
    

    public EjecucionDocumento convertidor ( String linea, EjecucionDocumento ejedoc ) {
        
        
        EjecucionDocumentoEstructura ede = new EjecucionDocumentoEstructura();
        EjecucionDocumentoDAO dao = new EjecucionDocumentoDAO();
                
        
        ArrayList<String> estruc = new ArrayList<String>();
        ArrayList<String> dato = new ArrayList<String>();
                
        String[] line3 = linea.split("~");          

        estruc = ede.getEstructura(line3);
        
        ejedoc = ede.setEstructura(ejedoc, estruc);
   
        dato = ede.getDato(line3);
        
        ejedoc = dao.precommit(ejedoc, dato);
        

        return ejedoc;
    }    
    
    
    

    public EjecucionDocumento actualizarDescripcion ( String linea, EjecucionDocumento ejedoc ) 
            throws SQLException {
        
        EjecucionDocumentoDAO dao = new EjecucionDocumentoDAO();
   
        
        dao.updateConcepto(ejedoc.getId(), linea);
        

        return ejedoc;
    }    
    
    
    
    
 
    
}


