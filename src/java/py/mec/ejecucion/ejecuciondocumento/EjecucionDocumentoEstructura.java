/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.ejecucion.ejecuciondocumento;




import java.util.ArrayList;
import nebuleuse.util.Cadena;
import nebuleuse.util.Datetime;


/**
 *
 * @author hugom_000
 */

public class EjecucionDocumentoEstructura  {
        

    
    
    
    
    public  ArrayList<String> getEstructura (  String[] linea ) {        
        
        ArrayList<String> ret = new ArrayList<String>();
        
        Integer divisor = 0;
        String str = "";

        if (linea.length > 0) {            
            Integer i = 0;
            
            while (divisor == 0 && i < linea.length  ){

                str = linea[i];                
                if (str.equals("O")  || str.equals("E")){
                    divisor = i;                    
                    i = linea.length;
                }                       
                i++;    
            }            
        }
        

        if (divisor > 0){
            for (int y = 0; y < divisor; y++) {
                ret.add(linea[y]);           
            }        
        }
    

        return ret ;
    }    
    
    
    
        
    public  EjecucionDocumento setEstructura ( EjecucionDocumento ejedoc,
            ArrayList<String> estruc  ) {
    
        
        
     
        
        if (estruc.size() > 0 ){
        /*   for (String string : estruc) {
                System.out.println(string + " - " );            
            }                                
        */
        

        
            if (estruc.size() == 7 
                    && ( estruc.get(3).length() == 3 ) ){
               

                ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                ejedoc.setPrograma(Integer.parseInt(estruc.get(1)));
                ejedoc.setActividad(Integer.parseInt(estruc.get(2)));
                
                ejedoc.setObj(Integer.parseInt(estruc.get(3)));
                ejedoc.setFf(Integer.parseInt(estruc.get(5)));
                ejedoc.setOf(Integer.parseInt(estruc.get(6)));
            
/*                
                System.out.println(  ejedoc.getClase() +"-"+ ejedoc.getPrograma()
                        +"-"+ ejedoc.getActividad()+"-"+ ejedoc.getObj() 
                        +"-"+ ejedoc.getFf() +"-"+ ejedoc.getOf());
  */              
            }         
            
            
            //  1;1;1;111 SUELDOS;10;1; 
            else if (estruc.size() == 6                     
                    && ( !Cadena.isInt(estruc.get(3)) )
                    && (Cadena.isInt(estruc.get(3).substring(0, 3)))
                    && (estruc.get(3).substring(3, 4).equals(" ") )
                    ){
            

                ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                ejedoc.setPrograma(Integer.parseInt(estruc.get(1)));
                ejedoc.setActividad(Integer.parseInt(estruc.get(2)));
                
                ejedoc.setObj(Integer.parseInt( estruc.get(3).substring(0, 3)));
                ejedoc.setFf(Integer.parseInt(estruc.get(4)));
                ejedoc.setOf(Integer.parseInt(estruc.get(5)));
            
            }            
            
            else if (estruc.size() == 6 
                    && ( estruc.get(2).length() == 3 ) ){

                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                ejedoc.setActividad(Integer.parseInt(estruc.get(1)));
                
                ejedoc.setObj(Integer.parseInt(estruc.get(2)));
                ejedoc.setFf(Integer.parseInt(estruc.get(4)));
                ejedoc.setOf(Integer.parseInt(estruc.get(5)));
            
            }
            
            // [1, 1, 1, 834, OTRAS TRANSFERENCIAS AL SECTOR PÚBLICO Y A 30, 1]
            else if (estruc.size() == 6 
                    && ( estruc.get(3).length() == 3 ) 
                    && (Cadena.isInt(estruc.get(4).substring(
                            estruc.get(4).length()-2, estruc.get(4).length() )))
                    ){

                ejedoc.setClase(Integer.parseInt(estruc.get(0)));                
                ejedoc.setPrograma(Integer.parseInt(estruc.get(1)));
                ejedoc.setActividad(Integer.parseInt(estruc.get(2)));
                
                ejedoc.setObj(Integer.parseInt(estruc.get(3)));
                
                //ejedoc.setFf(Integer.parseInt(estruc.get(4)));
                ejedoc.setFf(Integer.parseInt( estruc.get(4).substring(
                        estruc.get(4).length()-2, 
                        estruc.get(4).length() )));                
                
                ejedoc.setOf(Integer.parseInt(estruc.get(5)));
                
                System.out.println("size() D" );
            
            }
            
            
            else if (estruc.size() == 5 
                    && ( estruc.get(1).length() == 3 ) ){
            

                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                ejedoc.setActividad(Integer.parseInt(estruc.get(0)));
                
                ejedoc.setObj(Integer.parseInt(estruc.get(1)));
                ejedoc.setFf(Integer.parseInt(estruc.get(3)));
                ejedoc.setOf(Integer.parseInt(estruc.get(4)));
            
            }

            
            else if (estruc.size() == 4 
                    && ( estruc.get(0).length() == 3 ) ){
                
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                //ejedoc.setActividad(Integer.parseInt(estruc.get(0)));
                
                ejedoc.setObj(Integer.parseInt(estruc.get(0)));
                ejedoc.setFf(Integer.parseInt(estruc.get(2)));
                ejedoc.setOf(Integer.parseInt(estruc.get(3)));
            
            }
            
            //6~111 SUELDOS~10~1~
            else if (estruc.size() == 4                     
                    && (Cadena.isInt(estruc.get(0)) )
                    && (Cadena.isInt(estruc.get(1).substring(0, 3)))
                    && (Cadena.isInt(estruc.get(2)) )
                    && (Cadena.isInt(estruc.get(3)) )
                    ){
                
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                
                ejedoc.setActividad(Integer.parseInt(estruc.get(0)));                
                ejedoc.setObj(Integer.parseInt( estruc.get(1).substring(0, 3) ));                
                ejedoc.setFf(Integer.parseInt(estruc.get(2)));
                ejedoc.setOf(Integer.parseInt(estruc.get(3)));
            
            }            
            
            
            // [114 AGUINALDO, 10, 1]
            else if (estruc.size() == 3 
                    && ((Cadena.isInt(estruc.get(0).substring(0, 3)))  )
                    && (estruc.get(1).length() == 2 ) 
                    )
            
            {
            
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                //ejedoc.setActividad(Integer.parseInt(estruc.get(0)));

                ejedoc.setObj(Integer.parseInt(estruc.get(0).substring(0, 3)));
                ejedoc.setFf(Integer.parseInt(estruc.get(1)));
                ejedoc.setOf(Integer.parseInt(estruc.get(2)));
            
            }            
            //  [834, OTRAS TRANSFERENCIAS AL SECTOR PÚBLICO Y A 30, 1]
            else if (estruc.size() == 3 
                    && ( Cadena.isInt(estruc.get(0)) ) 
                    && (Cadena.isInt(estruc.get(1).substring(
                            estruc.get(1).length()-2, estruc.get(1).length() )))
                    && ( Cadena.isInt(estruc.get(2)) ) 
                    )
            
            {
            
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                //ejedoc.setActividad(Integer.parseInt(estruc.get(0)));

                ejedoc.setObj(Integer.parseInt(estruc.get(0)));
                ejedoc.setFf(Integer.parseInt( estruc.get(1).substring(
                        estruc.get(1).length()-2, 
                        estruc.get(1).length() )));
                ejedoc.setOf(Integer.parseInt(estruc.get(2)));
            
                
                
            }            
            
            //[910, PAGO DE IMPUESTOS, TASAS, GASTOS JUDICIALES]
            else if (estruc.size() == 2                     
                    &&  (Cadena.isInt(estruc.get(0)))
                    &&  (estruc.get(0).length() == 3)
                    && (!(Cadena.isInt(estruc.get(1).substring(
                            estruc.get(1).length()-2, estruc.get(1).length()))))
                    )
            
            {
            
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                //ejedoc.setActividad(Integer.parseInt(estruc.get(0)));

                ejedoc.setObj(Integer.parseInt(estruc.get(0)));
                //ejedoc.setFf(Integer.parseInt(estruc.get(1)));
                //ejedoc.setOf(Integer.parseInt(estruc.get(0)));
                System.out.println("size() E" );

            }            
                         
            //[10, 1]
            else if (estruc.size() == 2                     
                    &&  (Cadena.isInt(estruc.get(0)))
                    &&  (estruc.get(0).length() == 2)
                    &&  (Cadena.isInt(estruc.get(1)))
                    )
            
            {
            
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                //ejedoc.setActividad(Integer.parseInt(estruc.get(0)));

                //ejedoc.setObj(Integer.parseInt(estruc.get(0)));
                ejedoc.setFf(Integer.parseInt(estruc.get(0)));
                ejedoc.setOf(Integer.parseInt(estruc.get(1)));
                System.out.println("size() F" );

            }            
                         
            //  [834 OTRAS TRANSFERENCIAS AL SECTOR PÚBLICO Y A 30, 1]
            else if (estruc.size() == 2                     
                    && ((Cadena.isInt(estruc.get(0).substring(0, 3))))
                    && (Cadena.isInt(estruc.get(0).substring(
                        estruc.get(0).length()-2, estruc.get(0).length() )))
                    && (Cadena.isInt(estruc.get(1)))
                    )
            
            {
            
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                //ejedoc.setActividad(Integer.parseInt(estruc.get(0)));

                ejedoc.setObj(Integer.parseInt(((estruc.get(0).substring(0, 3)))) );
                ejedoc.setFf(Integer.parseInt( ((estruc.get(0).substring(
                        estruc.get(0).length()-2, estruc.get(0).length() ))) ));
                ejedoc.setOf(Integer.parseInt(estruc.get(1)));
                //System.out.println("size() G" );

            }             
            
            
            
            
            else if (estruc.size() == 1                     
                    &&  (Cadena.isInt(estruc.get(0)))
                    )
            
            {
            
                //ejedoc.setClase(Integer.parseInt(estruc.get(0)));
                //ejedoc.setPrograma(Integer.parseInt(estruc.get(0)));
                //ejedoc.setActividad(Integer.parseInt(estruc.get(0)));

                //ejedoc.setObj(Integer.parseInt(estruc.get(0).substring(0, 3)));
                //ejedoc.setFf(Integer.parseInt(estruc.get(1)));
                ejedoc.setOf(Integer.parseInt(estruc.get(0)));

            }            
            else if (estruc.size() == 1                     
                    &&  (!(Cadena.isInt(estruc.get(0))))
                    )            
            {
                // solo letras
            }            
                                      
            
            
            else{
                /*
                System.out.println("estruc.size() " +  estruc);
                System.out.println(estruc.size());            
                */
            }
                        

            
            
        }            
        /*
        System.out.println("return estruc.size() " +  estruc);
        System.out.println(estruc.size());            
    */
        
        return ejedoc;
    }
    
    
    
    
    
    public  ArrayList<String> getDato (  String[] linea ) {        
        
        ArrayList<String> ret = new ArrayList<String>();
        
        boolean quiebre = false;
        //String str = "";

        
        for (int i = 0; i < linea.length; i++) {
            String str = linea[i].trim();
            //System.out.println(str);
            
            
            if (quiebre == false) {
                if (str.equals("O")  || str.equals("E")){
                    quiebre = true;
                    ret.add(str);                
                }            
            }
            else{
                ret.add(str);                
            }
        }
        

        
        return ret ;
    }    
    

}


