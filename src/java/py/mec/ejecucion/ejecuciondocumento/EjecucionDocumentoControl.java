/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.ejecucion.ejecuciondocumento;




import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.util.Cadena;
import nebuleuse.util.Datetime;
import py.com.aplicacion.estructura.Estructura;
import py.com.aplicacion.migracion.Migracion;


/**
 *
 * @author hugom_000
 */

public class EjecucionDocumentoControl  {
        
    
    public  ArrayList<String> control01 ( ArrayList<String> dato  ) {
    
            // hacer un control de linea
            //O~41455~17/08/2021~4695094-0~OBLIGACION~PARA JORNALES~( BONIFICACION POR~657.852~0
            // el 3er elemento tiene que ser O o E
            // y el 4ro elemento tiene que ser numerico
                
            String aux = "";
            String ExpFecha = "^(0?[1-9]|[12][0-9]|3[01])[\\/](0?[1-9]|1[012])[/\\\\/](19|20)\\d{2}$";
            
//System.out.println("control == " + dato.size() );            
            
            if (dato.size() == 9){
 
                
                char str = dato.get(2).charAt(0);                                
                
                if (!(str == 'O'  || str == 'E')){
                    
                    
                    
                    // si los 2 ultimos campos son numeros
                    if ( Cadena.isLong(dato.get(7).replaceAll("\\.","")) && 
                        Cadena.isLong(dato.get(8).replaceAll("\\.","")) ){
                        
                        dato.set(4, dato.get(4) +" "+ dato.get(5) +" "+ dato.get(6));
                        dato.remove(5);
                        dato.remove(5);                        
                    }
                    
                    
                    else if( (dato.get(1).charAt(dato.get(1).length()-1)== 'O' 
                          || dato.get(1).charAt(dato.get(1).length()-1) == 'E'
                          && dato.get(1).charAt(dato.get(1).length()-2) == ' '
                          && dato.get(7).equals("-")
                         ))
                    {

                    System.out.println(                    
                        dato.get(1)
                    );
                    
                    System.out.println(                    
                        dato.get(1).substring(dato.get(1).length()-1, dato.get(1).length())
                    );
                    
                    dato.add(2, dato.get(1).substring(dato.get(1).length()-1, dato.get(1).length()) );
                    
                    dato.set(1, dato.get(1).substring(0, dato.get(1).length()-2)  );
                    
                    dato.set(8, "-" +dato.get(9) );
                    dato.remove(9);
                  
                    }
                                    
                    
                    
                    
                    
                    
                }                
                
                else if(dato.get(7).equals("-"))
                {
                    dato.set(8, "-"+ dato.get(8));
                    dato.set(7, dato.get(6));
                    dato.set(6, dato.get(5));
                    dato.set(5,null);
                
                }
                
                

                
                
            }
            
            
            
            
            
            
            
            else if (dato.size() == 10) {
                
                // si ambos ultimos campos son numericos
                char str1 = dato.get(0).charAt(0);                                
                char str2 = dato.get(2).charAt(0);                                
                //String ExpFecha = "^(0?[1-9]|[12][0-9]|3[01])[\\/](0?[1-9]|1[012])[/\\\\/](19|20)\\d{2}$";
                
                
                if ( (str1 == 'O'  || str1 == 'E') && (str2 == 'O'  || str2 == 'E') 
                    && dato.get(4).matches(ExpFecha)                        
                    && Cadena.isLong(dato.get(8).replaceAll("\\.",""))  
                    && Cadena.isLong(dato.get(9).replaceAll("\\.","")) ){

                    dato.set(6, dato.get(6) +" "+ dato.get(7));
                    dato.set(7, dato.get(8));
                    dato.set(8, dato.get(9));                    
                    dato.remove(9);
                }                
                
                
                
                char str = dato.get(2).charAt(0);      
                str = dato.get(8).charAt(0);        
                if ( str == '-' && Cadena.isLong(dato.get(9).replaceAll("\\.","")) ){

                    dato.set(8, "-"+ dato.get(9));
                    dato.remove(9);
                }                       
                
                

                str = dato.get(0).charAt(0);                                                
                if ((str == 'O'  || str == 'E') &&  dato.get(2).matches(ExpFecha) 
                        && Cadena.isLong(dato.get(8).replaceAll("\\.",""))
                        && Cadena.isLong(dato.get(9).replaceAll("\\.",""))
                        ) {
                

                    dato.set(3, dato.get(3) +" "+ dato.get(4) 
                            +" "+ dato.get(5)+" "+ dato.get(6)+" "+ dato.get(7)  );
                    dato.remove(4); 
                    dato.remove(4); 
                    dato.remove(4); 
                    
                }   

                
                
                
                
                
            }
            else if (dato.size() == 8) {
                
                char str1 = dato.get(0).charAt(0);                                
                char str2 = dato.get(2).charAt(0);                                
            
                //String ExpFecha = "^(0?[1-9]|[12][0-9]|3[01])[\\/](0?[1-9]|1[012])[/\\\\/](19|20)\\d{2}$";
                
                if ((str1 == 'O'  || str1 == 'E') &&  dato.get(2).matches(ExpFecha) ) {
 
                    dato.set(4, dato.get(4) +" "+ dato.get(5) );
                    dato.remove(5);
                    
                }
                else if ( (str1 == 'O'  || str1 == 'E') && (str2 == 'O'  || str2 == 'E') 
                    && dato.get(4).matches(ExpFecha)
                    && Cadena.isLong(dato.get(6).replaceAll("\\.",""))
                    && Cadena.isLong(dato.get(7).replaceAll("\\.",""))
                        ){ 
                
                    dato.add(8, " ");
                    dato.set(8,dato.get(7) );
                    dato.set(7,dato.get(6) );
                    dato.set(6," " );
                    
                    //dato.remove(5);
                    // agregar un dato mas para convertirle en tamaño 9
                    
                }
                    
                //Cadena.isLong(dato.get(8).replaceAll("\\.",""))
            }


            else if (dato.size() == 11) {
                                
                
                char str1 = dato.get(0).charAt(0);                                
                char str2 = dato.get(2).charAt(0);                                
            
                //String ExpFecha = "^(0?[1-9]|[12][0-9]|3[01])[\\/](0?[1-9]|1[012])[/\\\\/](19|20)\\d{2}$";


                // tipo y ref, fecha, ultimo campo numerico y penultimo -
                if ( (str1 == 'O'  || str1 == 'E') && (str2 == 'O'  || str2 == 'E') 
                    && dato.get(4).matches(ExpFecha)                        
                    && Cadena.isLong(dato.get(8).replaceAll("\\.",""))  
                    && dato.get(9).charAt(0)  == '-'    
                    && Cadena.isLong(dato.get(10).replaceAll("\\.","")) ){

                    
                        dato.set(6, dato.get(6) +" "+ dato.get(7));                        
                        dato.set( 9, "-" +dato.get(10) );
                        dato.remove(7);
                        dato.remove(9);
                }                
                
                else if ( (str1 == 'O'  || str1 == 'E') && (str2 == 'O'  || str2 == 'E')                        
                        &&  dato.get(4).matches(ExpFecha)) {
                    
                    dato.set(6, dato.get(6) +" "+ dato.get(7) +" "+ dato.get(8) );
                    dato.remove(7);
                    dato.remove(7);
                    
                }
                
                
                //Cadena.isLong(dato.get(8).replaceAll("\\.",""))
            }
            
            
            else if (dato.size() == 6) {
                                
                
                char str1 = dato.get(0).charAt(0);                                
                char str2 = dato.get(2).charAt(0);                                
            
                //String ExpFecha = "^(0?[1-9]|[12][0-9]|3[01])[\\/](0?[1-9]|1[012])[/\\\\/](19|20)\\d{2}$";
                
                if ( (str1 == 'O'  || str1 == 'E')                         
                        &&  dato.get(2).matches(ExpFecha)
                        && Cadena.isLong(dato.get(4).replaceAll("\\.",""))
                        && Cadena.isLong(dato.get(5).replaceAll("\\.",""))                        
                        ) {
 
                    dato.add(3, "");
                    
                    
                }

                    
                //Cadena.isLong(dato.get(8).replaceAll("\\.",""))
            }
                          
            
            
            
            
            
            
    
        return dato;
    }
        
    
    
    
    
}


