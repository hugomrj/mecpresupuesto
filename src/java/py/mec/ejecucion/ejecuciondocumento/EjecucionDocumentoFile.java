/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.mec.ejecucion.ejecuciondocumento;

import java.sql.SQLException;
import py.com.aplicacion.estructura.Estructura;
import py.com.aplicacion.migracion.Migracion;



public class EjecucionDocumentoFile {
    
    
    
     
    public EjecucionDocumento leerLinea ( String linea , EjecucionDocumento ejedoc ) 
            throws SQLException {
        
        

        
        EjecucionDocumentoLinea ejelinea = new EjecucionDocumentoLinea();
        
        String cnt = linea.toString().trim();  


        if (cnt.contains("----------------------")) {                   
            return ejedoc;
        }                    
        else if (cnt.contains("                                                           ")) {        
            return ejedoc;
        } 
        else if (cnt.contains("Snips afec.")) {        
            return ejedoc;
        }           
        else if (cnt.startsWith("'MINIST.")) {        
            return ejedoc;
        }            
        else if (cnt.startsWith("S.S.E.A.F")) {        
            return ejedoc;
        }            
        else if (cnt.startsWith("CRIGAS01Z")) {        
            return ejedoc;
        }    
        else if (cnt.startsWith("Estructura Programática")) {        
            return ejedoc;
        }    
        else if (cnt.startsWith("F e c h a")) {        
            return ejedoc;
        }    
        else if (cnt.startsWith("Cla   Pro")) {        
            return ejedoc;
        }    
        else if (cnt.startsWith("Entidad:")) {        
            return ejedoc;
        }    
        else if (cnt.startsWith("Unidad:")) {        
            return ejedoc;
        }    
        else if (cnt.startsWith("SIAF")) {        
            return ejedoc;
        }   
        else if (cnt.startsWith("Snips afec.")) {        
            return ejedoc;
        }   
        else if (cnt.startsWith("Cla    Pro   Act/Pry")) {        
            return ejedoc;
        }   
        
        
        else{
        
                        
            ejedoc = ejelinea.insert(ejedoc, linea);
            
                       
        }
        
        
        
        return ejedoc;
    }
    
    
    
}
