/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.mec.ejecucion.ejecuciondocumento;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import static jakarta.ws.rs.client.Entity.json;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Webinf;
import py.com.aplicacion.presupuesto.PresupuestoJSON;


/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("ejecuciondocumento")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class EjecucionDocumentoWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    
    
                         
    public EjecucionDocumentoWS() {
        
    }

    
    

    
    @GET
    @Path("/consulta1")
    public  Response proceso (  
            @HeaderParam("token") String strToken,            
            @MatrixParam("clase") Integer clase,
            @MatrixParam("prog") Integer prog,
            @MatrixParam("acti") Integer acti,
            @MatrixParam("obj") Integer obj,            
            @MatrixParam("ff") Integer ff,
            @MatrixParam("of") Integer of,
            @MatrixParam("fe1") String fe1,
            @MatrixParam("fe2") String fe2
    ) {
        
                
        String json = "";
                    
        try {
                        
            if (clase == null){clase = 0;}
            if (prog == null){prog = 0;}
            if (acti == null){acti = 0;}
            
            if (obj == null){obj = 0;}
            if (ff == null){ff = 0;}
            if (of == null){of = 0;}
            
                        
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                EjecucionDocumentoJSON ejedoc = new EjecucionDocumentoJSON();
                           
                
                JsonObject jsonarray =  ejedoc.consulta1(clase, prog, acti,
                        obj, ff, of,
                        fe1, fe2);     
                                                
                //json = jsonarray.toString().trim();                       
                                
                return Response
                        .status( this.status )
                        .entity( jsonarray.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                
                
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }                      
         
        
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

    
       

    
    @GET
    @Path("/consulta2")
    public  Response consulta2 (  
            @HeaderParam("token") String strToken,                        
            @MatrixParam("concepto") String concepto
    ) {
        
        
System.out.println("consulta2");        
        String json = "";
                    
        try {
            
                        
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                EjecucionDocumentoJSON ejedoc = new EjecucionDocumentoJSON();
                           
                JsonObject jsonarray =  ejedoc.consulta2( concepto);     

                return Response
                        .status( this.status )
                        .entity( jsonarray.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                
                
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }                      
         
        
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

        
    

    
}


