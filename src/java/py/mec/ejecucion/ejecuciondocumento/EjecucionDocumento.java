/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.mec.ejecucion.ejecuciondocumento;

import java.util.Date;
import py.com.aplicacion.estructura.Estructura;

/**
 *
 * @author hugo
 */
public class EjecucionDocumento {
    
    private Integer id;
    
    private Integer clase;
    private Integer programa;
    private Integer actividad;
    
    private Integer obj;
    private Integer ff;
    private Integer of;

    private String doc_tipo;
    private Integer doc_num;
    private String ref_tipo;
    private Integer ref_num;
    private Date fecha;
    private String rcp;
    private String concepto;
    private Long obligado;
    private Long pagado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClase() {
        return clase;
    }

    public void setClase(Integer clase) {
        this.clase = clase;
    }

    public Integer getPrograma() {
        return programa;
    }

    public void setPrograma(Integer programa) {
        this.programa = programa;
    }

    public Integer getActividad() {
        return actividad;
    }

    public void setActividad(Integer actividad) {
        this.actividad = actividad;
    }

    public Integer getObj() {
        return obj;
    }

    public void setObj(Integer obj) {
        this.obj = obj;
    }

    public Integer getFf() {
        return ff;
    }

    public void setFf(Integer ff) {
        this.ff = ff;
    }

    public Integer getOf() {
        return of;
    }

    public void setOf(Integer of) {
        this.of = of;
    }



    public Integer getDoc_num() {
        return doc_num;
    }

    public void setDoc_num(Integer doc_num) {
        this.doc_num = doc_num;
    }


    public Integer getRef_num() {
        return ref_num;
    }

    public void setRef_num(Integer ref_num) {
        this.ref_num = ref_num;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRcp() {
        return rcp;
    }

    public void setRcp(String rcp) {
        this.rcp = rcp;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Long getObligado() {
        return obligado;
    }

    public void setObligado(Long obligado) {
        this.obligado = obligado;
    }

    public Long getPagado() {
        return pagado;
    }

    public void setPagado(Long pagado) {
        this.pagado = pagado;
    }

    public String getDoc_tipo() {
        return doc_tipo;
    }

    public void setDoc_tipo(String doc_tipo) {
        this.doc_tipo = doc_tipo;
    }

    public String getRef_tipo() {
        return ref_tipo;
    }

    public void setRef_tipo(String ref_tipo) {
        this.ref_tipo = ref_tipo;
    }

    
}


//1~1~1~111~SUELDOS~10~1~O~63~22/01/2021~PERSONAL~OBLIG. POR SUELDOS - CORRESPONDIENTE AL MES DE~6.521.210.695~0