/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.mec.ejecucion.ejecuciondocumento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.util.Datetime;
import py.com.aplicacion.estructura.Estructura;

/**
 *
 * @author hugo
 */
public class EjecucionDocumentoDAO {
    
    
    public Integer insertar (EjecucionDocumento ejedoc) {
    
        Integer ret = 0;
        
        try {
            
            Integer cod = 0;
            cod = this.getid(ejedoc);
            
        
            if (cod == 0) {                        
                            
                EjecucionDocumento eje = (EjecucionDocumento) new Persistencia().insert(ejedoc);                
                //System.out.println("codigo: " + eje.getId());
                ret = eje.getId();

            }
            else{
                //System.out.println("codigoE: " + cod);                
                ret = cod;
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
                        
        return ret;

    }
    
    
    
    public Integer updateConcepto (Integer id, String concepto) throws SQLException {
        
        Integer ret = id;


        Integer cod = 0;
        cod = this.getidConcepto(id, concepto);

        if (cod == 0){

            String sql =    " UPDATE ejecucion.ejecucion_documentos\n" +
                            " SET concepto =  concepto ||' '|| '"+concepto+"'" +
                            " WHERE id= " + id; 

            new Persistencia().ejecutarSQL(sql);       
            
        }
        return ret;

    }
    
    
    
    
    
    
    
    
    
    public Integer getid (EjecucionDocumento ejedoc) throws SQLException{
        
        Integer ret = 0;
        
        
        String sql =    " select id   \n" +
                        " from ejecucion.ejecucion_documentos  \n" +
                        " where doc_tipo like '"+ejedoc.getDoc_tipo()+"' " +
                        " and doc_num = "+ejedoc.getDoc_num() +
                        " and fecha = '"+ Datetime.toSQLDate(ejedoc.getFecha()) +"'" ;
                    
        
        
        
        ret = new Persistencia().ejecutarSQL ( sql, "id") ;        

        return ret;
    }
    
    
    
    
    public Integer getidConcepto (Integer id, String concepto) 
            throws SQLException{
        
        Integer ret = 0;
        
        
        String sql =    " SELECT id, concepto\n" +
                        " FROM ejecucion.ejecucion_documentos\n" +
                        " WHERE id = " + id  +
                        " AND concepto like '%"+concepto+"%' " ;
                    
        
        
        
        ret = new Persistencia().ejecutarSQL ( sql, "id") ;        

        return ret;
    }
    
    
        
    
    
    
        
    
    public  EjecucionDocumento precommit ( EjecucionDocumento ejedoc,
            ArrayList<String> dato  ) {
        
        EjecucionDocumentoDAO dao = new EjecucionDocumentoDAO();
        EjecucionDocumentoControl cntr = new EjecucionDocumentoControl();
        
        
        
        String mostrar = "";
        Integer cod = 0;
        
        // enviar a base de datos


        if (dato.size() > 0){

            dato = cntr.control01(dato);

            if (dato.size() == 7){
                
                ejedoc.setDoc_tipo( dato.get(0));
                ejedoc.setDoc_num(Integer.parseInt(dato.get(1)));                
                ejedoc.setFecha( Datetime.castDate(dato.get(2)) );
                ejedoc.setRef_tipo(null);
                ejedoc.setRef_num(null);  
                
                
                ejedoc.setRcp(dato.get(3));
                ejedoc.setConcepto(dato.get(4));
    
                ejedoc.setObligado( Long.parseLong(   dato.get(5).replaceAll("\\.","")   )  );
                ejedoc.setPagado(Long.parseLong(dato.get(6).replaceAll("\\.","")  ));
                

                
                // guardar
                cod = dao.insertar(ejedoc);
            
            }
            else if (dato.size() == 9){
                
                ejedoc.setDoc_tipo( dato.get(0));
                ejedoc.setDoc_num(Integer.parseInt(dato.get(1)));    
                ejedoc.setRef_tipo(dato.get(2));
                ejedoc.setRef_num(Integer.parseInt(dato.get(3)));    
                
                ejedoc.setFecha( Datetime.castDate(dato.get(4)) );
                ejedoc.setRcp(dato.get(5));
                ejedoc.setConcepto(dato.get(6));
    
                ejedoc.setObligado( Long.parseLong(   dato.get(7).replaceAll("\\.","")   )  );
                ejedoc.setPagado(Long.parseLong(dato.get(8).replaceAll("\\.","")  ));
                
                
                // guardar
                cod =  dao.insertar(ejedoc);
                
                
            
            }
        
        }
        
    
        ejedoc.setId(cod);
        
        return ejedoc;
    }
        
        
    
    
    
    public List<EjecucionDocumento>  lista ( ResultSet resultSet ) {
                
        List<EjecucionDocumento>  lista = null;        
    
        
        try {                        
        
            lista = new Coleccion<EjecucionDocumento>().resultsetToList(
                        new EjecucionDocumento(), 
                        resultSet
                    );                            
            
             
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    
    
    
        
    
    
}




//1~1~1~111~SUELDOS~10~1~O~63~22/01/2021~PERSONAL~OBLIG. POR SUELDOS - CORRESPONDIENTE AL MES DE~6.521.210.695~0