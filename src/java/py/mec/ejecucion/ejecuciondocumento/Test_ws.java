package py.mec.ejecucion.ejecuciondocumento;

import com.google.gson.JsonObject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author hugo
 */
public class Test_ws {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
        throws FileNotFoundException, IOException, InvalidFormatException, SQLException, Exception {

        

        String json = "";
        
        EjecucionDocumentoJSON ejedoc = new EjecucionDocumentoJSON();
        /*
        JsonObject jsonarray = new EjecucionDocumentoJSON().consulta1(1, 1, 1,
        244, 10, 1, '20000101', '22220101');
         */
        
        
        JsonObject jsonarray =  ejedoc.consulta2("subsidio");        
        
                
        json = jsonarray.toString().trim();                       
                
        
        System.out.println(json);    

    }
}
