/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.ejecucion.ejecuciondocumento;


import py.com.aplicacion.migracion.inicial.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.aplicacion.migracion.Migracion;




/**
 *
 * @author hugo
 */
public class EjecucionDocumentoSQL {
    

      
    
    
    public String consulta1 ( Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of,
            String fecha1, String fecha2)
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("EjecucionDocumento");
        readerSQL.fileExt = "consulta1.sql";
        
        
        // fechas
        if (fecha1.equals("") || fecha2.equals("")) {            
            fecha1 = "10000101";
            fecha2 = "10000102";
        }        
        
        
        
        sql = readerSQL.get( clase, programa, actividad, obj, ff, of,
                fecha1, fecha2);
                
        
        
    
        return sql ;             
    }        
        
    

    
    public String consulta1_suma ( Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of,
            String fecha1, String fecha2)
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("EjecucionDocumento");
        readerSQL.fileExt = "consulta1_suma.sql";
        
        // fechas
        if (fecha1.equals("") || fecha2.equals("")) {            
            fecha1 = "10000101";
            fecha2 = "10000102";
        }        
                
        
        
        sql = readerSQL.get( clase, programa, actividad, obj, ff, of,
                fecha1, fecha2);
                
        
        return sql ;             
    }        
        
    
        
    
    
    
    public String consulta2 ( String concepto )
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("EjecucionDocumento");
        readerSQL.fileExt = "consulta2.sql";
        
        sql = readerSQL.get( concepto );
             
        return sql ;             
    }        
            
    
    
    
    public String consulta2_suma ( String concepto )
            throws Exception {
    
        String sql = "";                         
        ReaderT readerSQL = new ReaderT("EjecucionDocumento");
        readerSQL.fileExt = "consulta2_suma.sql";
                        
        sql = readerSQL.get( concepto );
                        
        return sql ;             
    }        
        
        
    
    
    
    
}

