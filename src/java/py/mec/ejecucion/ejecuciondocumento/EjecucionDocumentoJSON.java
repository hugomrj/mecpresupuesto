/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.ejecucion.ejecuciondocumento;

import py.com.aplicacion.presupuesto.consulta.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Map;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class EjecucionDocumentoJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public EjecucionDocumentoJSON ( ) throws IOException  {
    
    }
      
    
    

    public JsonObject  consulta1 (  Integer clase, Integer programa, Integer actividad,
            Integer obj, Integer ff, Integer of , 
            String fecha1, String fecha2 ) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        EjecucionDocumentoSQL oSql = new EjecucionDocumentoSQL();
        
        
        
        try 
        {   
  

            String sql = oSql.consulta1(
                    clase, programa, actividad, obj, ff, of ,
                    fecha1, fecha2);
                       
            
            
            
            ResultSet rsData = new ResultadoSet().resultset(sql);                
            
         
            EjecucionDocumentoDAO dao = new EjecucionDocumentoDAO();                
            List<EjecucionDocumento> lista = dao.lista(rsData);                
         
        
         
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
        
        /*
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
          */
        
        
        
        
            // sumas            
            
            sql = oSql.consulta1_suma(
                    clase, programa, actividad, obj, ff, of ,
                    fecha1, fecha2);
                    
                         
            
            ResultSet rsSuma = new ResultadoSet().resultset(sql);   
            
            JsonObject jsonSuma = new JsonObject();
            jsonSuma = new JsonObjeto().json_suma( rsSuma );
        
                
            
            // union de partes

            jsonObject.add("datos", jsonArrayDatos);                                        
            jsonObject.add("summary", jsonSuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    

    
    
    
    

    public JsonObject  consulta2 ( String concepto ) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        EjecucionDocumentoSQL oSql = new EjecucionDocumentoSQL();
        
        
        
        try 
        {   
  

            if (concepto.trim().equals("")) {
                concepto = "------------------------------------------------";
            }
                       
            
            String sql = oSql.consulta2( concepto );
            
            ResultSet rsData = new ResultadoSet().resultset(sql);                
            
         
            EjecucionDocumentoDAO dao = new EjecucionDocumentoDAO();                
            List<EjecucionDocumento> lista = dao.lista(rsData);                
         
        
         
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
        
       
        
            // sumas            
            
            sql = oSql.consulta2_suma( concepto );
                    
                         
            
            ResultSet rsSuma = new ResultadoSet().resultset(sql);   
            
            JsonObject jsonSuma = new JsonObject();
            jsonSuma = new JsonObjeto().json_suma( rsSuma );
        


            jsonObject.add("datos", jsonArrayDatos);                                        
            jsonObject.add("summary", jsonSuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    

    
            
    

        
    
    
    
        
}
