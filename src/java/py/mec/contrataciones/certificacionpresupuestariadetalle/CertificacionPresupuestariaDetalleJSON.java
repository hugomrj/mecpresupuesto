/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.mec.contrataciones.certificacionpresupuestariadetalle;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;



/*
 * @author hugo
 */
public class CertificacionPresupuestariaDetalleJSON {
 
    
    
    public CertificacionPresupuestariaDetalleJSON ( ) throws IOException  {
    
    }
      
          
    
    

    public JsonObject  codigo ( Integer id) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
            
            ResultadoSet resSet = new ResultadoSet();          
            CertificacionPresupuestariaDetalleSQL codigoSQL = new CertificacionPresupuestariaDetalleSQL();          
            String sql = "";            
            
            sql = codigoSQL.codigo(id);
            ResultSet rsData = resSet.resultset(sql);
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            

            //jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            
            
            resSet.close();
        }         
        catch (Exception ex) {                                    
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    
    
}
