/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.contrataciones.certificacionpresupuestariadetalle;

import py.mec.contrataciones.codigocontratacion.CodigoContratacion;
import java.sql.SQLException;
import nebuleuse.ORM.db.Persistencia;
import py.com.aplicacion.migracion.Migracion;



/**
 *
 * @author hugo
 */
public class CertificacionPresupuestariaDetalleDAO extends CodigoContratacion {
   
    
    
    public CodigoContratacion gen ( String linea,  Integer ccid ) 
            throws SQLException, Exception{

        
        String numero = linea;    
                            
        numero = numero.replaceAll(" ", "").trim();   
        numero = numero.replaceAll(",", "").trim();   

        //System.out.println(linea); 

        if (numero.matches("\\d*")){
            //System.out.println("Es un número");
            linea = linea.replaceAll(",", "").trim();   
            
            this.insert(linea, ccid);
            
        }        

        
        return this;
    }
    
    

    public Integer insert ( String linea, Integer ccid ) throws SQLException{

        Integer intID = 0;
        
        linea =  new Migracion().recorrerLinea(linea.trim());    
        String[] partes = linea.split(";");                                                   
                                
        if (partes.length == 10) {                        
            String line = linea.substring(0, 5) + "12;7;" + linea.substring(5, linea.length());        
            partes = line.split(";"); 
        }
        
        //2019;12;7;1;1;0;0;310;30;1;99;81000000
        
        String strSQL =
                " INSERT INTO contrataciones.certificacion_presupuestaria_detalle(\n" +
                " codigo_contratacion, " +
                " agno, nivel, entidad, " +
                " tp, pg, sp, py, "+
                " obj, ff, of, dpt, " +
                " monto )\n" +
                " VALUES ("+ccid+","
                + partes[0] + "," + partes[1] + "," + partes[2] + "," 
                + partes[3] + "," + partes[4] + "," + partes[5] + "," + partes[6] + "," 
                + partes[7] + "," + partes[8] + "," + partes[9] + "," + partes[10] + "," 
                + partes[11] + " " +                
                " ) RETURNING id ; ";
        

        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );
        
        return intID;

    }
    
 


    public boolean  borarCertificacionDetalle (  Integer ccid ) throws SQLException{
                
        Persistencia persinstencia = new Persistencia();

        String strSQL =
                " DELETE FROM contrataciones.certificacion_presupuestaria_detalle\n" +
                " WHERE codigo_contratacion =  " + ccid + ";";
        
        boolean bool = persinstencia.ejecutarSQL( strSQL );
        
        return bool;

    }
                
    
    
    
    
    
}
