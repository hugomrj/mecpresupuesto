/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.mec.contrataciones.certificacionpresupuestariadetalle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Webinf;
import py.mec.contrataciones.codigocontratacion.CodigoContratacionJSON;




/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("certificacionpresupuetariadetalle")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class CertificacionPresupuetariaDetalleWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    
    
                         
    public CertificacionPresupuetariaDetalleWS() {
        
    }

  
    
    
         


    @GET
    @Path("/codigo/{codigo}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("codigo") Integer codigo ) {
                     
        try 
        {                  

            if (autorizacion.verificar(strToken))
            //if ( true )
            {
                autorizacion.actualizar();              
                
                String json = "";    
                JsonObject jsonObject;
                                                
                jsonObject 
                        = new CertificacionPresupuestariaDetalleJSON().codigo(codigo);
                               
                
                //
                
                return Response
                        .status( this.status )
                        .entity( jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }

            
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }        
        
    }    
      
        
    
    

       
        
    
    
}


