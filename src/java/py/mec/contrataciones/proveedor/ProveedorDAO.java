/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.contrataciones.proveedor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.postgres.Conexion;
import py.com.aplicacion.migracion.Migracion;
import py.mec.contrataciones.codigocontratacion.CodigoContratacionDAO;

/* 
 * @author hugo
 */
public class ProveedorDAO extends Proveedor{
    
    
    
    public ProveedorDAO gen ( String linea, String linea_anterior, 
            Integer ccid) 
            throws Exception {


        String control = "";
        String patron = "";

        control = linea_anterior.toString().trim();          
        patron = "Nombre / Razón Social del Proveedor y/o";
        if ( control.equals(patron) ) {            
            this.setNombre(linea);            
        }        
        

        control = linea.toString().trim();          
        patron = "Nombre / Razón Social del Proveedor";
        if ( control.startsWith(patron) ) {            
            linea = linea.replaceAll(patron, "").trim();                                    
            //this.setDomicilio_legal(linea);
            if (!(linea.trim().equals("y/o"))) {
                this.setNombre(linea);           
            }
        }        
        
        

        control = linea.toString().trim();          
        patron = "Domicilio Legal:";
        if ( control.startsWith(patron) ) {
            
            linea = linea.replaceAll(patron, "").trim();                                    
            this.setDomicilio_legal(linea);
            
        }        
        
        
                
        control = linea.toString().trim();          
        patron = "Nombre de Fantasía:";
        if ( control.startsWith(patron) ) {
            
            linea = linea.replaceAll(patron, "").trim();                                    
            this.setNombre_fantasia(linea);
            
        }        
    
        control = linea.toString().trim();          
        patron = "Representante Legal:";
        if ( control.startsWith(patron) ) {
            
            linea = linea.replaceAll(patron, "").trim();                                    
            this.setRepresentante_legal(linea);
            
        }                
        

        control = linea.toString().trim();          
        patron = "País de Origen:";
        if ( control.startsWith(patron) ) {
            
            linea = linea.replaceAll(patron, "").trim();                                    
            this.setPais_origen(linea);
            
        }                


        control = linea.toString().trim();          
        patron = "R.U.C. / IDAP";
        if ( control.startsWith(patron) ) {
            
            
            linea = linea.replaceAll("R.U.C. / IDAP:", "");                                    
            linea = linea.replaceAll("Tel. / Fax:", "");                                    
            linea = linea.replaceAll("Correo Electrónico:", "");                                    
            
            
            linea =  new Migracion().recorrerLinea(linea.trim());    
            String[] partes = linea.split(";");                                       
            
            if (partes.length == 3) {
                
                this.setRuc(partes[0]);
                this.setTelefono(partes[1]);
                this.setEmail(partes[2]);
                            
            }
            else{
                
                if (partes.length == 4) {

                    this.setRuc(partes[0]);
                    this.setTelefono(partes[1]+partes[2]);
                    this.setEmail(partes[3]);

                }                
                else{
                                        
                    if (partes.length == 1) {
                        this.setRuc(partes[0]);
                        this.setTelefono("");
                        this.setEmail("");
                    }                                
                }
            }
            
            
            // primer comprobar que no exita el mismo ruc
            Integer proveedorid = this.existe(this.getRuc());
            
            if (proveedorid == 0 ) {                                                
                Integer intID = this.insert();
                
                
                this.setProveedor(intID);
            }
            else{                
                this.setProveedor(proveedorid);
            }
            
            // relaciona proveedor con el codigo de contratacion
            
            new CodigoContratacionDAO().updateProveedor(
                   ccid , this.getProveedor());
            
            
            
        }                


        
        return this;
    }
    
  
    
       
    
    public Integer existe(String ruc) throws Exception {      
        
        Integer ret = 0;
        
        Conexion conexion = new Conexion();
        conexion.conectar();            

        
        Statement  statement ;
        ResultSet resultset;
        
                
        String sql = 
                    " SELECT proveedor\n" +
                    " FROM contrataciones.proveedores\n" +
                    " WHERE ruc like '" + ruc + "' " ;
                
          
        statement = conexion.getConexion().createStatement();                           
        resultset = statement.executeQuery( sql );     


        if(resultset.next()) 
        {                
            ret =  Integer.parseInt(resultset.getString("proveedor")); 
        }

        resultset.close();
        conexion.desconectar(); 
  
        return ret;          

      }
     
        
    

    public Integer insert (  ) throws SQLException{

        Integer intID = 0;
        
        String strSQL =
                " INSERT INTO contrataciones.proveedores\n" +
                " (nombre, domicilio_legal, nombre_fantasia, representante_legal, "
                + " pais_origen, ruc, telefono, email)\n" +
                " VALUES( "
                + "'"+this.getNombre()+"', "
                + "'"+this.getDomicilio_legal()+"', "
                + "'"+this.getNombre_fantasia()+"', "
                + "'"+this.getRepresentante_legal()+"', "
                + "'"+this.getPais_origen()+"', "
                + "'"+this.getRuc()+"', "
                + "'"+this.getTelefono()+"', "
                + "'"+this.getEmail()+"' "
                + ")\n" +
                " RETURNING proveedor ;";
        
        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "proveedor" );
        
        return intID;

    }
    
 

    
    
}
