/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.contrataciones.codigocontratacion;


import py.com.aplicacion.presupuesto.*;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class CodigoContratacionSQL {
    
    
    
    public String codigo ( Integer id  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CodigoContratacion");
        reader.fileExt = "codigo.sql";
        
        sql = reader.get( id );    
        
        return sql ;      
    }

    
    
    public String detalles ( Integer id  )
            throws Exception {
    
        String sql = "" +
            " SELECT id, codigo_contratacion, nivel, entidad, tp, pg, sp, py, obj, \n" +
            "  ff, of, dpt, monto, agno\n" +
            "  FROM contrataciones.certificacion_presupuestaria_detalle\n" +
            "  where codigo_contratacion = " + id +
            "  order by id;";                                 
        
        return sql ;      
    }
    
 
    
    
    
    public String lista ( String buscar)
            throws Exception {
    
        String sql = "";           
        
        if (buscar != null) 
        {
            buscar = buscar.replace(" ", "%") ;    
        }
                
        
        
        ReaderT reader = new ReaderT("CodigoContratacion");
        reader.fileExt = "lista.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;      
    }
    
    
    

    
    public String lista_filtro ( String buscar, Integer obj)
            throws Exception {
    
        String sql = "";           
        
        if (buscar != null) 
        {
            buscar = buscar.replace(" ", "%") ;    
        }
                
        
        
        ReaderT reader = new ReaderT("CodigoContratacion");
        reader.fileExt = "lista_filtro.sql";
        
        sql = reader.get( buscar , obj );    
        
        return sql ;      
    }
                
            
            
    public String proveedor ( Integer prov  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CodigoContratacion");
        reader.fileExt = "proveedor.sql";
        
        sql = reader.get( prov );    
        
        return sql ;      
    }
    
    
            
    
}
