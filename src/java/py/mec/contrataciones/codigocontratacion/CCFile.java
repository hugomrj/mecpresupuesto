/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.mec.contrataciones.codigocontratacion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import py.com.aplicacion.migracion.planfinanciero.PlanFinanciero;
//import static py.mec.contrataciones.codigocontratacion.CodigoContratacionMigracion.BUFFER_SIZE;
import py.mec.contrataciones.certificacionpresupuestariadetalle.CertificacionPresupuestariaDetalleDAO;
import py.mec.contrataciones.proveedor.Proveedor;
import py.mec.contrataciones.proveedor.ProveedorDAO;



public class CCFile {
    
    
    static final int BUFFER_SIZE = 1024;
    private int bloque = 0;
    

    public void crear(InputStream inputStream, String pathfile) 
            throws IOException {
        
          
        //File saveFile = new File(save_dir + nombre_archivo);
        File saveFile = new File(pathfile);
        saveFile.setWritable(true);
        saveFile.setReadable(true);         
         
        // opens an output stream for writing file
        FileOutputStream outputStream = new FileOutputStream(saveFile);
         
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = -1;
                
        
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        
        outputStream.close();
        inputStream.close();
         
        // sends response to client
        //response.getWriter().print("UPLOAD DONE");        
        
        
    }


    
    
    
    
    
     
    public Integer leer (  String pathfile ) {

        
        CodigoContratacionDAO codigocontratacionDAO = new CodigoContratacionDAO();        
        ProveedorDAO proveedorDao = new ProveedorDAO();        
        CertificacionPresupuestariaDetalleDAO detalleDAO = new CertificacionPresupuestariaDetalleDAO();
        
        String linea = "";
        String linea_anterior = "";        
        
        BufferedReader br = null;
        try {
            
           br = new BufferedReader(new InputStreamReader(new FileInputStream(pathfile),"UTF8"));
           
           String texto = br.readLine();
           
           while(texto != null)
           {               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() !=0 ){
                    
                    
                    linea = sSubCadena.toString().trim();  
                    
                    String control = linea.toString().trim();  

                    String patron = "";
                    patron = "1- Datos del Proveedor / Contratista Adjudicado";
                    if ( control.startsWith(patron) && bloque ==0 ) {
                        bloque = 1;
                    }
                    else
                    {
                        patron = "2- Datos del Procedimiento";
                        if ( control.startsWith(patron) && bloque == 1 ) {
                            bloque = 2;
                        }
                        else
                        {
                            patron = "3- Certificación Presupuestaria";
                            if ( control.startsWith(patron) && bloque == 2 ) {
                                bloque = 3;
                            }         
                        }
                    }    





                    switch (bloque) {
                        case 0:
                               
                            codigocontratacionDAO.gen(linea);                            
                            break;            
                            
                        case 1:
                                               
                            proveedorDao.gen(linea, linea_anterior,
                                    codigocontratacionDAO.getId());
                            break;
                            
                        case 2:
                            
                            codigocontratacionDAO.datosProcedimiento(
                                    linea, linea_anterior, 
                                    codigocontratacionDAO.getId());                            
  
                            break;
                            
                        case 3:
                            
                            detalleDAO.gen(linea, codigocontratacionDAO.getId() );
                            
                            break;
                        default:
                            break;
                    }                    
                    
                }                              
               texto = br.readLine();
               linea_anterior = linea;

           }
        }
        
                
        
        catch (FileNotFoundException e) {            
            System.out.println(e.getMessage());
        }
        catch(Exception e) {            
            System.out.println(e.getMessage());
        }
        
        
        finally 
        {
            try {
                if(br != null){
                    br.close();
                }
                return codigocontratacionDAO.getId() ;
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
                return null;
            }
        }
        
        
     
    }
    
    
    
    
    
    
/*

    public void leerLinea (String line ) throws IOException {

        String sobj = line.substring( 0, 1);

        String control = line.toString().trim();  
        
        
        String patron = "";
        patron = "1- Datos del Proveedor / Contratista Adjudicado";
        if ( control.startsWith(patron) && bloque ==0 ) {
            bloque = 1;
        }
        else
        {
            patron = "2- Datos del Procedimiento";
            if ( control.startsWith(patron) && bloque == 1 ) {
                bloque = 2;
            }
            else
            {
                patron = "3- Certificación Presupuestaria";
                if ( control.startsWith(patron) && bloque == 2 ) {
                    bloque = 3;
                }         
            }
        }    
    
        
   
    
        
        switch (bloque) {
            case 0:
                System.out.println(bloque);    
                break;            
            case 1:
                System.out.println(bloque);    
                break;
            case 2:
                System.out.println(bloque);    
                break;
            case 3:
                System.out.println(bloque);    
                break;
            default:
                break;
        }
    }    
    
    */
    

    
    
    
    
}
