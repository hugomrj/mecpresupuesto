/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.mec.contrataciones.codigocontratacion;

import py.com.aplicacion.migracion.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Webinf;




/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("codigoscontrataciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class CodigoContratacionWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    
    
                         
    public CodigoContratacionWS() {
        
    }

    
    

    
    @POST
    @Path("/proceso")
    public  Response proceso (  @HeaderParam("token") String strToken ) {
                            
        try {            
            
            Webinf webinf = new Webinf();
            webinf.setCarpeta("files");
            webinf.ini();
            
            String ruta = webinf.getPath();            
    
            Migracion migra = new Migracion();
            
            migra.setCarpeta(ruta);
            migra.setFicheros();

            persistencia.ejecutarSQL (" DELETE FROM migracion.ejecucion_migracion; ") ;            
            persistencia.ejecutarSQL (" DELETE FROM migracion.planfinanciero_migracion; ") ;        
            persistencia.ejecutarSQL (" DELETE FROM migracion.inicial_migracion; ") ;        
            persistencia.ejecutarSQL (" DELETE FROM migracion.prevcom_migracion; ") ;   
                        
            migra.recorrerArchivos();                
        
            return Response
                    .status(Response.Status.OK)
                    .entity("true")
                    .header("token", strToken )
                    .build();                        

        }                      
        
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

    

        


    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  

            if (autorizacion.verificar(strToken))
            //if ( true )
            {
                autorizacion.actualizar();              
                
                String json = "";    
                
                JsonObject jsonObject 
                        = new CodigoContratacionJSON().codigo(id);
                
                
                return Response
                        .status( this.status )
                        .entity( jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }

            
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }        
        
    }    
      
        
    
    
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,
            @MatrixParam("z") String z,
            @MatrixParam("q") String q,
            @MatrixParam("obj") Integer obj
            ) {
        
        
        if (q == null) { q = ""; }  
        if (obj == null) { obj = 0; }
        
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                JsonObject jsonObject;
                                
                if (z == null) {            
                    
                    jsonObject = new CodigoContratacionJSON().lista(page, q);
                }   
                else{
                    
                    jsonObject = new CodigoContratacionJSON().lista_filtro(page, q, obj);
                }

                
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
      
 
        
    

    
    
    
    
    @GET    
    @Path("/proveedor/{prov}")
    public Response proveedor ( 
            @HeaderParam("token") String strToken,            
            @PathParam("prov") Integer prov
            ) {
        
        
        //if (prov == null) { prov = 0; }
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                JsonObject jsonObject;
                    
                jsonObject = new CodigoContratacionJSON().proveedor(prov);
                    
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
      
 
        
        
    
    
}


