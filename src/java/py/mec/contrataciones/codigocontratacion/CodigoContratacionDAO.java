/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.contrataciones.codigocontratacion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.util.Datetime;
import py.mec.contrataciones.certificacionpresupuestariadetalle.CertificacionPresupuestariaDetalleDAO;



/**
 *
 * @author hugo
 */
public class CodigoContratacionDAO extends CodigoContratacion {
   
    public CodigoContratacion gen ( String line ) throws SQLException, Exception{

        
        String control = line.toString().trim();          
        String patron = "";

        patron = "C.C.:";
        if ( control.startsWith(patron) ) {
            
            
            String patronF = "Fecha de emisión";
            if (line.contains(patronF)){
                                
                line = line.replaceAll(patron, "").trim();                        
                line = line.replaceAll(patronF, "").trim();
                                
                String[] partes = line.split(":");      
                if (partes.length == 2){
                
                    this.setId(0);
                    this.setCc(partes[0].trim());     
                    this.setFecha_emision(                    
                        Datetime.castDate( partes[1].replaceAll("-", "/").trim() )                                
                    );    
                    
                    Integer intID = this.insertCab();
                    this.setId(intID);
                    
                }
                
            }
            else{
                line = line.replaceAll(patron, "").trim();                        
                this.setId(0);
                this.setCc(line);                
            }

            

            
        }        
        

        patron = "Fecha de emisión:";
        if ( control.startsWith(patron) ) {
            
            line = line.replaceAll(patron, "").trim();
            line = line.replaceAll("-", "/").trim();
                        
            this.setFecha_emision(                    
                    Datetime.castDate( line )                                
            );            
            
            
            Integer intID = this.insertCab();
            this.setId(intID);
            
        }          
        
        return this;
    }
    
    
    
    
    
    public Integer insertCab ( ) throws Exception {
    
        //  buscar si existe registro de cc                        
        Integer ccid = this.existe(this.getCc());            
        if (ccid != 0 ) {                
            // borrar certtificaciones relacionadas
            new CertificacionPresupuestariaDetalleDAO()
                    .borarCertificacionDetalle(ccid);

            this.delete(this.getCc());                
        }                        
        Integer intID = this.add();

        return intID;
    }
    
    
    
    
    
    
    public Integer add ( ) throws SQLException{

        
        
        Integer intID = 0;
        
        String strSQL ="INSERT INTO contrataciones.codigos_contrataciones (\n" +
                    " cc, fecha_emision)\n" +
                    " VALUES ( '"
                + this.getCc() + "', '"
                + Datetime.toSQLDate(this.getFecha_emision())+ "' )  RETURNING id ;";        

        
        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );
        
        return intID;

    }


    
    
    public Integer existe(String cc) throws Exception {      
        
        Integer ret = 0;
        
        Conexion conexion = new Conexion();
        conexion.conectar();            

        
        Statement  statement ;
        ResultSet resultset;
        
                
        String sql = 
                    " SELECT id\n" +
                    " FROM contrataciones.codigos_contrataciones\n" +
                    " WHERE cc like '" + cc + "' " ;
        
          
        statement = conexion.getConexion().createStatement();                           
        resultset = statement.executeQuery( sql );     


        if(resultset.next()) {                
            ret =  Integer.parseInt(resultset.getString("id")); 
        }

        resultset.close();
        conexion.desconectar(); 
  
        return ret;          

      }
     
        
        


    public boolean  delete (  String cc ) throws SQLException{
                
        Persistencia persinstencia = new Persistencia();

        String strSQL =
                " DELETE FROM contrataciones.codigos_contrataciones\n" +
                " WHERE cc like '"+cc+"'";
        
        boolean bool = persinstencia.ejecutarSQL( strSQL );
        
        return bool;

    }
        
        
    
    

    public CodigoContratacion datosProcedimiento ( String linea, String linea_anterior,
            Integer id) 
            throws SQLException, Exception{

          
        String control = linea.toString().trim();  
        
        String patron = "";
        
        patron = "Nivel de Entidad:";
        if ( control.startsWith(patron) ) {
            linea = linea.replaceAll(patron, "").trim();            
            this.setNivel_entidad(linea);                  
        }        
        
        
        patron = "Entidad:";
        if ( control.startsWith(patron) ) {
            linea = linea.replaceAll(patron, "").trim();            
            this.setEntidad(linea);                  
        }        


        patron = "Unidad Compradora:";
        if ( control.startsWith(patron) ) {
            linea = linea.replaceAll(patron, "").trim();            
            this.setUnidad_compradora(linea);                  
        }        

        
        patron = "Tipo de Procedimiento:";
        if ( control.startsWith(patron) ) {
            linea = linea.replaceAll(patron, "").trim();            
            this.setTipo_procedimiento(linea);                  
        }        

        
        
        
        
        patron = "Descripción del Bien, Obra o Servicio:";
        if ( control.startsWith(patron) ) {
            linea = linea.replaceAll(patron, "").trim();            
            this.setDescripcion(linea);                                                        
        }       
        else {       
            
            if ( linea_anterior.trim().startsWith(patron) ) {
                
                if (!(linea.trim().startsWith("ID:")
                    || linea.trim().startsWith("Nro Pac RL:")
                    || linea.trim().startsWith("Número de PAC:")                            
                    ))
                {                                             
                    this.setDescripcion( this.getDescripcion() + " " + linea);   
                }                                         
            }      
            
            
        
        }
        
        
        
        
        
        patron = "Número de PAC:";
        if ( control.startsWith(patron) ) {
            linea = linea.replaceAll(patron, "").trim();            
            this.setNumero_pac(linea);                  
        }           
        patron = "ID:";
        if ( control.startsWith(patron) ) {
            linea = linea.replaceAll(patron, "").trim();            
            this.setNumero_pac(linea);                              
        }           


        
        patron = "Tipo de Contrato:";
        if ( control.startsWith(patron) ) {
            //linea = linea.replaceAll(patron, "").trim();            
            //this.setDescripcion(linea);                  
            
            linea = linea.replaceAll("Tipo de Contrato:", "");                                    
            linea = linea.replaceAll("Número", "");                                    
            linea = linea.replaceAll("Fecha", "");  
            linea = linea.replaceAll("vigencia desde", "");  
            
            String[] partes = linea.split(":");       
            
            if (partes.length == 3) {                
                this.setTipo_contrato(partes[0].trim());
                this.setNumero(partes[1].trim());
                
                //12-09-2018
                if (partes[2].trim() != ""){
                    partes[2] = partes[2].replaceAll("-", "/").trim();
                    
                    this.setFecha(                    
                        Datetime.castDate( partes[2] )                                
                    );                
                }
            }  
            
        }      
        
        
        
     
        patron = "Monto Total de la Renovación:";
        if ( control.startsWith(patron) ) {
            
            linea = linea.replaceAll(patron, "").trim(); 
            
            linea = linea.replaceAll("En Letras", "").trim(); 
            String[] partes = linea.split(":");    
            partes[0] = partes[0].replaceAll(",", "").trim(); 
                        
            this.setMonto_total( Long.parseLong(partes[0]));        
            
        }
        else{
                
            control = linea_anterior.toString().trim();   
            patron = "Monto Total";

            if ( control.startsWith(patron) && !(control.contains("Renovación")) ) {
            //if ( control.startsWith(patron) ) {
                
                linea = linea.replaceAll("En Letras", "").trim(); 
                String[] partes = linea.split(":");    
                partes[0] = partes[0].replaceAll(",", "").trim(); 
                this.setMonto_total( Long.parseLong(partes[0]));                  

                
                
            }
        }
        
        
        control = linea.toString().trim();   
        patron = "Moneda:";
        if ( control.startsWith(patron) ) {
            
            linea = linea.replaceAll(patron, "").trim();            
            this.setMoneda(linea);   
            
            this.updateDatosProcedimiento ( id );
             
        }             
            
        return this;
    }
    
    
    
    
    public Integer updateDatosProcedimiento ( Integer intID ) throws SQLException{


        
        String strSQL =
                " UPDATE contrataciones.codigos_contrataciones\n" +
                " SET\n" +
                " nivel_entidad='"+this.getNivel_entidad()+"', \n" +
                " entidad='"+this.getEntidad()+"', \n" +
                " unidad_compradora='"+this.getUnidad_compradora()+"', \n" +
                " tipo_procedimiento='"+this.getTipo_procedimiento()+"', \n" +
                " descripcion='"+this.getDescripcion()+"', \n" +
                " numero_pac='"+this.getNumero_pac()+"', \n" +
                " tipo_contrato='"+this.getTipo_contrato()+"', \n" +
                " numero='"+this.getNumero()+"', \n" +
                " fecha='"+Datetime.toSQLDate(this.getFecha())+"', \n" +
                " monto_total="+this.getMonto_total().toString()+", \n" +
                " moneda='"+this.getMoneda()+"'\n" +
                " WHERE id = " + intID +
                " returning id;";

           
        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );
        
        return intID;

    }


    
    public Integer updateProveedor ( Integer intID, Integer proveedor ) throws SQLException{

        
        String strSQL =
                " UPDATE contrataciones.codigos_contrataciones\n" +
                " SET\n" +
                " proveedor =" + proveedor + " \n" +
                " WHERE id = " + intID +
                " returning id;";
               

        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );
        
        return intID;

    }


    
    
    
    
    public List<CodigoContratacion>  lista ( Integer page, ResultSet resultSet ) {
                
        List<CodigoContratacion>  lista = null;        
    
    
System.out.println("List<CodigoContratacion>");
        
        try {                        
        
            lista = new Coleccion<CodigoContratacion>().resultsetToList(
                        new CodigoContratacion(), 
                        resultSet
                    );                            
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

        
    
    
    
}
