package py.mec.contrataciones.codigocontratacion;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Enumeration;
import nebuleuse.util.Recurso;


//import org.apache.log4j.Logger;

@WebServlet(
        name = "CodigoContratacionMigracion", 
        urlPatterns = {"/Codigo/Contratacion/Migracion"}
)

@MultipartConfig(
  fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
  maxFileSize = 1024 * 1024 * 10,      // 10 MB
  maxRequestSize = 1024 * 1024 * 100   // 100 MB
)



public class CodigoContratacionMigracion extends HttpServlet {
    
    //static final int BUFFER_SIZE = 1024;
    
     
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
         

       
        
        try {

            Recurso path = new Recurso();
            path.pathWebInf("files");
            String nombre_archivo = "";      
            String save_dir = path.path;            
         
            long now = System.currentTimeMillis();
            nombre_archivo = String.valueOf(now);
            String pathfile = path.path + nombre_archivo + ".txt" ;            
            
            CCFile ccfile = new CCFile();
            InputStream inputStream = request.getInputStream();

            ccfile.crear(inputStream, pathfile);

            Integer retcod = ccfile.leer(pathfile);        

           

            //response.getWriter().print(retcod);
            
            
            PrintWriter out = response.getWriter();
            out.println(retcod);


            
        }         
        catch (Exception ex) {          
            System.out.println(ex.getMessage());
        }
        
        

        
        // Gets file name for HTTP header
        //String fileName = request.getHeader("fileName");

        /*
        System.out.println("===== Begin headers =====");
        Enumeration<String> names = request.getHeaderNames();
        while (names.hasMoreElements()) {
            String headerName = names.nextElement();
            //System.out.println(headerName + " = " + request.getHeader(headerName));                    
            if (headerName.equals("nombre_archivo")){
                nombre_archivo = request.getHeader(headerName);
            }            
        }
        System.out.println("===== End headers =====\n");
        */
        
        /*  
        //File saveFile = new File(save_dir + nombre_archivo);
        File saveFile = new File(pathfile);
        saveFile.setWritable(true);
        saveFile.setReadable(true);         
        
               
        // opens input stream of the request for reading data
        InputStream inputStream = request.getInputStream();
         
        // opens an output stream for writing file
        FileOutputStream outputStream = new FileOutputStream(saveFile);
         
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = -1;
        System.out.println("Receiving data...");
         
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
         
        System.out.println("Data received.");
        outputStream.close();
        inputStream.close();
         
        // sends response to client
        response.getWriter().print("UPLOAD DONE");


        */
    }
}
