/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.contrataciones.codigocontratacion;

import java.util.Date;

/**
 *
 * @author hugo
 */
public class CodigoContratacion {
   
    private Integer id;
    private String cc;
    private Date fecha_emision;
    
    private String nivel_entidad;
    private String entidad;    
    private String unidad_compradora;
    private String tipo_procedimiento;
    private String descripcion;
    private String numero_pac;
    private String tipo_contrato;
    private String numero;
    private Date fecha;
    private Long monto_total;
    private String moneda;
    private Integer proveedor;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public Date getFecha_emision() {
        return fecha_emision;
    }

    public void setFecha_emision(Date fecha_emision) {
        this.fecha_emision = fecha_emision;
    }

    public String getUnidad_compradora() {
        return unidad_compradora;
    }

    public void setUnidad_compradora(String unidad_compradora) {
        this.unidad_compradora = unidad_compradora;
    }

    public String getTipo_procedimiento() {
        return tipo_procedimiento;
    }

    public void setTipo_procedimiento(String tipo_procedimiento) {
        this.tipo_procedimiento = tipo_procedimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNumero_pac() {
        return numero_pac;
    }

    public void setNumero_pac(String numero_pac) {
        this.numero_pac = numero_pac;
    }

    public String getTipo_contrato() {
        return tipo_contrato;
    }

    public void setTipo_contrato(String tipo_contrato) {
        this.tipo_contrato = tipo_contrato;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(Long monto_total) {
        this.monto_total = monto_total;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getNivel_entidad() {
        return nivel_entidad;
    }

    public void setNivel_entidad(String nivel_entidad) {
        this.nivel_entidad = nivel_entidad;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Integer getProveedor() {
        return proveedor;
    }

    public void setProveedor(Integer proveedor) {
        this.proveedor = proveedor;
    }
    
}
