/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mec.contrataciones.codigocontratacion;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class CodigoContratacionJSON  {


    
    
    public CodigoContratacionJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  codigo ( Integer id) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
            
            ResultadoSet resSet = new ResultadoSet();          
            CodigoContratacionSQL codigoSQL = new CodigoContratacionSQL();          
            String sql = "";            
            
            sql = codigoSQL.codigo(id);
            ResultSet rsData = resSet.resultset(sql);
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            
            
            // detalles
            sql = codigoSQL.detalles(id);            
            ResultSet rsDet = resSet.resultset(sql);
            //rsData = resSet.resultset(sql);
            JsonArray jsonDetalles = new JsonArray();            
            jsonDetalles = new JsonObjeto().array_datos(rsDet);
            
    
            
            // union de partes
            //jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            jsonObject.add("detalles", jsonDetalles);            
            
            
            resSet.close();
        }         
        catch (Exception ex) {                                    
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    

    public JsonObject  lista ( Integer page, String buscar) {
        
        Map<String, String> map = null;        
        RegistroMap registoMap = new RegistroMap();                     
        
        
        JsonObject jsonObject = new JsonObject();        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
  
    
            String sql = "";      
            
            CodigoContratacionSQL objSQL = new CodigoContratacionSQL();               
            sql = objSQL.lista(buscar);            
            ResultSet rsData = new ResultadoSet().resultset(sql, page);  

            
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            while(rsData.next()) 
            {   
                map = registoMap.convertirHashMap(rsData);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonArrayDatos.add( element );
                
            }               
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
    
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    

     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
       
    
        

    public JsonObject  lista_filtro( Integer page, String buscar, Integer obj)  {
        
        Map<String, String> map = null;        
        RegistroMap registoMap = new RegistroMap();             
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
    
            String sql = "";      
            
            CodigoContratacionSQL objSQL = new CodigoContratacionSQL();               
            sql = objSQL.lista_filtro(buscar, obj); 
            

System.out.println(sql);
            
            ResultSet rsData = new ResultadoSet().resultset(sql, page);  
            
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            while(rsData.next()) 
            {   
                map = registoMap.convertirHashMap(rsData);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonArrayDatos.add( element );
                
            }               
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
    
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
            
            
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        
    
        
    
    
    
        
    
    

    public JsonObject  proveedor( Integer prov)  {
        
        Map<String, String> map = null;        
        RegistroMap registoMap = new RegistroMap();             
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
    
            String sql = "";      
            
            CodigoContratacionSQL objSQL = new CodigoContratacionSQL(); 
            
            sql = objSQL.proveedor(prov);
            
            ResultSet rsData = new ResultadoSet().resultset(sql);  
            
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            while(rsData.next()) 
            {   
                map = registoMap.convertirHashMap(rsData);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonArrayDatos.add( element );                
            }               
            
            /*
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            */
    
            
            // union de partes
            // jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
            
            
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        
        
    
    
        
}
