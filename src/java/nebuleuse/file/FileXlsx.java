/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.file;

import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
*/

import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook;






/**
 *
 * @author hugo
 */
public class FileXlsx extends FileBin {


    
    
    private XSSFWorkbook libro;
    private XSSFSheet hoja;
    private Row fila;
    private ArrayList <String> cabecera =  new ArrayList<>();
    private ArrayList <String> campos =  new ArrayList<>();
    
    
        
    public  void gen ( ResultSet resulset ) throws SQLException, Exception {
        
           
            this.newlibro();
            //this.newhoja("hoja1");


            this.writeCabecera(0);
            this.writeContenido(resulset);
            
        
        // Se salva el libro.
        try {

                String ruta = "";
                ruta = this.getFilePath();

                
                FileOutputStream file = new FileOutputStream( ruta );                
                
                this.getLibro().write(file);

                file.close();                
            
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
    }
    
    
    
    
    
    public  void newlibro () {        
        this.setLibro(new XSSFWorkbook());        
    }    
    

    public  void newhoja (String nombrehoja) {        
        this.hoja = this.getLibro().createSheet(nombrehoja);
    }    
    
    
    public  void newfila (Integer indice) {        
        this.setFila(this.hoja.createRow(indice));
    }        
    
    
    
    
    public  void writeCabecera (Integer indice) {        
        
        this.newfila(0);

        int i = 0;
        for (String titulo : cabecera) {     
            
            
            Cell cell = this.getFila().createCell(i);
            cell.setCellValue(titulo);
            
            //this.getFila().createCell(i).setCellValue( titulo );   
            
            
            
            i++;
        }
    }            

    
    
    
    
    
    public  void writeContenido ( ResultSet resulset ) throws SQLException {        

        Integer nrofila = 1;        

        while (resulset.next()) {            
            //String em = resulset.getString("EM_ID");
            int i = 0;
            
            this.newfila(nrofila);
            
            for (String campo : this.campos) {   
                
                String em = "";                

                if (resulset.getString(campo) != null){
                    em = resulset.getString(campo);                    
                    String cad = em;
                    
                    
                    try {
                        Long num = Long.parseLong(cad);
                        this.getFila().createCell(i).setCellValue(Long.parseLong(cad));
                    }
                    catch (NumberFormatException nfe) {
                        this.getFila().createCell(i).setCellValue( em );
                    }                    
                    
         /*           
                    //if (cad.matches("[0-9]*")){
                    if ( Long.parseLong(cad) ){
                        
                    }
                    else{
                        
                    }  
*/
                    
                    
                }

                

                
    //            String em = resulset.getString(campo);                

//System.out.println(em);


          
                
                i++;
            }            
            nrofila++;
        }        
                 

    }            

    
    

    public  void gen (Integer indice) {        
        
        this.newfila(0);

        int i = 0;
        for (String titulo : cabecera) {        
            this.getFila().createCell(i).setCellValue( titulo );
            i++;
        }
    }            
    
    
    
    
    
    
    
    
    public ArrayList <String> getCabecera() {
        return cabecera;
    }

    public void setCabecera(ArrayList <String> cabecera) {
        this.cabecera = cabecera;
        
    }

    public ArrayList <String> getCampos() {
        return campos;
    }

    public void setCampos(ArrayList <String> campos) {
        this.campos = campos;
    }

    public XSSFWorkbook getLibro() {
        return libro;
    }

    public void setLibro(XSSFWorkbook libro) {
        this.libro = libro;
    }
    
    public Row getFila() {
        return fila;
    }

    public void setFila(Row fila) {
        this.fila = fila;
    }    
    
    public XSSFSheet getHoja() {
        return hoja;
    }

    

    

    
    public  void formato () {        
        
        //this.newfila(0);

        int i = 0;
        for (String titulo : cabecera) {     
            
            Cell cell = this.hoja.getRow(0).getCell(i);
            
            // formato 
            
            //System.out.println(this.hoja.getRow(0).getCell(i+1).getCellType());
            
            XSSFCellStyle greenCellStyle = this.hoja.getWorkbook().createCellStyle();
            java.awt.Color green = new java.awt.Color(220, 220, 220);
            greenCellStyle.setFillForegroundColor(new XSSFColor(green, new DefaultIndexedColorMap()));
            greenCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
            cell.setCellStyle(greenCellStyle);
            
            
            i++;
        }
        
        
        int y = 1;
        
        while (y > 0) {
        
            
            try {      
                
                Cell cell0 = this.hoja.getRow(y).getCell(0);
                
                Row row = cell0.getRow();
                //System.out.println(row.getRowNum());
                                
                
                XSSFCellStyle MistyRoseCellStyle = this.hoja.getWorkbook().createCellStyle();
                java.awt.Color MistyRose = new java.awt.Color(255, 228, 225);
                MistyRoseCellStyle.setFillForegroundColor(new XSSFColor(MistyRose, new DefaultIndexedColorMap()));
                MistyRoseCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
                
                
                Cell cell = row.getCell(39);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(40);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(41);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(42);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                cell = row.getCell(43);
                cell.setCellStyle(MistyRoseCellStyle);                
                
                
                XSSFCellStyle BlueWebCellStyle = this.hoja.getWorkbook().createCellStyle();
                java.awt.Color BlueWeb = new java.awt.Color(206, 231, 255);
                BlueWebCellStyle.setFillForegroundColor(new XSSFColor(BlueWeb, new DefaultIndexedColorMap()));
                BlueWebCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                   
                
                cell = row.getCell(44);
                cell.setCellStyle(BlueWebCellStyle);                
                
                
                
            }
            catch (Exception ex)
            {
                //System.out.println(ex.getMessage() );                
                y = -1;
                break;
                
            }  
            
            y++;
        }
        
        
        
        
        
        
    }            


    
    
}        