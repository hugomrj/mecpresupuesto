

   
function form_inicio(){    


        ajax.url = html.url.absolute()+'/aplicacion/consultalinea/htmf/cab.html'; 
        ajax.metodo = "GET";            
        document.getElementById( "cab_form" ).innerHTML =  ajax.public.html();       
        
        form_cab_limpiar();
        
        form_cab_accion();
        
        form_det_html();
        
     
    
}
        
    
    
function form_cab_limpiar(){  
    
    
    //cab_clase    
    var form_campos = ["cab_clase", "cab_programa", "cab_actividad", 
        "cab_obj", "cab_ff", "cab_of", "cab_dpto"];

    form_campos.forEach(function (elemento, indice, array) {    
        document.getElementById( elemento ).value = "0"; 
        //console.log(elemento, indice);
    });
    document.getElementById( "obj_descripcion" ).innerHTML = "";

}




function form_cab_accion(){  
    
    
    //cab_clase    
    var form_campos = ["cab_clase", "cab_programa", "cab_actividad", 
        "cab_obj", "cab_ff", "cab_of", "cab_dpto"];


    form_campos.forEach(function (elemento, indice, array) {            
        document.getElementById( elemento ).addEventListener( 'blur',
            function(event) {   
                //console.log(this.value);
                this.value = fmtNum(this.value); 
            },
            false
        );               
    });
  
    
    
        var btn_cab_buscar = document.getElementById('btn_cab_buscar');         
        btn_cab_buscar.onclick = function(  )
        {
            form_det_data_WS();
        }

}




   
   
function form_det_data_WS(){
    
        var data = 0;

        var apipath = "/presupuesto/linea;n1="+document.getElementById( "cab_clase" ).value
                +";n2="+document.getElementById( "cab_programa" ).value
                +";n3="+document.getElementById( "cab_actividad" ).value
                +";obj="+document.getElementById( "cab_obj" ).value
                +";ff="+document.getElementById( "cab_ff" ).value
                +";of="+document.getElementById( "cab_of" ).value
                +";dpto="+document.getElementById( "cab_dpto" ).value
        
        loader.inicio();
        xhr =  new XMLHttpRequest();
        //var url = html.url.absolute()+"/api/ordenescompras/"+ordencompra_ordencompra.value;           
        var url = html.url.absolute()+"/api"+apipath;           
        var metodo = "GET";        
        var type = "application/json";
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if ( xhr.readyState == 4 ){
                loader.fin();

                ajax.local.token = xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token"));     
                //sessionStorage.setItem('total_registros',  xhr.getResponseHeader("total_registros"));

                switch ( xhr.status ) {
                    case 200:


//console.log(xhr.responseText);

                        form_datos_UI ( xhr.responseText);


                        break;

                    case 204:                            

                        form_det_limpiar_UI();
                        msg.error.mostrar( "No encontrado" );         
                        break;
                        
                    case 401:                            
                        window.location = html.url.absolute();                                     
                        break;                        

                    default:
                        msg.error.mostrar( xhr.status );
                        console.log(xhr.responseText );
                }       
            }
        };
        xhr.onerror = function (e) {  
            msg.error.mostrar("error ");
        };         
        xhr.setRequestHeader('Content-Type', type);   

        xhr.setRequestHeader("token", localStorage.getItem('token'));
        xhr.send( data );    
       
    
}   
   
   
  
  
    
function form_det_html(){  
    
    ajax.url = html.url.absolute()+'/aplicacion/consultalinea/htmf/det.html'; 
    ajax.metodo = "GET";            
    document.getElementById( "det_form" ).innerHTML =  ajax.public.html();           
    

}






function form_datos_UI ( json ){

        form.name = "form_ordencompra";
        form.json = json;           
        //form.llenar();         
  
        var ojson = JSON.parse(json) ; 
        var i = 0 ; 
    
        document.getElementById( "obj_descripcion"  ).value = ojson[i]["obj_decripcion"]; 
    
    
    
    var formdet_campos = ["inicial", "modifi", "vigente", "platotal", "ejetotal",
    "planfinanciero", "obligado", "saldo", "saldo_plan",
        "pagado", "pendiente", "prevision", "compromiso" ];

    formdet_campos.forEach(function (elemento, indice, array) {    
    
        document.getElementById( "det_" + elemento ).value = ojson[i][elemento]; 
        document.getElementById( "det_" + elemento ).value
            = fmtNum(document.getElementById( "det_" + elemento ).value); 
        //console.log(elemento, indice);
    });
    
    
    
    // tabla
    var formdet_campos = [
        "pla1", "pla2", "pla3", "pla4", "pla5","pla6",
        "pla7", "pla8", "pla9", "pla10", "pla11","pla12",
        "eje1", "eje2", "eje3", "eje4", "eje5","eje6",
        "eje7", "eje8", "eje9", "eje10", "eje11","eje12"
    ];

    formdet_campos.forEach(function (elemento, indice, array) {    
         
        document.getElementById( elemento ).innerHTML = ojson[i][elemento]; 
        document.getElementById( elemento ).innerHTML
            = fmtNum(document.getElementById( elemento ).innerHTML); 
        //console.log(elemento, indice);
    });
    
    
        
        
};



function form_det_limpiar_UI(){  
    

    
    document.getElementById( "obj_descripcion"  ).value = ""; 
    
    
    
    var formdet_campos = ["inicial", "modifi", "vigente", "platotal", "ejetotal",
    "planfinanciero", "obligado", "saldo", "saldo_plan",
        "pagado", "pendiente", "prevision", "compromiso" ];

    formdet_campos.forEach(function (elemento, indice, array) {    
    
        document.getElementById( "det_" + elemento ).value
            = fmtNum("0"); 
        //console.log(elemento, indice);
    });
    
    
    
    // tabla
    var formdet_campos = [
        "pla1", "pla2", "pla3", "pla4", "pla5","pla6",
        "pla7", "pla8", "pla9", "pla10", "pla11","pla12",
        "eje1", "eje2", "eje3", "eje4", "eje5","eje6",
        "eje7", "eje8", "eje9", "eje10", "eje11","eje12"
    ];

    formdet_campos.forEach(function (elemento, indice, array) {    

        document.getElementById( elemento ).innerHTML
            = fmtNum("0"); 
        //console.log(elemento, indice);
    });
    
    
    
    
};





