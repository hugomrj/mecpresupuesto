

function CajaChica(){
    
   this.tipo = "cajachica";   
   this.recurso = "cajachica";   
   this.value = 0;
   
   this.json_descrip = "nombre";
   
   this.dom="";
   
   this.carpeta=  "/certificaciones";   
   
   
   this.titulosin = ""
   this.tituloplu = ""   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['fecha', 'dependencia.nombre', 'nombre_empresa',
                'saldo_disponible', 'monto_requerido', 'monto_restante'];
   
   this.etiquetas =  [''];                                  
    
   this.tablaformat = ['D', 'C', 'C', 'N', 'N','N' ];                                  
   
   this.tbody_id = "objeto-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "objeto-acciones";   
         
   this.parent = null;
   
   
}



CajaChica.prototype.new = function( obj , mes, 
                            clase, programa, actividad, objeto, ff, of, dpto,
                            saldo_disponible ) {                
    

    var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html' 

    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {
      
        document.getElementById('arti_form').innerHTML = data ;
              
              
        var cajachica_mes = document.getElementById('cajachica_mes');
        cajachica_mes.value = mes;
        cajachica_mes.disabled = true;
        
        
        document.getElementById('cajachica_clase').value = clase;
        document.getElementById('cajachica_programa').value = programa;
        document.getElementById('cajachica_actividad').value = actividad;
        document.getElementById('cajachica_obj').value = objeto;
        document.getElementById('cajachica_ff').value = ff;
        document.getElementById('cajachica_of').value = of;
        document.getElementById('cajachica_dpto').value = dpto;
        
        document.getElementById('cajachica_saldo_disponible').value = saldo_disponible;
        
        
        obj.form_boolean();                
        obj.form_ini_abm(obj);        
        obj.form_ini_abm_accion(obj);   
        
        document.getElementById('linea_obligado').style.display = 'none';


    
        //  botones de alta        
        reflex.acciones.button_add_promise(obj);       

        boton.objeto = "" + obj.tipo;        
        boton.ini(obj);
        document.getElementById( obj.tipo +'-acciones' ).innerHTML 
                    =  boton.basicform.get_botton_add();    





        var btn_objeto_guardar = document.getElementById('btn_cajachica_guardar');
        btn_objeto_guardar.addEventListener('click',
            function(event) {               


                if ( obj.form_validar()) {
                    
                    loader.inicio();

                    form.name = "form_"+obj.tipo;

                    var xhr =  new XMLHttpRequest();     
                    var url = arasa.html.url.absolute()  +"/api/"+ obj.recurso;   

                    var metodo = "POST";        
                    ajax.json = form.datos.getjson() ;
                   
console.log(form.datos.getjson());                   

                    xhr.open( metodo.toUpperCase(),   url,  true );      

                    xhr.onreadystatechange = function () {
                        if (this.readyState == 4 ){

                            //ajax.headers.get();
                            ajax.local.token =  xhr.getResponseHeader("token") ;            
                            localStorage.setItem('token', xhr.getResponseHeader("token") );     
                            //ajax.state = xhr.status;




                            switch (xhr.status) {

                              case 200:

                                    msg.ok.mostrar("registro agregado");          
                                    reflex.data  = xhr.responseText;
                                                                                        
                                    //reflex.form_id_promise( obj, reflex.getJSONdataID( obj.campoid ) ) ;  
                                    
                                    
                                    //alert(reflex.getJSONdataID( obj.campoid ) );
                                    
                                    obj.form_id_promise( obj, reflex.getJSONdataID( obj.campoid ) );   
                                    
                                    break; 


                              case 401:

                                    window.location =  html.url.absolute();         
                                    break;                             


                              case 500:

                                    msg.error.mostrar(  reflex.data  );          
                                    break; 


                              case 502:                              
                                    msg.error.mostrar(  reflex.data  );          
                                    break;                                 


                              default: 
                                msg.error.mostrar("error de acceso");           
                            }                        

                            //arasa.html.url.redirect( xhr.status );                            
                            loader.fin();


                        }
                    };
                    xhr.onerror = function (e) {                    
                        reject( xhr );                 
                    };  


                    var type = "application/json";
                    xhr.setRequestHeader('Content-Type', type);   
                    //ajax.headers.set();
                    xhr.setRequestHeader("token", localStorage.getItem('token'));           

                    xhr.send( ajax.json );      


                }

            },
            false
        )




        var btn_objeto_cancelar = document.getElementById('btn_cajachica_cancelar');
        btn_objeto_cancelar.addEventListener('click',
            function(event) {                     
                alert("cancelar")
            },
            false
        );    

              
              
            
      })







    // reflex.form_new( obj );         
      
    /*
    reflex.acciones.button_add_promise(obj);          
    
    var certificacionplanfin_certificacion_tipo 
            = document.getElementById('certificacionplanfin_certificacion_tipo');          
    */
   
   
//    certificacionplanfin_certificacion_tipo.value = tipo_certi;



    
};








CajaChica.prototype.form_ini = function(obj) {    
    
    
    var mes = new Mes(); 
    mes.combobox("mes");    
    
    
    var btn_buscare = document.getElementById('btn_buscare') ;
    btn_buscare.onclick = function(){                                  

        var obj = new CertificacionPlanfin();                 

        obj.acctionresul = function(id) {    
            
            // pasara a una funcion
            var obj2 = new CajaChica();      
            obj2.cabecera_detalle(obj2, id);
                
            
        };            
        modal.ancho = 700;
        
        obj.url = html.url.absolute() + obj.carpeta +'/'+ 
                    obj.tipo + '/htmf/lista_busqueda.html'; 
        
        var va = 2;
        obj.get_api = html.url.absolute()+'/api/' + obj.recurso + '/lista/' + va;   
        
        busqueda.modal.custom(obj);        
        document.getElementById( obj.tipo + '_paginacion' ).innerHTML = "";   
        

        var saldo_disponible = document.getElementById('saldo_disponible');            
         saldo_disponible.onblur  = function() {                  
             saldo_disponible.value = fmtNum(saldo_disponible.value);      
        };      
        saldo_disponible.onblur();           
        
    };       
    
    
    
    var combomes = document.getElementById('mes');    
    combomes.onchange = function(){

        // pasara a una funcion
        var obj2 = new CajaChica();      
        obj2.cabecera_detalle(obj2, document.getElementById('id_lineae').value);

        
    };
    
    
    




    // buscar
    var busquedat = document.getElementById('busquedat') ;  
    busquedat.onkeypress = function(event) {                
        
        if (event.keyCode == 13) {                
            
            // funcion filtrar  certificaciones
            var instance = new CajaChica(); 
            var buscar = document.getElementById("busquedat" ).value;
            instance.lista_estructura(instance, 1, buscar);

        };                
        
    };      
    
    
       
    
    
    obj.detalles_html(obj);
    
    
    
};






CajaChica.prototype.cabecera_detalle = function(obj, id) {
         
            
    var url = html.url.absolute()+'/api/' + 'certificacionplanfin' + '/'+id;   
    ajax.async.json( "get", url, null )
        .then(( xhr ) => {

            if (xhr.status == 200)
            {     

                var ojson = JSON.parse( xhr.responseText ) ;      

                document.getElementById('id_lineae').value =  id;
    
                document.getElementById('eclase').innerHTML =  ojson["clase"] ;
                document.getElementById('eprograma').innerHTML =  ojson["programa"] ;
                document.getElementById('eactividad').innerHTML =  ojson["actividad"] ;
                document.getElementById('eobj').innerHTML =  ojson["obj"] ;
                document.getElementById('eff').innerHTML =  ojson["ff"] ;
                document.getElementById('eof').innerHTML =  ojson["of"] ;
                document.getElementById('edpto').innerHTML =  ojson["dpto"] ;


                var combomes = document.getElementById('mes');  
                var nombremes = combomes.options[combomes.selectedIndex].text;
                nombremes = nombremes.toLowerCase() + "_saldo";
                document.getElementById('saldo_disponible').value =  ojson[nombremes] ;

                document.getElementById('saldo_disponible').onblur();            



                // funcion filtrar  certificaciones
                var instance = new CajaChica(); 
                var buscar = document.getElementById("busquedat" ).value;
                instance.lista_estructura(instance, 1, buscar);


            }
            else{
            }   
        })      






}




CajaChica.prototype.form_ini_abm = function(obj) {    
    
        
    var cajachica_saldo_disponible = document.getElementById('cajachica_saldo_disponible');            
     cajachica_saldo_disponible.onblur  = function() {                  
         cajachica_saldo_disponible.value = fmtNum(cajachica_saldo_disponible.value);      
    };      
    cajachica_saldo_disponible.disabled = true;
    cajachica_saldo_disponible.onblur();          
    
    
    var cajachica_monto_restante = document.getElementById('cajachica_monto_restante');            
     cajachica_monto_restante.onblur  = function() {                  
         cajachica_monto_restante.value = fmtNum(cajachica_monto_restante.value);      
    };      
    cajachica_monto_restante.disabled = true;
    cajachica_monto_restante.onblur();          
        
    
    
    var cajachica_monto_requerido = document.getElementById('cajachica_monto_requerido');            
     cajachica_monto_requerido.onblur  = function() {                  
         cajachica_monto_requerido.value = fmtNum(cajachica_monto_requerido.value);    
         
         cajachica_monto_restante.value = 
                parseInt(NumQP(cajachica_saldo_disponible.value))
                - parseInt(NumQP(cajachica_monto_requerido.value)) ;
        cajachica_monto_restante.onblur();  
         
    };          
    cajachica_monto_requerido.onblur();          
    


    var cajachica_dependencia = document.getElementById('cajachica_dependencia');            
     cajachica_dependencia.onblur  = function() {          
         cajachica_dependencia.value = fmtNum(cajachica_dependencia.value);      
         cajachica_dependencia.value = NumQP(cajachica_dependencia.value);               
    };      
    cajachica_dependencia.onblur();          
    
    
    
    var cajachica_monto_obligado = document.getElementById('cajachica_monto_obligado');            
     cajachica_monto_obligado.onblur  = function() {          
         cajachica_monto_obligado.value = fmtNum(cajachica_monto_obligado.value);                         
    };      
    cajachica_monto_obligado.onblur();       
    
    
};






CajaChica.prototype.lista_estructura = function( obj, page, buscar ) { 
    
            

    var mes = document.getElementById('mes').value;
    var clase = document.getElementById('eclase').innerHTML;
    var programa = document.getElementById('eprograma').innerHTML;
    var actividad = document.getElementById('eactividad').innerHTML;
    var objeto = document.getElementById('eobj').innerHTML;
    var ff = document.getElementById('eff').innerHTML;
    var of = document.getElementById('eof').innerHTML;
    var dpto = document.getElementById('edpto').innerHTML;
            
                
    loader.inicio();


    var url = html.url.absolute()+'/api/' + "cajachica/lista/"
        +mes+"/"+clase+"/"+programa+"/"+actividad+"/"
        +objeto+"/"+ff+"/"+of+"/"+dpto + "?page="+page+"&q="+buscar;   



    ajax.async.json( "get", url, null )
        .then(( xhr ) => {

            loader.fin();

            if (xhr.status == 200)
            {     


                var ojson = JSON.parse( xhr.responseText ) ; 
                tabla.json = JSON.stringify(ojson['datos']) ;  


                tabla.ini(obj);                            
                tabla.gene();          
                tabla.formato(obj);


    // acciones de registros
    //tabla.set.tablaid(obj);     
    //tabla.lista_registro(obj, reflex.form_id_promise );                 
                   
            
                tabla.set.tablaid(obj);     
                
                tabla.lista_registro(obj, 
                    function ( obj, id ){

                        /*
                        var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');
                        ordencompra_ordencompra.value =   id

                        ordencompra_ordencompra.value = fmtNum(ordencompra_ordencompra.value);      
                        ordencompra_ordencompra.value = NumQP(ordencompra_ordencompra.value);                      
                        cargar_datos ();        

                        modal.ventana_cerrar("vid");
                        */
                       
                       
                       obj.form_id_promise(obj, id);

                    }

                ); 

                
                var json_paginacion = JSON.stringify(ojson['paginacion']);                
                obj.paginacion_html(obj, json_paginacion )                
            
            
            }
            else {
            }                  

        })            

      


};






CajaChica.prototype.paginacion_html = function( obj , json ) {   


    arasa.html.paginacion.ini(json);

    document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
            = arasa.html.paginacion.gene();   

//    arasa.html.paginacion.move(obj, "", reflex.form_id);    
        

    var listaUL = document.getElementById( obj.tipo + "_paginacion" );
    var uelLI = listaUL.getElementsByTagName('li');


    var pagina = 0;


    for (var i=0 ; i < uelLI.length; i++)
    {
        var lipag = uelLI[i];   

        if (lipag.dataset.pagina == "act"){                                     
            pagina = lipag.firstChild.innerHTML;
        }                    
    }



    for (var i=0 ; i < uelLI.length; i++)
    {
        var datapag = uelLI[i].dataset.pagina;     

        if (!(datapag == "act"  || datapag == "det"  ))
        {
            uelLI[i].addEventListener ( 'click',
                function() {                                      

                    switch (this.dataset.pagina)
                    {
                       case "sig": 
                               pagina = parseInt(pagina) +1;
                               break;                                                                          

                       case "ant":                                     
                               pagina = parseInt(pagina) -1;
                               break;

                       default:  
                               pagina = this.childNodes[0].innerHTML.toString().trim();
                               break;
                    }
                    pagina = parseInt( pagina , 10);                                                                       

                    //arasa.html.paginacion.pagina = pagina;
                    
                    // funcion filtrar  certificaciones
                    var instance = new CajaChica();                     
                    var buscar = document.getElementById("busquedat" ).value;                    
                    instance.lista_estructura(instance, pagina, buscar);                    

                    //tabla.refresh_promise( obj, pagina, busca, fn  ) ;

                },
                false
            );                
        }            
    }    


    
    
    
}













CajaChica.prototype.form_validar = function() {   
    
    
    var cajachica_fecha = document.getElementById('cajachica_fecha');
    if (cajachica_fecha.value == ""){
        msg.error.mostrar("Error en fecha ");                    
        cajachica_fecha.focus();
        cajachica_fecha.select();                                       
        return false;        
    }    
    
    
   
    var cajachica_dependencia = document.getElementById('cajachica_dependencia');    
    if (parseInt(NumQP(cajachica_dependencia.value)) == 0 ) {
        
        msg.error.mostrar("Falta seleccionar Dependencia ");                    
        cajachica_dependencia.focus();
        cajachica_dependencia.select();                                       
        return false;        
    }
    
            
    
    
    
    return true;
};






CajaChica.prototype.detalles_html = function(obj) {   

    var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/detalle.html' 
    
    
    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {
      
        document.getElementById('objeto_detalle').innerHTML = data ;


        var btn_insertar = document.getElementById('btn_insertar');              
        btn_insertar.onclick = function( obj )
        {  
            
            var obj = new CajaChica();    
                        
            var mes = document.getElementById('mes').value;     
            var clase = document.getElementById('eclase').innerHTML;     
            var programa = document.getElementById('eprograma').innerHTML;     
            var actividad = document.getElementById('eactividad').innerHTML;     
            var objeto = document.getElementById('eobj').innerHTML;     
            var ff = document.getElementById('eff').innerHTML;     
            var of = document.getElementById('eof').innerHTML;     
            var dpto = document.getElementById('edpto').innerHTML;     
            
            
            var saldo_disponible =  document.getElementById('saldo_disponible').value;
            saldo_disponible = parseInt(NumQP(saldo_disponible));
            
            obj.new(obj, mes, 
                clase, programa, actividad, objeto, ff, of, dpto,
                saldo_disponible
                );
            
        }; 
            
      })

}











CajaChica.prototype.main_form = function(obj) {    

    var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html'     


    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {
      
        document.getElementById('arti_form').innerHTML = data ;

        obj.form_ini(obj);
      
        // cargar combo
        //obj.combobox_tipo_certificacion("tipo_certificacion");
        
           // obj.form_ini(obj)                
            
      })


};







CajaChica.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['departamento'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}




CajaChica.prototype.preedit = function( obj  ) {                



        
};








CajaChica.prototype.combobox_tipo_certificacion = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/certificacionestipos/all' ;    
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['certificacion_tipo'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}




 


CajaChica.prototype.form_id = function( obj, id ) { 
    
    
    var url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html';   
    
    
    fetch( url )
      .then( response => {
        return response.text();
      })
      .then(data => {
      
             document.getElementById( obj.dom ).innerHTML = data;
     

            var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   
            ajax.async.json( "get", url, null )
                .then(( xhr ) => {

                    if (xhr.status == 200)
                    {     
    //                    var ojson = JSON.parse(xhr.responseText) ;     

                        form.name = "form_" + obj.tipo;
                        form.json = xhr.responseText;   
                        
                        
                        form.disabled(false);
                        form.llenar();                
                        form.llenar_cbx(obj);   
  
                        obj.form_form_id_botones(obj, id);

                    }
                    else{


                    }                  

                })            
            
      })


};




CajaChica.prototype.form_form_id_botones = function( obj, id ) { 
       
        
        
        form.name = "form_certificacionplanfin";
        form.disabled(false);
        

        boton.ini(obj);
        boton.blabels = ["Modificar", "Lista"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;



        var btn_certificacionplanfin_modificar = document.getElementById('btn_certificacionplanfin_modificar');              
        btn_certificacionplanfin_modificar.onclick = function(  )
        {  
            /*
            var obj = new TransitoCosto();    
             */
            
            obj.form_editar(obj, id);
        };       


        var btn_certificacionplanfin_lista = document.getElementById('btn_certificacionplanfin_lista');              
        btn_certificacionplanfin_lista.onclick = function(  )
        {  
 
            var obj = new CertificacionPlanfin();    
            obj.dom = 'arti_form';

            var  tipo_certi =  document.getElementById('certificacionplanfin_certificacion_tipo').value;               



            serie_inicio(obj, tipo_certi);
            
            
            
        };       



}









CajaChica.prototype.post_form_id = function( obj  ) {                

    //certificacionplanfin-acciones
    var id = document.getElementById('certificacionplanfin_id').value;   
    
    obj.form_form_id_botones(obj, id);    
    
    
};






CajaChica.prototype.form_editar = function( obj, id ) { 

        form.name = "form_certificacionplanfin";
        
        obj.form_ini();
        
        form.disabled(true);
        
        boton.ini(obj);
        
        
        boton.blabels = ['Guardar', "Cancelar"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;        
        
        
       
        var btn_certificacionplanfin_guardar = document.getElementById('btn_certificacionplanfin_guardar');              
        btn_certificacionplanfin_guardar.onclick = function(  )
        {  
                        
            
            //var obj = new TransitoCosto();              

            if ( obj.form_validar())
            {
                
                var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   

                form.name = "form_"+obj.tipo;

                var data = form.datos.getjson() ;  
                
                var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   
                ajax.async.json( "put", url, data )
                    .then(( xhr ) => {


                        switch (xhr.status) {

                          case 200:

                                msg.ok.mostrar("registro agregado");          
                                reflex.data  = data;
                                //reflex.form_id( obj, reflex.getJSONdataID( obj.campoid ) ) ;       

                                obj.dom = 'arti_form';
                                obj.form_id( obj, id );
                                
                                break; 

                          case 401:
                                window.location = html.url.absolute();         
                                break;                             

                          case 500:
                                msg.error.mostrar( data );          
                                break; 

                          case 502:                              
                                msg.error.mostrar( data );          
                                break;                                 

                          default: 
                            msg.error.mostrar("error de acceso");           
                        }   


                    })            
                

            }

            
        };       
        
        
        
        
        
        
        
        

        var btn_certificacionplanfin_cancelar = document.getElementById('btn_certificacionplanfin_cancelar');              
        btn_certificacionplanfin_cancelar.onclick = function(  )
        {  
            //obj.form_form_id_botones(obj);
        };       



        
};





CajaChica.prototype.form_id_promise = function( obj, id ){                
  
    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var comp = window.location.pathname;                 
        var path =  comp.replace(arasa.html.url.absolute() , "");

        var xhr =  new XMLHttpRequest();            
        var url = arasa.html.url.absolute() +"/api/"+ obj.recurso + '/'+id;   


        var metodo = "GET";                         
        ajax.json  = null;
        xhr.open( metodo.toUpperCase(),   url,  true );      


        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){



console.log(xhr.responseText)

                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     
                ajax.state = xhr.status;

                var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html' 
                
                fetch(url)
                    .then( response => {
                        return response.text();
                    })
                    .then(data => {
                        document.getElementById('arti_form').innerHTML = data ;

                        // cargar datos 
                        form.name = "form_" + obj.tipo;
                        form.json = xhr.responseText;   

                        form.disabled(false);
                        obj.form_llenar(obj, xhr.responseText )
              
        //form.llenar();                
        //
        //

        //form.llenar_cbx(obj);                
        
                        

                        reflex.acciones.button_reg_promise(obj);
                        
                        boton.ini(obj);
                        boton.blabels =  ['Modificar', 'Eliminar' ,'Lista'] ;
                        var strhtml =  boton.get_botton_base();    
                        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;

                    
                    
                        var btn_cajachica_modificar = document.getElementById('btn_cajachica_modificar');              
                        btn_cajachica_modificar.onclick = function(  )
                        {  
                            obj.editar_acciones(obj, id);                           
                        };       
          
                              
                    

                        var btn_cajachica_eliminar = document.getElementById('btn_cajachica_eliminar');              
                        btn_cajachica_eliminar.onclick = function(  )
                        {                              
                            obj.eliminar_acciones(obj, id);                            
                        };       
          
          


                    

                        var btn_cajachica_lista = document.getElementById('btn_cajachica_lista');              
                        btn_cajachica_lista.onclick = function(  )
                        {  
                            //obj.form_form_id_botones(obj);
                            obj.atras_lista(obj);
                            
                         
                            
                        };       
          
          
          
                        

                    })







                resolve( xhr );
                loader.fin();

            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        xhr.setRequestHeader("path", path );
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( ajax.json  );                       

    })

    return promise;




    
};




CajaChica.prototype.form_llenar = function( obj, json  ) {                

    var ojson = JSON.parse(form.json) ; 
    var campos = document.getElementById( form.name ).getElementsByTagName("input");    

    for(var i=0; i< campos.length; i++) {            
        var c = campos[i];                      


        if (typeof(c.dataset.foreign) === "undefined") {

            if (c.type == "text"  ||  c.type == "hidden"  )
            {                
                if (c.className == "num")
                {
                    c.value  = ojson[c.name]; 
                }
                else
                {  
                    c.value  = ojson[c.name]; 
                    if (c.value === 'undefined') {
                        c.value  = ""; 
                    }  
                }                
            }
            else
            {

                var type= c.type;
                switch(type) {

                    case "date":                                                 
                        c.value  = jsonToDate( ojson[c.name] );   
                        break;                        

                    case "password":                                  
                        c.value  = ojson[c.name];                     
                        break;

                    case "radio":                                  
                        //c.value  = oJson[c.name];         
                        form.radios.databool(c.name, ojson[c.name] );
                        break;

                    default:
                        //code block
                }                           
            }            
        }     
    }


    // mes
    document.getElementById('cajachica_mes' ).value  = ojson["mes"]["mes"];
    
    document.getElementById('cajachica_dependencia' ).value  
            = ojson["dependencia"]["dependencia"];
    
    document.getElementById('dependencia_descripcion_out' ).innerHTML  
            = ojson["dependencia"]["nombre"];
    
            
    obj.form_boolean(json);
};



CajaChica.prototype.atras_lista = function( obj ) {                

    
    obj.dom = 'arti_form';
    obj.main_form(obj);       
    
    

};




CajaChica.prototype.eliminar_acciones = function( obj, id ){      
    

    boton.ini(obj);
    boton.blabels = ["Eliminar", "Cancelar"]
    var strhtml =  boton.get_botton_base();
    document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;
    
    
    
    
    var btn_cajachica_eliminar = document.getElementById('btn_cajachica_eliminar');              
    btn_cajachica_eliminar.onclick = function(  )
    {  
        
        loader.inicio();

        form.name = "form_"+obj.tipo;

        var xhr =  new XMLHttpRequest();     
        var url = arasa.html.url.absolute()  +"/api/"+ obj.recurso +"/"+id;   

        var metodo = "DELETE";        
        
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     

                switch (xhr.status) {

                  case 200:

                        msg.ok.mostrar("registro eliminado");          
                        reflex.data  = xhr.responseText;

                        // ir a lista. de se posible filtrado
                        obj.atras_lista(obj);

                        break; 

                  default: 
                    msg.error.mostrar("error de acceso");           
                }                        

                //arasa.html.url.redirect( xhr.status );                            
                loader.fin();


            }
        };
        xhr.onerror = function (e) {                    
            reject( xhr );                 
        };  


        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);           
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( null );      
        
        
    };       
    

    var btn_cajachica_cancelar = document.getElementById('btn_cajachica_cancelar');              
    btn_cajachica_cancelar.onclick = function(  )
    {  
        alert("cancelar")
    };       




}










CajaChica.prototype.editar_acciones = function( obj, id ){      
    
    // debloquear botones    
    form.name = "form_" + obj.tipo;
    form.disabled(true);        
    obj.form_ini_abm(obj);      
    obj.form_ini_abm_accion(obj);   
    
    document.getElementById('cajachica_monto_requerido').disabled =  true;              
    
    form.mostrar_foreign();
    
        
    
    boton.ini(obj);
    boton.blabels = ["Editar", "Cancelar"]
    var strhtml =  boton.get_botton_base();
    document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;
    
    
    
    
    
    
    var btn_cajachica_editar = document.getElementById('btn_cajachica_editar');              
    btn_cajachica_editar.onclick = function(  )
    {  
        
        loader.inicio();



        form.name = "form_"+obj.tipo;

        var xhr =  new XMLHttpRequest();     
        var url = arasa.html.url.absolute()  +"/api/"+ obj.recurso +"/"+id;   

        var metodo = "PUT";       
        var json = form.datos.getjson() ;


        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     

                switch (xhr.status) {

                  case 200:

                        msg.ok.mostrar("registro editado");          
                        reflex.data  = xhr.responseText;

                        // ir a lista. de se posible filtrado
                        // obj.atras_lista(obj);

                         obj.form_id_promise(obj, id);

                        break; 

                  default: 
                    msg.error.mostrar("error de acceso");           
                }                        

                //arasa.html.url.redirect( xhr.status );                            
                loader.fin();


            }
        };
        xhr.onerror = function (e) {                    
            reject( xhr );                 
        };  


        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);           
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( json );      



        
        
        
    };       
    

    var btn_cajachica_cancelar = document.getElementById('btn_cajachica_cancelar');              
    btn_cajachica_cancelar.onclick = function(  )
    {          
        obj.form_id_promise(obj, id);
    };       




}







CajaChica.prototype.form_boolean = function(json) {     
            
        
    var selbool = document.getElementById("cajachica_obligado");
                
    if (json == null){
        
    
        var opt = document.createElement('option');            
        opt.value = true;
        opt.innerHTML = "Si";                        
        selbool.appendChild(opt);   

        var opt = document.createElement('option');            
        opt.value = false;
        opt.innerHTML = "No";                        
        selbool.appendChild(opt);               
        
        
    }
    else{        
        console.log("bolean no es  null");
        
        var ojson = JSON.parse(json) ; 
        
        var p = ojson["obligado"]; 
        
        if (p == true) {

            var opt = document.createElement('option');            
            opt.value = true;
            opt.innerHTML = "Si";                        
            selbool.appendChild(opt);               
            
            var opt = document.createElement('option');            
            opt.value = false;
            opt.innerHTML = "No";                        
            selbool.appendChild(opt);               
            
        }
        
        if (p == false) {

            var opt = document.createElement('option');            
            opt.value = false;
            opt.innerHTML = "No";                        
            selbool.appendChild(opt);               
            
            var opt = document.createElement('option');            
            opt.value = true;
            opt.innerHTML = "Si";                        
            selbool.appendChild(opt);               
            
        }
        
                
    }
        
}





CajaChica.prototype.form_ini_abm_accion = function(obj) {    
    

                
        
        // buscar dependencia
        var cajachica_dependencia = document.getElementById('cajachica_dependencia');   
        cajachica_dependencia.onblur  = function() {                        

            cajachica_dependencia.value = fmtNum(cajachica_dependencia.value);      
            cajachica_dependencia.value = NumQP(cajachica_dependencia.value);      
            
            var depen = cajachica_dependencia.value ;

            ajax.url =  html.url.absolute()+'/api/dependencias/'+depen;
                        
            ajax.async.get()
                .then(( xhr ) => {

                    if (xhr.status == 200)
                    {     
                        var ojson = JSON.parse(xhr.responseText) ;     

                        document.getElementById('cajachica_dependencia').value = ojson['dependencia'];

                        document.getElementById('dependencia_descripcion_out').innerHTML = 
                                ojson['nombre'];
                    }
                    else{
                        document.getElementById('cajachica_dependencia').value = 0;
                        document.getElementById('cajachica_descripcion_out').innerHTML = "";
                    }                  
                })            
         };    

        

        var ico_more_dependencia = document.getElementById('ico-more-dependencia');
        ico_more_dependencia.addEventListener('click',
            function(event) {     

                var obj = new Dependencia();      

                obj.acctionresul = function(id) {    
                    cajachica_dependencia.value = id; 
                    cajachica_dependencia.onblur(); 
                };       
                modal.ancho = 900;
                busqueda.modal.objeto(obj);
            },
            false
        );        
    
    
};










