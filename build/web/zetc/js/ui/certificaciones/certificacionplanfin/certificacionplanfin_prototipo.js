

function CertificacionPlanfin(){
    
   this.tipo = "certificacionplanfin";   
   this.recurso = "certificacionplanfin";   
   this.value = 0;
   
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/certificaciones";   
   
   
   this.titulosin = ""
   this.tituloplu = ""   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['clase', 'programa', 'actividad', 'obj', 'ff', 'of', 'dpto' ];
   
   this.etiquetas =  ['Dependencia', 'Codigo', 'Nombre'];                                  
    
   this.tablaformat = ['C', 'C'];                                  
   
   this.tbody_id = "certificacionplanfin-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "certificacionplanfin-acciones";   
         
   this.parent = null;
   
   
}





CertificacionPlanfin.prototype.new = function( obj, tipo_certi  ) {                


    reflex.form_new( obj ); 
    
    
    reflex.acciones.button_add_promise(obj);  
    
    
    
    var certificacionplanfin_certificacion_tipo 
            = document.getElementById('certificacionplanfin_certificacion_tipo');          
    
    certificacionplanfin_certificacion_tipo.value = tipo_certi;
    
    
    
    
};








CertificacionPlanfin.prototype.form_ini = function() {    
    
    var certificacionplanfin_clase = document.getElementById('certificacionplanfin_clase');          
    certificacionplanfin_clase.onblur  = function() {                
        certificacionplanfin_clase.value  = fmtNum(certificacionplanfin_clase.value);
    };     
    certificacionplanfin_clase.onblur();        
    
    var certificacionplanfin_programa = document.getElementById('certificacionplanfin_programa');          
    certificacionplanfin_programa.onblur  = function() {                
        certificacionplanfin_programa.value  = fmtNum(certificacionplanfin_programa.value);
    };     
    certificacionplanfin_programa.onblur();        
        
    var certificacionplanfin_actividad = document.getElementById('certificacionplanfin_actividad');          
    certificacionplanfin_actividad.onblur  = function() {                
        certificacionplanfin_actividad.value  = fmtNum(certificacionplanfin_actividad.value);
    };     
    certificacionplanfin_actividad.onblur();        
    
    var certificacionplanfin_obj = document.getElementById('certificacionplanfin_obj');          
    certificacionplanfin_obj.onblur  = function() {                
        certificacionplanfin_obj.value  = fmtNum(certificacionplanfin_obj.value);
    };     
    certificacionplanfin_obj.onblur();        
        
    var certificacionplanfin_ff = document.getElementById('certificacionplanfin_ff');          
    certificacionplanfin_ff.onblur  = function() {                
        certificacionplanfin_ff.value  = fmtNum(certificacionplanfin_ff.value);
    };     
    certificacionplanfin_ff.onblur();        
    
    var certificacionplanfin_of = document.getElementById('certificacionplanfin_of');          
    certificacionplanfin_of.onblur  = function() {                
        certificacionplanfin_of.value  = fmtNum(certificacionplanfin_of.value);
    };     
    certificacionplanfin_of.onblur();        
    
    var certificacionplanfin_dpto = document.getElementById('certificacionplanfin_dpto');          
    certificacionplanfin_dpto.onblur  = function() {                
        certificacionplanfin_dpto.value  = fmtNum(certificacionplanfin_dpto.value);
    };     
    certificacionplanfin_dpto.onblur();        
    
    
    
    
    
    
    
    var certificacionplanfin_enero = document.getElementById('certificacionplanfin_enero');          
    certificacionplanfin_enero.onblur  = function() {                
        certificacionplanfin_enero.value  = fmtNum(certificacionplanfin_enero.value);
    };     
    certificacionplanfin_enero.onblur();        
    
    var certificacionplanfin_febrero = document.getElementById('certificacionplanfin_febrero');          
    certificacionplanfin_febrero.onblur  = function() {                
        certificacionplanfin_febrero.value  = fmtNum(certificacionplanfin_febrero.value);
    };     
    certificacionplanfin_febrero.onblur();        
    
    var certificacionplanfin_marzo = document.getElementById('certificacionplanfin_marzo');          
    certificacionplanfin_marzo.onblur  = function() {                
        certificacionplanfin_marzo.value  = fmtNum(certificacionplanfin_marzo.value);
    };     
    certificacionplanfin_marzo.onblur();        
    
    var certificacionplanfin_abril = document.getElementById('certificacionplanfin_abril');          
    certificacionplanfin_abril.onblur  = function() {                
        certificacionplanfin_abril.value  = fmtNum(certificacionplanfin_abril.value);
    };     
    certificacionplanfin_abril.onblur();        
    
    var certificacionplanfin_mayo = document.getElementById('certificacionplanfin_mayo');          
    certificacionplanfin_mayo.onblur  = function() {                
        certificacionplanfin_mayo.value  = fmtNum(certificacionplanfin_mayo.value);
    };     
    certificacionplanfin_mayo.onblur();        
    
    var certificacionplanfin_junio = document.getElementById('certificacionplanfin_junio');          
    certificacionplanfin_junio.onblur  = function() {                
        certificacionplanfin_junio.value  = fmtNum(certificacionplanfin_junio.value);
    };     
    certificacionplanfin_junio.onblur();        
    
    var certificacionplanfin_julio = document.getElementById('certificacionplanfin_julio');          
    certificacionplanfin_julio.onblur  = function() {                
        certificacionplanfin_julio.value  = fmtNum(certificacionplanfin_julio.value);
    };     
    certificacionplanfin_julio.onblur();        
    
    var certificacionplanfin_agosto = document.getElementById('certificacionplanfin_agosto');          
    certificacionplanfin_agosto.onblur  = function() {                
        certificacionplanfin_agosto.value  = fmtNum(certificacionplanfin_agosto.value);
    };     
    certificacionplanfin_agosto.onblur();        

    var certificacionplanfin_septiembre = document.getElementById('certificacionplanfin_septiembre');          
    certificacionplanfin_septiembre.onblur  = function() {                
        certificacionplanfin_septiembre.value  = fmtNum(certificacionplanfin_septiembre.value);
    };     
    certificacionplanfin_septiembre.onblur();        
    
    var certificacionplanfin_octubre = document.getElementById('certificacionplanfin_octubre');          
    certificacionplanfin_octubre.onblur  = function() {                
        certificacionplanfin_octubre.value  = fmtNum(certificacionplanfin_octubre.value);
    };     
    certificacionplanfin_octubre.onblur();        
    
    var certificacionplanfin_noviembre = document.getElementById('certificacionplanfin_noviembre');          
    certificacionplanfin_noviembre.onblur  = function() {                
        certificacionplanfin_noviembre.value  = fmtNum(certificacionplanfin_noviembre.value);
    };     
    certificacionplanfin_noviembre.onblur();        
    
    var certificacionplanfin_diciembre = document.getElementById('certificacionplanfin_diciembre');          
    certificacionplanfin_diciembre.onblur  = function() {                
        certificacionplanfin_diciembre.value  = fmtNum(certificacionplanfin_diciembre.value);
    };     
    certificacionplanfin_diciembre.onblur();        
    
    
    
    
    
    
    
};






CertificacionPlanfin.prototype.form_validar = function() {    
    
    return true;
};










CertificacionPlanfin.prototype.main_form = function(obj) {    


    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/lista.html' )
      .then( response => {
        return response.text();
      })
      .then(data => {
      
      console.log(data);
      
        document.getElementById('arti_form').innerHTML = data ;
      
        // cargar combo
        obj.combobox_tipo_certificacion("tipo_certificacion");
        
           // obj.form_ini(obj)                
            
      })


};







CertificacionPlanfin.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['departamento'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}




CertificacionPlanfin.prototype.preedit = function( obj  ) {                

        
};








CertificacionPlanfin.prototype.combobox_tipo_certificacion = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/certificacionestipos/all' ;    
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['certificacion_tipo'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}






CertificacionPlanfin.prototype.serie = function( obj, dom, ojson ) { 
    
    
    
    var elementos = dom.getElementsByClassName('serie_elemento_valor');
    for(var i=0; i< elementos.length; i++) {       

        var e = elementos[i];
                
        switch(e.dataset.valor) {
            
            case "clase":                                                 
                e.innerHTML = ojson.clase ;
                break;             
            
            case "programa":                                                 
                e.innerHTML = ojson.programa ;
                break;                   
                
            case "actividad":                                                 
                e.innerHTML = ojson.actividad ;
                break;                   
            
            case "obj":                                                 
                e.innerHTML = ojson.obj ;
                break;                               
            
            case "ff":                                                 
                e.innerHTML = ojson.ff ;
                break;                               
            
            case "of":                                                 
                e.innerHTML = ojson.of ;
                break;                               
            
            case "dpto":                                                 
                e.innerHTML = ojson.dpto ;
                break;                               
            
            
            case "enero":                                                 
                e.innerHTML = fmtNum(ojson.enero) ;
                break;                               
                        
            case "febrero":                                                 
                e.innerHTML = fmtNum(ojson.febrero) ;
                break;                               
                        
            case "marzo":                                                 
                e.innerHTML = fmtNum(ojson.marzo) ;
                break;                               
                        
            case "abril":                                                 
                e.innerHTML = fmtNum(ojson.abril) ;
                break;                               
                        
            case "mayo":                                                 
                e.innerHTML = fmtNum(ojson.mayo) ;
                break;                               
                        
            case "junio":                                                 
                e.innerHTML = fmtNum(ojson.junio) ;
                break;                               
                        
            case "julio":                                                 
                e.innerHTML = fmtNum(ojson.julio) ;
                break;                               
                        
            case "agosto":                                                 
                e.innerHTML = fmtNum(ojson.agosto) ;
                break;                               
                        
            case "septiembre":                                                 
                e.innerHTML = fmtNum(ojson.septiembre) ;
                break;                               
                        
            case "octubre":                                                 
                e.innerHTML = fmtNum(ojson.octubre) ;
                break;                               
                        
            case "noviembre":                                                 
                e.innerHTML = fmtNum(ojson.noviembre) ;
                break;                               
                        
            case "diciembre":                                                 
                e.innerHTML = fmtNum(ojson.diciembre) ;
                break;                               
                        
                        
            case "enero_suma":                                                 
                e.innerHTML = fmtNum(ojson.enero_suma) ;
                break;                               
                        
            case "febrero_suma":                                                 
                e.innerHTML = fmtNum(ojson.febrero_suma) ;
                break;                               
                        
            case "marzo_suma":                                                 
                e.innerHTML = fmtNum(ojson.marzo_suma) ;
                break;                               
                        
            case "abril_suma":                                                 
                e.innerHTML = fmtNum(ojson.abril_suma) ;
                break;                               
                        
            case "mayo_suma":                                                 
                e.innerHTML = fmtNum(ojson.mayo_suma) ;
                break;                               
                        
            case "junio_suma":                                                 
                e.innerHTML = fmtNum(ojson.junio_suma) ;
                break;                               
                        
            case "julio_suma":                                                 
                e.innerHTML = fmtNum(ojson.julio_suma) ;
                break;                               
                        
            case "agosto_suma":                                                 
                e.innerHTML = fmtNum(ojson.agosto_suma) ;
                break;                               
                        
            case "septiembre_suma":                                                 
                e.innerHTML = fmtNum(ojson.septiembre_suma) ;
                break;                               
                        
            case "octubre_suma":                                                 
                e.innerHTML = fmtNum(ojson.octubre_suma) ;
                break;                               
                        
            case "noviembre_suma":                                                 
                e.innerHTML = fmtNum(ojson.noviembre_suma) ;
                break;                               
                        
            case "diciembre_suma":                                                 
                e.innerHTML = fmtNum(ojson.diciembre_suma) ;
                break;                               

            

            case "enero_saldo":                                                 
                e.innerHTML = fmtNum(ojson.enero_saldo) ;
                break;                               
                        
            case "febrero_saldo":                                                 
                e.innerHTML = fmtNum(ojson.febrero_saldo) ;
                break;                               
                        
            case "marzo_saldo":                                                 
                e.innerHTML = fmtNum(ojson.marzo_saldo) ;
                break;                               
                        
            case "abril_saldo":                                                 
                e.innerHTML = fmtNum(ojson.abril_saldo) ;
                break;                               
                        
            case "mayo_saldo":                                                 
                e.innerHTML = fmtNum(ojson.mayo_saldo) ;
                break;                               
                        
            case "junio_saldo":                                                 
                e.innerHTML = fmtNum(ojson.junio_saldo) ;
                break;                               
                        
            case "julio_saldo":                                                 
                e.innerHTML = fmtNum(ojson.julio_saldo) ;
                break;                               
                        
            case "agosto_saldo":                                                 
                e.innerHTML = fmtNum(ojson.agosto_saldo) ;
                break;                               
                        
            case "septiembre_saldo":                                                 
                e.innerHTML = fmtNum(ojson.septiembre_saldo) ;
                break;                               
                        
            case "octubre_saldo":                                                 
                e.innerHTML = fmtNum(ojson.octubre_saldo) ;
                break;                               
                        
            case "noviembre_saldo":                                                 
                e.innerHTML = fmtNum(ojson.noviembre_saldo) ;
                break;                               
                        
            case "diciembre_saldo":                                                 
                e.innerHTML = fmtNum(ojson.diciembre_saldo) ;
                break;                               


            default:
                //code block
        }                              

    
        //campos[i].innerHTML = "fasdfasjfkasjfkasj";

    }

    
    
    //var ele = dom.getElementsByTagName("div");        
    //console.log(ele.length);
    
    
};


 


CertificacionPlanfin.prototype.form_id = function( obj, id ) { 
    
    
    var url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html';   
    
    
    fetch( url )
      .then( response => {
        return response.text();
      })
      .then(data => {
      
             document.getElementById( obj.dom ).innerHTML = data;
     

            var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   
            ajax.async.json( "get", url, null )
                .then(( xhr ) => {

                    if (xhr.status == 200)
                    {     
    //                    var ojson = JSON.parse(xhr.responseText) ;     

                        form.name = "form_" + obj.tipo;
                        form.json = xhr.responseText;   
                        
                        
                        form.disabled(false);
                        form.llenar();                
                        form.llenar_cbx(obj);   
  

                        obj.form_form_id_botones(obj, id);


                    }
                    else{


                    }                  

                })            

            
      })


};




CertificacionPlanfin.prototype.form_form_id_botones = function( obj, id ) { 
       
        
        
        form.name = "form_certificacionplanfin";
        form.disabled(false);
        

        boton.ini(obj);
        boton.blabels = ["Modificar", "Lista"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;



        var btn_certificacionplanfin_modificar = document.getElementById('btn_certificacionplanfin_modificar');              
        btn_certificacionplanfin_modificar.onclick = function(  )
        {  
            /*
            var obj = new TransitoCosto();    
             */
            
            obj.form_editar(obj, id);
        };       


        var btn_certificacionplanfin_lista = document.getElementById('btn_certificacionplanfin_lista');              
        btn_certificacionplanfin_lista.onclick = function(  )
        {  
 
            var obj = new CertificacionPlanfin();    
            obj.dom = 'arti_form';

            var  tipo_certi =  document.getElementById('certificacionplanfin_certificacion_tipo').value;               


console.log(tipo_certi);

            serie_inicio(obj, tipo_certi);
            
            
            
        };       



}









CertificacionPlanfin.prototype.post_form_id = function( obj  ) {                

    //certificacionplanfin-acciones
    var id = document.getElementById('certificacionplanfin_id').value;   
    
    obj.form_form_id_botones(obj, id);    
    
    
};






CertificacionPlanfin.prototype.form_editar = function( obj, id ) { 

        form.name = "form_certificacionplanfin";
        
        obj.form_ini();
        
        form.disabled(true);
        
        boton.ini(obj);
        
        
        boton.blabels = ['Guardar', "Cancelar"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;        
        
        
       
        var btn_certificacionplanfin_guardar = document.getElementById('btn_certificacionplanfin_guardar');              
        btn_certificacionplanfin_guardar.onclick = function(  )
        {  
                        
            
            //var obj = new TransitoCosto();              

            if ( obj.form_validar())
            {
                
                var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   

                form.name = "form_"+obj.tipo;

                var data = form.datos.getjson() ;  
                
                
console.log(data);                
                
                var url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;   
                ajax.async.json( "put", url, data )
                    .then(( xhr ) => {


                        switch (xhr.status) {

                          case 200:

                                msg.ok.mostrar("registro agregado");          
                                reflex.data  = data;
                                //reflex.form_id( obj, reflex.getJSONdataID( obj.campoid ) ) ;       

                                obj.dom = 'arti_form';
                                obj.form_id( obj, id );
                                
                                break; 

                          case 401:
                                window.location = html.url.absolute();         
                                break;                             

                          case 500:
                                msg.error.mostrar( data );          
                                break; 

                          case 502:                              
                                msg.error.mostrar( data );          
                                break;                                 

                          default: 
                            msg.error.mostrar("error de acceso");           
                        }   


                    })            
                

            }

            
        };       
        
        
        
        
        
        
        
        

        var btn_certificacionplanfin_cancelar = document.getElementById('btn_certificacionplanfin_cancelar');              
        btn_certificacionplanfin_cancelar.onclick = function(  )
        {  
            //obj.form_form_id_botones(obj);
        };       



        
};




