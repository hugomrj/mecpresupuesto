
var  gFiltro = "";

window.onload = function( dom ) {
                    
        
    arasa.html.url.control_promise()
        .then(( xhr ) => {

  
            html.menu.mostrar();    

            var obj = new CodigoContratacion();    
            obj.dom = 'arti_form';

            obj.main_list(obj, 1);   
        

        })
        .catch(( xhr ) => {                                         
            console.log(xhr.message);                    
        });             

    
};
   
   
   
   
   
   
   
   
   
window.onresize = function() {
    document.body.style.minheight  = "100%" ;  
    var nodeList = document.querySelectorAll('.capaoscura');
    for (var i = 0; i < nodeList.length; ++i) {
        document.getElementById(nodeList[i].id).style.height = document.body.scrollHeight.toString() + "px";                
    }
};








function busqueda_bar_accion( obj ){    

    var search_bar = document.getElementById( "icon-search_bar" );
    search_bar.addEventListener('click',
        function(event) {      


            var b1 = document.getElementById( "icon-search_bar" ); 
            b1.style.visibility = "hidden";        
            b1.parentNode.style.display = "none";     

            var b2 = document.getElementById( "icon-up_bar" ); 
            b2.style.visibility = "visible";        
            b2.parentNode.style.display = "block";     



            // cuadro de busqueda            
            fetch( html.url.absolute() +  '/contrataciones/codigocontratacion/'+ '/htmf/busqueda.html' )
              .then(response => {
                return response.text();
              })
              .then(data => {
                document.getElementById( "divbusqueda" ).innerHTML =  data;            

                // funcion js
                var busquedatexto = document.getElementById("busquedatexto");
                busquedatexto.addEventListener("keyup", function(event) {                      
                  if (event.keyCode === 13) {                      
                    event.preventDefault();     
                    
                    var tex = this.value;
                    busqueda_main_promise( obj, tex, 1);
                    
                  }
                });




                var idcompuest = document.getElementById("idcompuest");    
                idcompuest.onclick = function()
                {                                  
                    document.getElementById("busqueda_simple").style.display="none";                       

                    fetch( html.url.absolute() +   '/contrataciones/codigocontratacion/'+ '/htmf/busqueda_compuesta.html' )
                      .then(response => {
                        return response.text();
                      })
                      .then(data => {
                        
                        document.getElementById("bus_com_id").style.display="inherit";                           
                        document.getElementById( "bus_com_id" ).innerHTML =  data;    
                        
                        form_busqueda_compuesta( obj );
                          
                      })        
                };        
                
              })            


        },
        false
    );     

    
    

    var up_bar = document.getElementById( "icon-up_bar" );
    up_bar.addEventListener('click',
        function(event) {      


            var b1 = document.getElementById( "icon-search_bar" ); 
            b1.style.visibility = "visible";        
            b1.parentNode.style.display = "block";     


            var b2 = document.getElementById( "icon-up_bar" ); 
            b2.style.visibility = "hidden";        
            b2.parentNode.style.display = "none";      


            document.getElementById( "divbusqueda" ).innerHTML =  "";            
                
            obj.filtro = "";    
            obj.main_list(obj, 1); 

        },
        false
    );     

}






  
function busqueda_main_promise(obj, textobusqueda, page ){                          

    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var comp = window.location.pathname;                 
        var path =  comp.replace(arasa.html.url.absolute() , "");

        var xhr =  new XMLHttpRequest();            

        obj.filtro = ';q=' + textobusqueda ;
        var url = arasa.html.url.absolute() +'/api/' + "codigoscontrataciones" 
                + obj.getUrlFiltro(obj) + '?page=' + page;  ;    

        //var url = arasa. +"/api/monedas?page=2"; 
        var metodo = "GET";                         
        ajax.json  = null;

        xhr.open( metodo.toUpperCase(),   url,  true );      


        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){


                arasa.vista.tabla_html(obj)
                    .then(( text ) => {

                        ajax.local.token =  xhr.getResponseHeader("token") ;            
                        localStorage.setItem('token', xhr.getResponseHeader("token") );     
                        //sessionStorage.setItem('total_registros',  ajax.xhr.getResponseHeader("total_registros"));

                        // crear y cargar la tabla html

                        var ojson = JSON.parse( xhr.responseText ) ;         
                        

                        tabla.json = JSON.stringify(ojson['datos']) ;


                        tabla.ini(obj);
                        tabla.gene();   
                        tabla.formato(obj);

                        tabla.set.tablaid(obj);     
                        tabla.lista_registro(obj, reflex.form_id_promise ); 


                        //acciones_main_list(obj);


                       var json_paginacion = JSON.stringify(ojson['paginacion']);

                       arasa.vista.paginacion_html(obj, json_paginacion );

                       ajax.state = xhr.status;

                            resolve( xhr );

                       loader.fin();
                        
                    })


            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        xhr.setRequestHeader("path", path );
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   

        //ajax.headers.set();
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( ajax.json  );                       



    })

    return promise;

}






  
function form_busqueda_compuesta( obj ){      
    
        
    
    var objetocom = document.getElementById( 'objetocom');
    objetocom.onblur  = function() {          
        objetocom.value = fmtNum(objetocom.value);      
        objetocom.value = NumQP(objetocom.value);               
    };      
    objetocom.onblur();
    
    
    var btn_com_buscar = document.getElementById( 'btn_com_buscar');
    btn_com_buscar.onclick = function()
    {  
    
        gFiltro = "";

        var buscarcom = document.getElementById( 'buscarcom');
        gFiltro = gFiltro + ";buscarcom=" + buscarcom.value ;

  
  
        gFiltro = gFiltro + ";obj=" + objetocom.value ;
        
        gFiltro = gFiltro + ";z=a"  ;

        loader.inicio();

        var comp = window.location.pathname;                 
        var path =  comp.replace(arasa.html.url.absolute() , "");

        var xhr =  new XMLHttpRequest();            
//        var url = arasa. +"/api/monedas"; 


        obj.filtro = gFiltro;
        var url = arasa.html.url.absolute() +'/api/' + "codigoscontrataciones" 
                + obj.getUrlFiltro(obj) + '?page=' + 1;      


        //var url = arasa. +"/api/monedas?page=2"; 
        var metodo = "GET";                         
        ajax.json  = null;

        xhr.open( metodo.toUpperCase(),   url,  true );      


        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){


                arasa.vista.tabla_html(obj)
                    .then(( text ) => {

                        ajax.local.token =  xhr.getResponseHeader("token") ;            
                        localStorage.setItem('token', xhr.getResponseHeader("token") );     
                        //sessionStorage.setItem('total_registros',  ajax.xhr.getResponseHeader("total_registros"));

                        // crear y cargar la tabla html

                        var ojson = JSON.parse( xhr.responseText ) ;                        

                        tabla.json = JSON.stringify(ojson['datos']) ;


                        tabla.ini(obj);
                        tabla.gene();   
                        tabla.formato(obj);

                        tabla.set.tablaid(obj);     
                        tabla.lista_registro(obj, reflex.form_id_promise ); 

                        //acciones_main_list(obj);
                       
                       
                       //'paginacion
                       var json_paginacion = JSON.stringify(ojson['paginacion']);

                       arasa.vista.paginacion_html(obj, json_paginacion );

                       ajax.state = xhr.status;

                       loader.fin();
                        
                    })


            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        xhr.setRequestHeader("path", path );
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   

        //ajax.headers.set();
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( ajax.json  );                       




    };   
    
    
        

    
    
    
    

    var btn_com_cerrar = document.getElementById( 'btn_com_cerrar');
    btn_com_cerrar.onclick = function()
    {  

        document.getElementById("busqueda_simple").style.display="inherit";   
        document.getElementById("bus_com_id").style.display="none";   
        
        
        
        //document.getElementById("bus_com_id").style.visibility = "hidden";    

    };   

    
    
}

