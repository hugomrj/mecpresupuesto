

function CodigoContratacion(){
    
   this.tipo = "codigocontratacion";   
   this.recurso = "codigoscontrataciones";   
   this.value = 0;
   
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/contrataciones";   
   
   
   this.titulosin = "Codigo contratacion"
   this.tituloplu = "Codigo contratacion"   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['cc', 'fecha_emision', 'tipo_procedimiento', 'numero_pac',
                        'tipo_contrato', 'numero', 'fecha', 'monto_total',
                        'nombre'
                        ];
   
   this.etiquetas =  ['cc', 'fecha_emision', 'tipo_procedimiento', 'numero_pac',
                        'tipo_contrato', 'numero', 'fecha', 'monto_total',
                        'nombre'
                        ];                           
    
   this.tablaformat = ['C', 'D', 'C', 'C', 'C', 'C', 'D', 'N', 'C'];                                  
   
   this.tbody_id = "codigocontratacion-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "codigocontratacion-acciones";   
         
   this.parent = null;
   
   this.filtro = "";
      
}





CodigoContratacion.prototype.new = function( obj  ) {       

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);  
    
};





CodigoContratacion.prototype.form_ini = function() {    
    
};






CodigoContratacion.prototype.form_validar = function() {        
    return true;
};










CodigoContratacion.prototype.main_list = function(obj, page) {    

    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
              
            /*
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            */
            
            
            
            // cuadro de busqueda            
            fetch(  html.url.absolute() + '/contrataciones/codigocontratacion/htmf/busqueda_bar.html' )
              .then(response => {
                return response.text();
              })
              .then(data => {
                document.getElementById( "bar_busqueda" ).innerHTML =  data;    
                busqueda_bar_accion(obj);
                
              })            
            
            
            


        })
        /*
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 
        */
};







CodigoContratacion.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['departamento'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}




CodigoContratacion.prototype.preedit = function( obj  ) {                

    //document.getElementById('dependencia_dependencia').disabled = false; 
    
    //alert("desbloquear dependencia codigo")
        
};






CodigoContratacion.prototype.post_form_id = function( obj, id  ) {                

   
    form_cc_datos_registro( id  )
    
    // botones de accion - nuevo para este caso       
    //boton.objeto = ""+obj.tipo;
    boton.blabels = ['Lista']
    document.getElementById( 'codigocontratacion-acciones' ).innerHTML 
        =  boton.get_botton_base();                
            
    var btn_codigocontratacion_lista = document.getElementById('btn_codigocontratacion_lista');              
    btn_codigocontratacion_lista.onclick = function(  )
    {   
        obj.main_list(obj, 1);   
    };   

    
    
    
    
};




CodigoContratacion.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    return ret;
    
};
