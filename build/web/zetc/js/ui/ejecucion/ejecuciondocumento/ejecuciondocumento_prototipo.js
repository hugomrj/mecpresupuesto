

function EjecucionDocumento(){
    
   this.tipo = "ejecuciondocumento";   
   this.recurso = "ejecucionesdocumentos";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/ejecucion";   
   
   
   this.campoid=  'id';
   
   /*
   this.tablacampos =  ['clase', 'programa', 'actividad', 'obj',  'ff', 'of',
                'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num', 
                'fecha', 'rcp', 'concepto', 'obligado', 'pagado' ];
   */
   
   
   this.tablacampos =  [ 'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];
   
   
   
   this.etiquetas =  [ 'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];            
            
   this.tablaformat =  [ 'U', 'N', 'U', 'Z',
                        'D', 'C', 'C', 'N', 'N'];
   

      
   this.botones_lista = [ this.lista_new] ;
   //this.botones_form = "consulta-acciones";   
  
   
}





EjecucionDocumento.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





EjecucionDocumento.prototype.form_ini = function(obj) {    


    var clase = document.getElementById('clase');          
    clase.onblur  = function() {                
        clase.value  = fmtNum(clase.value);
    };     
    clase.onblur();       
            
    var programa = document.getElementById('programa');          
    programa.onblur  = function() {                
        programa.value  = fmtNum(programa.value);
    };     
    programa.onblur();       
            
    var actividad = document.getElementById('actividad');          
    actividad.onblur  = function() {                
        actividad.value  = fmtNum(actividad.value);
    };     
    actividad.onblur();       
            
    var objeto = document.getElementById('objeto');          
    objeto.onblur  = function() {                
        objeto.value  = fmtNum(objeto.value);
    };     
    objeto.onblur();       
            
    var ff = document.getElementById('ff');          
    ff.onblur  = function() {                
        ff.value  = fmtNum(ff.value);
    };     
    ff.onblur();       
            
    var of = document.getElementById('of');          
    of.onblur  = function() {                
        of.value  = fmtNum(of.value);
    };     
    of.onblur();       
            

    
    
    
    
    // boton enviar
    var btn_cab_buscar = document.getElementById('btn_cab_buscar');
    btn_cab_buscar.onclick = function(event) {     
    
        
            var ed = new EjecucionDocumento;
            ed.consulta1_detalle_promesa(obj);
        
        
    }
          
        
    
};






EjecucionDocumento.prototype.form_validar = function() {    
    
    return true;
};







EjecucionDocumento.prototype.main_form = function(obj) {    

    
    //document.getElementById('det_form').innerHTML = "resultado";
    //ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html'; 



    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('cab_form').innerHTML = data ;
        
            obj.form_ini(obj)                
            
      })





    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/det.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('det_form').innerHTML = data ;
        
        //reflex.getTituloplu(obj);                                  
            
      })



};





EjecucionDocumento.prototype.consulta1_detalle_promesa = function( obj  ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();
            
   
            var url = html.url.absolute() + '/api/ejecuciondocumento/consulta1/'
                +';clase='+document.getElementById("clase").value 
                +';prog='+document.getElementById("programa").value 
                +';acti='+document.getElementById("actividad").value 
                +';obj='+document.getElementById("objeto").value 
                +';ff='+document.getElementById("ff").value 
                +';of='+document.getElementById("of").value 
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;

                    var ojson = JSON.parse( tabla.json ) ; 
                    tabla.json = JSON.stringify(ojson['datos']) ;  
                    
                    
                    if (tabla.json != "[]"){
                    
                        tabla.ini(obj);                            
                        tabla.gene();              
                            
                        tabla.formato(obj);                            
                        
                        
                        // suma de consultas
                        var jsumas = JSON.stringify(ojson['summary']) ;              
                        var ojsumas = JSON.parse(jsumas) ;
                        
                        document.getElementById("sum_obligado").innerHTML
                            =  fmtNum( ojsumas["obligado"] );
                        
                        document.getElementById("sum_pagado").innerHTML
                            =  fmtNum( ojsumas["pagado"] );
                        
                        
                    }
                    else{
                       
                        
                       
                        fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/det.html' )
                          .then(response => {
                            return response.text();
                          })
                          .then(data => {
                            document.getElementById('det_form').innerHTML = data ;
                            msg.error.mostrar("No existen registros");
                          })                       
                       
                        
                    }

  
                         resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};



