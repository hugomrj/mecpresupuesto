

SELECT  
  e.clase,   e.programa,   e.actividad,   e.obj,   e.ff,   e.of,   e.dpto,   objetos.obj_decripcion,  
  
	case when i.inicial  is null then 0 else i.inicial end as inicial, 
	case when i.modifi  is null then 0 else i.modifi end as modifi, 
	case when i.vigente  is null then 0 else i.vigente end as vigente,  
	
	case when pla1  is null then 0 else pla1 end as pla1, case when eje1  is null then 0 else eje1 end as eje1, 
	case when pla2  is null then 0 else pla2 end as pla2, case when eje2  is null then 0 else eje2 end as eje2, 
	case when pla3  is null then 0 else pla3 end as pla3, case when eje3  is null then 0 else eje3 end as eje3, 	
	case when pla4  is null then 0 else pla4 end as pla4, case when eje4  is null then 0 else eje4 end as eje4, 		
	case when pla5  is null then 0 else pla5 end as pla5, case when eje5  is null then 0 else eje5 end as eje5, 			
	case when pla6  is null then 0 else pla6 end as pla6, case when eje6  is null then 0 else eje6 end as eje6,  				
	case when pla7  is null then 0 else pla7 end as pla7, case when eje7  is null then 0 else eje7 end as eje7,  				  
	case when pla8  is null then 0 else pla8 end as pla8, case when eje8  is null then 0 else eje8 end as eje8,  	
	case when pla9  is null then 0 else pla9 end as pla9, case when eje9  is null then 0 else eje9 end as eje9,  				  	
  	case when pla10  is null then 0 else pla10 end as pla10, case when eje10  is null then 0 else eje10 end as eje10,  				  	
	case when pla11  is null then 0 else pla11 end as pla11, case when eje11  is null then 0 else eje11 end as eje11,  				  	
	case when pla12  is null then 0 else pla12 end as pla12, case when eje12  is null then 0 else eje12 end as eje12,  				  		
  
  	case when platotal  is null then 0 else platotal end as platotal, case when ejetotal  is null then 0 else ejetotal end as ejetotal, 

	case when planfinanciero  is null then 0 else planfinanciero end as planfinanciero,  
	case when obligado  is null then 0 else obligado end as obligado,  	
	case when saldo  is null then 0 else saldo end as saldo,  	
	case when saldo_plan  is null then 0 else saldo_plan end as saldo_plan,  	
	case when pagado  is null then 0 else pagado end as pagado,  		
	case when pendiente  is null then 0 else pendiente end as pendiente,  		
	case when prevision  is null then 0 else prevision end as prevision,  		
	case when compromiso  is null then 0 else compromiso end as compromiso   
  
FROM  
  migracion.qry_estructura e left join basicas.objetos on (objetos.obj = e.obj)  
  left join migracion.inicial_migracion i on ( i.clase = e.clase AND  i.programa = e.programa AND  i.actividad = e.actividad AND 
	i.obj = e.obj AND  i.ff = e.ff AND  i.of = e.of AND  i.dpto = e.dpto )  

  left join migracion.planfinanciero_migracion p on ( p.clase = e.clase AND  p.programa = e.programa AND  p.actividad = e.actividad AND 
	p.obj = e.obj AND  p.ff = e.ff AND  p.of = e.of AND  p.dpto = e.dpto )   


  left join migracion.qry_ejecucion_migracion j on ( j.clase = e.clase AND  j.programa = e.programa AND  j.actividad = e.actividad AND 
	j.obj = e.obj AND  j.ff = e.ff AND  j.of = e.of AND  j.dpto = e.dpto )  	 


  left join migracion.prevcom_migracion c on ( c.clase = e.clase AND  c.programa = e.programa AND  c.actividad = e.actividad AND 
	c.obj = e.obj AND  c.ff = e.ff AND  c.of = e.of AND  c.dpto = e.dpto )   


WHERE e.clase = v0 
    AND e.programa = v1  
    AND e.actividad = v2 
AND e.obj = v3  
AND e.ff = v4  
AND e.of = v5  
AND e.dpto = v6  

