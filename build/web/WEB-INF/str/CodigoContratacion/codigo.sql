
SELECT  
  codigos_contrataciones.id,   codigos_contrataciones.cc,   codigos_contrataciones.fecha_emision,  
  codigos_contrataciones.nivel_entidad,   codigos_contrataciones.entidad,   codigos_contrataciones.unidad_compradora,  
  codigos_contrataciones.tipo_procedimiento,   codigos_contrataciones.descripcion,   codigos_contrataciones.numero_pac,  
  codigos_contrataciones.tipo_contrato,   codigos_contrataciones.numero,   codigos_contrataciones.fecha,  
  codigos_contrataciones.monto_total,   codigos_contrataciones.moneda,   codigos_contrataciones.proveedor,  
  proveedores.nombre,   proveedores.domicilio_legal,   proveedores.nombre_fantasia,   proveedores.representante_legal,  
  proveedores.pais_origen,   proveedores.ruc,   proveedores.telefono,   proveedores.email 
FROM  
  contrataciones.codigos_contrataciones,   
  contrataciones.proveedores  
WHERE  
  codigos_contrataciones.proveedor = proveedores.proveedor       
  and id = v0    

