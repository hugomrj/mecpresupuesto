
 select e.*,  
 f.ley inicial1, p.inicial inicial2, 
 ((CASE WHEN f.ley is null THEN 0 ELSE f.ley end) - (CASE WHEN p.inicial is null THEN 0 ELSE p.inicial end)) dif_inicial, 
 f.modi modifi1, p.modifi modifi2, 
 ((CASE WHEN f.modi is null THEN 0 ELSE f.modi end) - (CASE WHEN p.modifi is null THEN 0 ELSE p.modifi end)) dif_modifi, 
 f.vig vigente1, p.vigente vigente2,  
 ((CASE WHEN f.vig is null THEN 0 ELSE f.vig end) - (CASE WHEN p.vigente is null THEN 0 ELSE p.vigente end)) dif_vigente, 
 
 f.pagado pagado1, p.pagado pagado2, 
 ((CASE WHEN f.pagado is null THEN 0 ELSE f.pagado end) - (CASE WHEN p.pagado is null THEN 0 ELSE p.pagado end)) dif_pagado, 
 
 f.oblipenpa pendiente1, p.pendiente pendiente2, 
 ((CASE WHEN f.oblipenpa is null THEN 0 ELSE f.oblipenpa end) - (CASE WHEN p.pendiente is null THEN 0 ELSE p.pendiente end)) dif_pendiente,  
 
 f.total_e ejetotal1, p.ejetotal ejetotal2, 
 ((CASE WHEN f.total_e is null THEN 0 ELSE f.total_e end) - (CASE WHEN p.ejetotal is null THEN 0 ELSE p.ejetotal end)) dif_ejetotal , 
 
 f.planf planificado1, p.planificado planificado2, 
 ((CASE WHEN f.planf is null THEN 0 ELSE f.planf end) - (CASE WHEN p.planificado is null THEN 0 ELSE p.planificado end)) dif_planificado , 
 
f.total_pl platotal1, p.platotal platotal2,  
 ((CASE WHEN f.total_pl is null THEN 0 ELSE f.total_pl end) - (CASE WHEN p.platotal is null THEN 0 ELSE p.platotal end)) dif_platotal,   
 
f.obligado obligado1, p.obligado obligado2, 
 ((CASE WHEN f.obligado is null THEN 0 ELSE f.obligado end) - (CASE WHEN p.obligado is null THEN 0 ELSE p.obligado end)) dif_obligado ,  
 
f.saldoprev prevision1, p.prevision prevision2,  
 ((CASE WHEN f.saldoprev is null THEN 0 ELSE f.saldoprev end) - (CASE WHEN p.prevision is null THEN 0 ELSE p.prevision end)) dif_prevision ,  
 
f.saldocom compromiso1, p.compromiso compromiso2,  
 ((CASE WHEN f.saldocom is null THEN 0 ELSE f.saldocom end) - (CASE WHEN p.compromiso is null THEN 0 ELSE p.compromiso end)) dif_compromiso , 
 
f.reserva reserva1, p.reserva reserva2,  
 ((CASE WHEN f.reserva is null THEN 0 ELSE f.reserva end) - (CASE WHEN p.reserva is null THEN 0 ELSE p.reserva end)) dif_reserva,  
 
f.saldoplan saldoplan1, p.saldo_plan saldoplan2, 
 ((CASE WHEN f.saldoplan is null THEN 0 ELSE f.saldoplan end) - (CASE WHEN p.saldo_plan is null THEN 0 ELSE p.saldo_plan end)) dif_saldoplan  
 
 from 
  migracion.qry_estructura_redundancia  e  
  left join migracion.basefox_actual f on ( f.tip_codigo = e.clase AND  f.pro_codigo = e.programa 
  AND  f.sub_codigo = e.actividad and f.obj_codigo = e.obj  
  AND  f.fue_codigo = e.ff AND  f.fin_codigo = e.of AND  f.dpt_codigo = e.dpto )   
  left join migracion.qry_migracion_completo p on ( p.clase = e.clase AND  p.programa = e.programa AND  p.actividad = e.actividad AND  
	p.obj = e.obj AND  p.ff = e.ff AND  p.of = e.of AND  p.dpto = e.dpto )   
	
	
	 


