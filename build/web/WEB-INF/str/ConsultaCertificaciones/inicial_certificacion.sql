
select clase, programa, actividad, obj, ff, of, dpto, inicial, vigente, '' as esp, 
agno, fecha_emision, proveedores.nombre, monto_total as adjudicacion, descripcion,   
obj, cc, inidet.monto 
FROM contrataciones.codigos_contrataciones, 
( 
    select clase, programa, actividad, ini.obj, ini.ff, ini.of, dpto, det.monto, inicial, 
    vigente, agno,    codigo_contratacion  
    from 
    ( 
        SELECT tip_codigo clase, pro_codigo programa, sub_codigo actividad, obj_codigo obj,  
        fue_codigo ff, fin_codigo as of,  dpt_codigo dpto, ley inicial, vig vigente 
        FROM migracion.basefox_actual  
    ) as ini, 
    ( 
        SELECT codigo_contratacion, agno, tp, pg, sp, py,  obj, ff, of, dpt, monto 
        FROM contrataciones.certificacion_presupuestaria_detalle 
        where agno >= 2020  
    ) as det 

    where ini.clase = det.tp 
    and ini.programa = det.pg 
    and ini.actividad = det.py 
    and ini.obj = det.obj 
    and ini.ff = det.ff 
    and ini.of = det.of 
    and ini.dpto = det.dpt 
) 
as inidet,  contrataciones.proveedores   
where codigos_contrataciones.id = inidet.codigo_contratacion 
and proveedores.proveedor = codigos_contrataciones.proveedor 
and cc like 'v0'  
order by clase, programa, actividad, obj, ff, of, dpto  




